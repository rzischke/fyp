\documentclass[12pt,a4paper,]{article}

\usepackage[margin=2cm]{geometry}
\usepackage{enumerate}
\usepackage{pgfgantt}

\begin{document}

\title{ECE4094 Project A Progress Report}
\author{Ryan Zischke}
\maketitle

\vfill
\begin{tabular}{|c|c|c|c|c|}
    \hline
    & \textbf{First Name} & \textbf{Last Name} & \textbf{ID} & \textbf{Email} \\
    \hline
    \textbf{Supervisor} & James & Saunderson & & james.saunderson@monash.edu \\
    \hline
    \textbf{Student} & Ryan & Zischke & 25109405 & razis1@student.monash.edu \\
    \hline
\end{tabular}
\vfill

\newpage

\tableofcontents

\newpage

\section{Objectives}

This project's purpose is to increase the cost efficiency of modelling commercially relevant time series processes, relative to existing modelling techniques. This will be achieved by developing modelling techniques that impose fewer requirements on data sets and require less engagement with the data. Ideally, imperfect commercial time-series data sets could be regarded as a black-box input or resource provided to a robust and accommodating algorithm. 

In particular, developed techniques will not require data sets to have equally spaced observations, nor will they require the specification of any hyper-parameters (for example, hypothesis test significance levels). Instead, these hyper-parameters will be selected from the dataset, or from suitable defaults. 
This leaves no requirements except for the data itself, and the selection of the appropriate set within which the underlying data generating process belongs.

Modelling for two sets of processes will be explored: univariate processes with a periodic mean in white Gaussian measurement noise, and multivariate zero-mean stationary Gaussian processes with autocorrelation.

A software application, \textit{Goodness of Fit}, will be written to support the research of such modelling techniques.

\subsection{Modelling techniques}

In the context of this project, a model is an equation that completely specifies the behaviour of a (multivariate or univariate) quantity over time. This equation would contain unknown parameters that are selected by a modelling technique to be the best fit to the data by some pre-determined criteria. Once these parameters are fitted to the data, this fitted model can be used to make predictions about unobserved quantities of interest. For a full definition of the associated models for the two processes of interest, see Common Model Properties, Model One, and Model Two in this project's requirements analysis.

For each process of interest, one modelling technique will be explored. Within the broader focus on methods with a high cost efficiency, there are several specific objectives associated with the creation of each technique.

\begin{enumerate}[A)]
    \item Both models explored in this project contain an infinite number of potential parameters to fit to the data. Find or develop a regularization technique that leads to the selection of a finite set of significant parameters.
    \item Find or create a technique to fit the model to the data. A fit might include point estimators for parameters, or the model's posterior distribution given a sensible prior.
    \item Demonstrate the conditions under which this technique is consistent. For frequentist estimators, consistency means that the sampling distribution converges in probability to the true parameters as the sample size grows to infinity. For Bayesian posteriors, consistency means that the postior collapses towards a unit impulse at the true parameters of the underlying data generating process as the sampling size grows to infinity.
    \item Apply the technique to one or more simulated or real-world datasets, and evaluate the performance of predictions made by the model. Note that an in depth analysis of the performance of modelling techniques, given a wide range of sample sizes and true parameters, is out of scope for this project and may be the subject of future research.
    \item Compare this modelling technique against pre-existing modelling techniques for the same set of processes, and discuss the requirements that each technique imposes on the data set (in the form of regularity) and on the user (in the form of inputs).
\end{enumerate}

\subsection{Goodness of Fit}

\textit{Goodness of Fit} is a software application that will support the research of modelling techniques on time series processes of the kind being studied in this project. It is comprised of four types of modules:
\begin{enumerate}
    \item Observation Time Simulation
    \item Dataset
    \item Model
    \item Report
\end{enumerate}
A Dataset module either simulates data or retrieves data from storage, and passes it to a Model module for modelling. The report module is responsible for evaluating the model's fit to the dataset and generating \LaTeX\ reports of the results. An Observation Time Simulation module is sometimes used by a Dataset module to decide the times at which to simulate observations of the process. The modules used and their precise configurations on any given run of \textit{Goodness of Fit} are passed into the application via a command line interface.

There are several specific modules and application wide components of \textit{Goodness of Fit} that will be developed in the course of this project:
\begin{enumerate}
    \item Application Wide
    \begin{enumerate}
        \item Application architecture, which includes defining the directory structure of the source code of the application, and defining the structure of application modules.
        \item The main loop which takes in the command line arguments, fetches the other components, passes them their command line arguments for configuration, and connects the components together for the rest of the execution.
        \item Application documentation, which includes the state of the understandability of the source code, documentation for running \textit{Goodness of Fit}, and documentation for developing \textit{Goodness of Fit} modules.
    \end{enumerate}
    \item Observation Time Simulation
    \begin{enumerate}
        \item The exponential simulator, which simulates the time between consecutive samples as an exponential distribution.
        \item The regular sampler, which produces evenly spaced observation times.
    \end{enumerate}
    \item Dataset
    \begin{enumerate}
        \item The simulator for Model One.
        \item The simulator for Model Two.
        \item The SQLite module, which fetches data using an SQL query to an sqlite3 database.
    \end{enumerate}
    \item Model
    \begin{enumerate}
        \item The modelling technique for Model One.
        \item The modelling technique for Model Two.
    \end{enumerate}
    \item Report
    \begin{enumerate}
        \item A simulations report, which has the benefit of being able to repeatedly apply the chosen Model to multiple samples of the same underlying data generating process.
        \item A real-world dataset report, where there is only one sample per underlying data generating process.
        \item A report on a Dataset only, with no Model involvement.
    \end{enumerate}
\end{enumerate}
        

\section{Progress to date}

\subsection{Modelling techniques} \label{ssec:1}

A modelling technique has been developed for univariate processes with a periodic mean in white Gaussian measurement noise (Model One in the requirements analysis). This technique models the underlying mean as a Fourier series. It selects a finite set of frequencies that, in the Fourier series, have a magnitude significantly different from zero. All other frequencies are assumed to have a zero magnitude. Then, the parameters of the significant frequency components are estimated using ordinary least squares. This method has also been applied to a few simple simulated data sets. Therefore, progress has been made on items A, B and D for the first of the two underlying processes of interest.

\subsection{Goodness of Fit}

There are several specific modules and application wide components of \textit{Goodness of Fit} that have already been developed, or have begun development:
\begin{enumerate}
    \item Application Wide
    \begin{enumerate}
        \item The structure of the modules has been defined, and is not likely to change in any significant way. A sensible directory structure is currently being used, and is likely to change slightly as \textit{Goodness of Fit} is separated from the general final year project content into its own independent project.
        \item The main loop is functional.
    \end{enumerate}
    \item Observation Time Simulation
    \begin{enumerate}
        \item The exponential simulator is functional.
    \end{enumerate}
    \item Dataset
    \begin{enumerate}
        \item The simulator for Model One is functional.
    \end{enumerate}
    \item Model
    \begin{enumerate}
        \item The modelling technique for Model One described in section \ref{ssec:1} is functional.
    \end{enumerate}
    \item Report
    \begin{enumerate}
        \item A basic proof-of-concept report is functional.
    \end{enumerate}
\end{enumerate}

While many components are functional, nothing is yet in a finished state.

\newpage

\section{Work to be completed}

%\begin{tabular}{|c|c|c|c|c|}
%    \hline
%    & \textbf{First Name} & \textbf{Last Name} & \textbf{ID} & \textbf{Email} \\
%    \hline
%    \textbf{Supervisor} & James & Saunderson & & james.saunderson@monash.edu \\
%    \hline
%    \textbf{Student} & Ryan & Zischke & 25109405 & razis1@student.monash.edu \\
%    \hline
%\end{tabular}

\subsection{Gantt chart}

\begin{center}

\begin{tabular}{|c|p{0.5\linewidth}|c|c|}
    \hline
    \multicolumn{4}{|c|}{\textbf{Tasks and expected time allotment}} \\
    \hline
    \textbf{\#} & \centering \textbf{Task} & \textbf{Expected hours} & \textbf{Start week} \\
    \hline
    1. & \textit{Goodness of Fit} refactoring & 14 & 11/06 \\
    \hline
    2. & \textit{Goodness of Fit} SQLite Dataset module & 9 & 11/06 \\
    \hline
    3. & \textit{Goodness of Fit} dataset only Report module & 5 & 18/06 \\
    \hline
    4. & \textit{Goodness of Fit} real-world Report module & 9 & 18/06 \\
    \hline
    5. & \textit{Goodness of Fit} regular sampler Observation Time Simulation module & 3 & 18/06 \\
    \hline
    6. & Research & 80 & 25/06 \\
    \hline
    7. & Refine Model One technique in \ref{ssec:1} to achieve consistency & 30 & 23/07 \\
    \hline
    8. & Evaluate predictive performance of refined technique & 5 & 30/07 \\
    \hline
    9. & \textit{Goodness of Fit} Dataset module simulator for Model Two & 10 & 30/07 \\
    \hline
    10. & Develop modelling technique for the Model Two process, including \textit{Goodness of Fit} Model module & 50 & 06/08 \\
    \hline
    11. & \textit{Goodness of Fit} Simulations Report module & 5 & 20/08 \\
    \hline
    12. & \textit{Goodness of Fit} documentation & 20 & 27/08 \\
    \hline
    13. & Poster & 5 & 03/09 \\
    \hline
    14. & Final report & 95 & 03/09 \\
    \hline
    15. & Video & 20 & 08/10 \\
    \hline
\end{tabular}

\begin{ganttchart}[y unit title=0.4cm,
y unit chart=0.5cm,
x unit=0.041cm,
vgrid={*{19}{white},*{1}{black}},
hgrid,
title label anchor/.style={below=-1.6ex},
title left shift=.05,
title right shift=-.05,
title height=1,
bar/.style={fill=blue!50},
incomplete/.style={fill=white},
progress label text={},
bar height=0.7,
group right shift=0,
group top shift=.6,
group height=.3,
group peaks height=.2]{1}{380}
    \gantttitle{Week}{380} \\
    \gantttitle{\tiny 11/06}{20}
    \gantttitle{\tiny 18/06}{20}
    \gantttitle{\tiny 25/06}{20}
    \gantttitle{\tiny 02/07}{20}
    \gantttitle{\tiny 09/07}{20}
    \gantttitle{\tiny 16/07}{20}
    \gantttitle{\tiny 23/07}{20}
    \gantttitle{\tiny 30/07}{20}
    \gantttitle{\tiny 06/08}{20}
    \gantttitle{\tiny 13/08}{20}
    \gantttitle{\tiny 20/08}{20}
    \gantttitle{\tiny 27/08}{20}
    \gantttitle{\tiny 03/09}{20}
    \gantttitle{\tiny 10/09}{20}
    \gantttitle{\tiny 17/09}{20}
    \gantttitle{\tiny 24/09}{20}
    \gantttitle{\tiny 01/10}{20}
    \gantttitle{\tiny 08/10}{20}
    \gantttitle{\tiny 15/10}{20} \\
    
    \ganttbar{1.}{1}{14} \\
    \ganttbar{2.}{15}{23} \\
    \ganttbar{3.}{24}{28} \\
    \ganttbar{4.}{29}{37} \\
    \ganttbar{5.}{38}{40} \\
    \ganttbar{6.}{41}{120} \\
    \ganttbar{7.}{121}{150} \\
    \ganttbar{8.}{151}{155} \\
    \ganttbar{9.}{156}{165} \\
    \ganttbar{10.}{166}{215} \\
    \ganttbar{11.}{216}{220} \\
    \ganttbar{12.}{221}{240} \\
    \ganttbar{13.}{241}{245} \\
    \ganttbar{14.}{246}{340} \\
    \ganttbar{15.}{341}{360}
\end{ganttchart}

\end{center}

\end{document}

