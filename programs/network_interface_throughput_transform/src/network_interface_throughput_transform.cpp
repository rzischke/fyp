#include <algorithm>
#include <cstdint>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <ios>
#include <iostream>
#include <stdexcept>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

#include <dlib/sqlite.h>

#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

bool table_exists(dlib::database& db, const std::string& tablename) {
    return dlib::query_int(db, "SELECT count(*) FROM sqlite_master WHERE name = '" + tablename + "'")==1;
}

template<unsigned long N, class T>
void bind_and_execute(dlib::statement& st, const T& arg) {
    st.bind(N, arg);
    st.exec();
}

template<unsigned long N = 1ul, class T, class... Ts>
void bind_and_execute(dlib::statement& st, const T& arg, const Ts&... args) {
    st.bind(N, arg);
    bind_and_execute<N + 1ul>(st, args...);
}

void assert_read_only(const fs::path& pth) {
    bool publically_writable = ((fs::status(pth).permissions() & fs::perms::others_write) != fs::perms::no_perms);
    if (publically_writable) {
        std::cerr << "Abort for injection vulnerability: " << pth << " writable by world." << std::endl;
        std::exit(EXIT_FAILURE);
    }
}

void assert_not_empty(const fs::path& pth) {
    if (!fs::exists(pth) || fs::is_empty(pth)) {
        std::cerr << "Abort for " << pth << " is empty or does not exist." << std::endl;
        std::exit(EXIT_FAILURE);
    }
}

std::string get_file_contents(const fs::path& pth) {
    assert_not_empty(pth);
    assert_read_only(pth);
    std::ifstream file_handle(pth.string());
    std::istreambuf_iterator<char> begin(file_handle);
    std::istreambuf_iterator<char> end;
    return std::string(begin, end);
}

std::vector<std::string> split(const std::string& str, char delimiter) {
    std::vector<std::string> ret;

    auto ret_elem_begin = str.cbegin();
    auto ret_elem_end = std::find(ret_elem_begin, str.cend(), delimiter);
    ret.push_back(std::string(ret_elem_begin, ret_elem_end));
    ret_elem_begin = ret_elem_end;
    while (ret_elem_begin != str.cend()) {
        ret_elem_begin++;
        ret_elem_end = std::find(ret_elem_begin, str.cend(), delimiter);
        ret.push_back(std::string(ret_elem_begin, ret_elem_end));
        ret_elem_begin = ret_elem_end;
    }

    return ret;
}

void execute_from_csv_line(dlib::statement& st, const std::string& file_line, char delimiter) {
    auto parameter_begin = file_line.cbegin();
    for (size_t i = 1; true; ++i) {
        auto parameter_end = std::find(parameter_begin, file_line.cend(), delimiter);
        std::string argument = (parameter_begin==parameter_end ? std::string("NULL") : std::string(parameter_begin, parameter_end));   // Data is ommitted if its value is zero
        st.bind(i, argument);
        if (parameter_end == file_line.cend()) break;
        parameter_begin = parameter_end;
        parameter_begin++;
    }
    st.exec();
}

int main(int argc, char **argv) {
    fs::path operating_directory(argc==2 ? fs::path(argv[1]) : fs::current_path());
    if (!fs::is_directory(operating_directory)) {
        std::cerr << "Abort for " << operating_directory << " is not a directory." << std::endl;
        return EXIT_FAILURE;
    }

    fs::path db_file = operating_directory/"network_interface_throughput.sqlite3";
    dlib::database db(db_file.c_str());

    dlib::statement begin_transaction(db, "BEGIN TRANSACTION");
    dlib::statement end_transaction(db, "END TRANSACTION");
    dlib::statement st_insert(db, "INSERT INTO throughput_five_minutely VALUES (?,?,?,?);");
    dlib::statement st_insert_unix(db, "INSERT INTO throughput_five_minutely_unix_time VALUES (?,?,?,?);");
    dlib::statement st_select(db, "SELECT sha1instancename, sampletime, bytesin, bytesout FROM throughput_five_minutely_import ORDER BY sha1instancename, sampletime");

    long double ln10inv = 1.0l/log(10.0l);
    auto log10p1 = [ln10inv] (auto x) {
        return static_cast<double>(ln10inv*log1p(static_cast<long double>(x)));
    };
    st_select.exec();
    begin_transaction.exec();
    while(st_select.move_next()) {
        std::string sha1instancename;
        std::string sampletime;
        long long bytesin;
        long long bytesout;

        double log10p1_bytes_in = log10p1(bytesin);
        double log10p1_bytes_out = log10p1(bytesout);

        st_select.get_column(0, sha1instancename);
        st_select.get_column(1, sampletime);
        st_select.get_column(2, bytesin);
        st_select.get_column(3, bytesout);

        int interface;
        if (sha1instancename == "397ae4e2e83eb5eb96ddd769994c87435e765102") {
            interface = 0;
        } else if (sha1instancename == "c49d9624222752a816aab2a87fcd391765c6168a") {
            interface = 1;
        } else if (sha1instancename == "df4594706c74649407779202d11b89845cf5ec12") {
            interface = 2;
        } else {
            throw std::runtime_error("sha1instancename == " + sha1instancename);
        }

        std::time_t observation_time_unix;
        struct std::tm observation_time_tm;
        std::stringstream ss;
        ss << sampletime;
        ss >> std::get_time(&observation_time_tm, "%Y-%m-%d %H:%M:%S");
        if (ss.fail()) throw std::runtime_error("Error reading time \"" + sampletime + "\"");
        observation_time_unix = std::mktime(&observation_time_tm);

        st_insert.bind(1, interface);
        st_insert.bind(2, sampletime);
        st_insert.bind(3, log10p1_bytes_in);
        st_insert.bind(4, log10p1_bytes_out);

        st_insert_unix.bind(1, interface);
        st_insert_unix.bind(2, observation_time_unix);
        st_insert_unix.bind(3, log10p1_bytes_in);
        st_insert_unix.bind(4, log10p1_bytes_out);

        st_insert.exec();
        st_insert_unix.exec();
    }
    end_transaction.exec();

    return EXIT_SUCCESS;
}
