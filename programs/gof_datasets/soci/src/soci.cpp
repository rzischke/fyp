#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <memory>
#include <sstream>
#include <string>
#include <utility>
#include <vector>
#include <boost/optional.hpp>
#include "cli_utilities.hpp"
#include "dataset.hpp"
#include "soci/soci.h"

namespace gof {

    class column_importer_interface {
        public:
            virtual void push_back(const soci::row& r, std::size_t c) = 0; // c for column
            virtual std::vector<double>& get(void) = 0;
    };

    template<class T>
    class column_importer: public column_importer_interface {
        public:
            void push_back(const soci::row& r, std::size_t c) override {
                data.push_back(static_cast<double>(r.get<T>(c)));
            }

            std::vector<double>& get(void) {
                return data;
            }
                
        private:
            std::vector<double> data;
    };

    template<>
    class column_importer<std::tm>: public column_importer_interface {
        public:
            void push_back(const soci::row& r, std::size_t c) override {
                std::tm t = r.get<std::tm>(c);
                data_raw.push_back(std::mktime(&t));
            }

            std::vector<double>& get(void) {
                std::time_t beginning = *std::min(data_raw.cbegin(), data_raw.cend());
                data.reserve(data_raw.size());
                std::transform(
                    data_raw.cbegin(),
                    data_raw.cend(),
                    std::back_inserter(data),
                    [beginning](std::time_t end) {
                        return std::difftime(end, beginning);
                    }
                );
                return data;
            }
        private:
            std::vector<double> data;
            std::vector<std::time_t> data_raw;
    };

    std::vector<std::unique_ptr<column_importer_interface>> get_column_importers(const soci::row& r) {
        std::vector<std::unique_ptr<column_importer_interface>> importers;
        for (std::size_t i = 0; i != r.size(); ++i) {
            switch (r.get_properties(i).get_data_type()) {
                case soci::dt_double:
                    importers.emplace_back(std::make_unique<column_importer<double>>());
                    break;
                case soci::dt_integer:
                    importers.emplace_back(std::make_unique<column_importer<int>>());
                    break;
                case soci::dt_long_long:
                    importers.emplace_back(std::make_unique<column_importer<int>>());
                    break;
                case soci::dt_unsigned_long_long:
                    importers.emplace_back(std::make_unique<column_importer<unsigned long long>>());
                    break;
                case soci::dt_date:
                    importers.emplace_back(std::make_unique<column_importer<std::tm>>());
                    break;
                case soci::dt_string:
                    throw std::runtime_error("soci dataset module rejects column " + r.get_properties(i).get_name() + " for being a string type.");
                    break;
            }
        }
        return importers;
    }
    
    class soci_dataset: public dataset {
        public:
            soci_dataset(
                std::vector<std::string> database_urls_in,
                std::vector<std::vector<std::string>> queries_in
            ):
                database_urls(std::move(database_urls_in)),
                queries(std::move(queries_in))
            { }

            std::string name(void) override { return "SOCI Database Access Library"; }

            std::vector<time_series> get_data(gof::rng*) override {
                std::vector<time_series> data;

                for (std::size_t db_idx = 0; db_idx != database_urls.size(); ++db_idx){
                    const auto& database_url = database_urls[db_idx];
                    const auto& queries_this_db = queries[db_idx];

                    soci::session sql(database_url);
 
                    for (auto query_iter = queries_this_db.cbegin(); query_iter != queries_this_db.cend(); ++query_iter) {
                        const auto& query = *query_iter;
                        soci::rowset<soci::row> rs = ( sql.prepare << (*query_iter) );
                        soci::rowset<soci::row>::const_iterator row_iter = rs.begin();
                        const soci::row& r = *row_iter;

                        if (row_iter->size() < 2) {
                            throw std::runtime_error("SOCI query must return at least two columns. Query: \"" + query + "\"");
                        }

                        auto importers = get_column_importers(r);

                        do {
                            const soci::row& r = *row_iter;
                            for (std::size_t col = 0; col != r.size(); ++col) {
                                importers[col]->push_back(r,col);
                            }
                            ++row_iter;
                        } while (row_iter != rs.end());

                        std::vector<double> t = std::move(importers.front()->get());
                        //std::vector<double> t_sorted(t);
                        //std::sort(t_sorted.begin(), t_sorted.end());
                        for (std::size_t col = 1; col != importers.size()-1; ++col) {
                            //std::vector<double> y(std::move(importers[col]->get()));
                            data.emplace_back(std::move(importers[col]->get()), t);
                        }
                        data.emplace_back(std::move(importers.back()->get()), std::move(t));
                    }
                }

                return data;
            }

            std::string description(void) override {
                auto latex_escape_chars = [](std::string ret) -> std::string {
                    auto find_and_replace = [](
                        std::string& str2,
                        const std::string& find_str,
                        const std::string& replace_str
                    ) {
                        auto fl = find_str.length();
                        auto rl = replace_str.length();
                        for (
                            std::size_t f = str2.find(find_str);
                            f != std::string::npos;
                            f = str2.find(
                                find_str,
                                f+rl
                            )
                        ) {
                            str2.replace(f, fl, replace_str);
                        }
                    };

                    find_and_replace(ret, "\\", " \\textbackslash ");
                    // This must be first or else escaping back slashes will be
                    // eroneously replaced.

                    //find_and_replace(ret, "<", "\\textless");
                    //find_and_replace(ret, ">", "\\textgreater");
                    find_and_replace(ret, "&", "\\&");
                    find_and_replace(ret, "%", "\\%");
                    find_and_replace(ret, "$", "\\$");
                    find_and_replace(ret, "#", "\\#");
                    find_and_replace(ret, "_", "\\_");
                    find_and_replace(ret, "{", "\\{");
                    find_and_replace(ret, "}", "\\}");
                    find_and_replace(ret, "~", " \\textasciitilde ");
                    find_and_replace(ret, "^", " \\textasciicircum ");
                    return "\\texttt{" + ret + "}\\\\";
                };

                std::ostringstream ss;
                for (std::size_t i = 0; i != database_urls.size(); ++i) {
                    ss << latex_escape_chars(database_urls[i]) << "\\\\";
                    for (std::size_t j = 0; j != queries[i].size(); ++j) {
                        ss << latex_escape_chars(queries[i][j]) << "\\\\";
                    }
                }
                ss << '\n';
                return ss.str();
            }

        private:
            std::vector<std::string> database_urls;
            std::vector<std::vector<std::string>> queries;
            // queries return (col1=t, col2=y).
    };
}

extern "C" std::unique_ptr<gof::dataset> gof_plugin_factory(int argc, const char * const argv[]) {
    const char *lib_file_name = argv[0];

    auto print_help = [lib_file_name]() {
        std::cout <<
            lib_file_name << " retrieves time series data from one or more database sources supported by SOCI - The C++ Database Access Library.\n"
            "Usage: " << lib_file_name << "--database <SOCI db url> <SQL query returning columns (time, dimension 1, dimension 2, ...)>... [--database <url> <query>...]..." << std::endl;
    };

    gof::assert_argc_geq(argc, 3, lib_file_name, print_help);

    boost::optional<std::vector<std::string>> database_urls;
    boost::optional<std::vector<std::vector<std::string>>> sql_queries;
    auto option_indices = gof::get_option_indices(
        argc,
        argv,
        {"--database"}
    );
    for (std::size_t ii = 0; ii != option_indices.size(); ++ii) {
        std::size_t i = option_indices[ii]+1;
        if (i == argc || (ii != option_indices.size()-1 && i == option_indices[ii+1]) ) {
            print_help();
            throw std::invalid_argument(std::string(lib_file_name) + "--dataset option requires at least one query.");
        } else {
            if (!database_urls) {
                database_urls.emplace();
                sql_queries.emplace();
            }
            database_urls->emplace_back(argv[i]);
            sql_queries->emplace_back();
            for (++i; i != argc && (ii == option_indices.size()-1 || i != option_indices[ii+1]); ++i) {
                sql_queries->back().emplace_back(argv[i]);
            }
        
        }
    }
    gof::assert_arg_set(database_urls, "--database", lib_file_name, print_help);

    return std::make_unique<gof::soci_dataset>(
        std::move(*database_urls),
        std::move(*sql_queries)
    );
}

