#include "cli_utilities.hpp"
#include "dataset.hpp"
#include "observation_times_simulator.hpp"
#include "rng.hpp"
#include "sine_series.hpp"
#include <algorithm>
#include <cmath>
#include <memory>
#include <random>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>
#include <boost/math/constants/constants.hpp>
#include <boost/optional.hpp>

// Get rid of this with debug comments.
#include <iostream>

#pragma GCC diagnostic ignored "-Wmaybe-uninitialized" // To suppress uninitialized error/warning on boost::optionals.

namespace gof {

    constexpr double pi = boost::math::constants::pi<double>();
    constexpr double two_pi = 2*pi;
    constexpr double pi_inv = 1.0/pi;
    constexpr double two_pi_inv = 1.0/two_pi;
    constexpr double pos_180_on_pi = 180*pi_inv;

    class fourier_series_simulation : public dataset {
        public:
            fourier_series_simulation(
                std::vector<sine_series_polar_cos<double>> multiseries_in,
                std::unique_ptr<gof::plugin_loader<gof::observation_times_simulator>> time_simulator_plugin_loader_in,
                std::unique_ptr<observation_times_simulator> time_simulator_in,
                double noise_variance_in
            ): 
                multiseries(std::move(multiseries_in)),
                time_simulator_plugin_loader(std::move(time_simulator_plugin_loader_in)),
                time_simulator(std::move(time_simulator_in)),
                noise_dist(0.0, sqrt(noise_variance_in))
            { }

            std::string name(void) override { return "Simulation of Fourier Series in Measurement Noise"; }
            std::string description(void) override {
                auto make_column_header = [](std::size_t j, const char *param) {
                    std::ostringstream ssh;
                    ssh << param << "_{" << j << ",l}";
                    return ssh.str();
                };
                auto stddev_e = noise_dist.stddev();
                auto var_e = stddev_e*stddev_e;

                std::ostringstream ss;
                ss << "\\begin{align*}\n"
                      "    y(t)_j &= \\sum_{l} \\Big( a_{j,l} \\cos (2 \\pi f_{j,l} t) + b_{j,l} \\sin (2 \\pi f_{j,l} t) \\Big) + \\varepsilon(t)_j \\\\\n"
                      "    &= \\sum_{l} \\Big( \\|(a_{j,l},b_{j,l})\\| \\cos \\big(2 \\pi f_{j,l} t - \\angle (a_{j,l},b_{j,l})\\big) \\Big) + \\varepsilon(t)_j \\\\\n"
                      "    \\shortintertext{\n"
                      "        \\[ \\varepsilon(t) \\sim N(0," << var_e << "I)\\ \\forall\\ t \\]\n"
                      "        \\[ \\mathrm{cov} (\\varepsilon(t_1), \\varepsilon(t_2)) = 0\\ \\forall\\ t_1 \\ne t_2 \\]\n"
                      "    }\n"
                      "\\end{align*}\n"
                      "\\begin{center}\n"
                      "\\begin{tabular}{|c|l l l l l l|}\n";

                for (std::size_t j = 0; j != multiseries.size(); ++j) {
                    const auto& series = multiseries[j];

                    auto f_hdr = make_column_header(j, "f");
                    auto a_hdr = make_column_header(j, "a");
                    auto b_hdr = make_column_header(j, "b");
                    auto ab = "(" + a_hdr + "," + b_hdr + ")";

                    ss << "    \\hline\n"
                          "    \\multicolumn{7}{|c|}{$ j = " << j << " $} \\\\\n"
                          "    \\hline\n"
                          "    $l$ & $" << f_hdr << "$ & $" << a_hdr << "$ & $" << b_hdr << "$ & $\\|" << ab << "\\|$ & $-\\angle " << ab << "\\ \\mathrm{rad}$ & $-\\angle " << ab << "^{\\circ}$ \\\\\n"
                          "    \\hline\n";
                    for (std::size_t l = 0; l != series.size(); ++l) {
                        const auto& sine_wave = series[l];
                        ss << "    "
                           << l << " & "
                           << sine_wave.get_frequency() << " & "
                           << sine_wave.get_cos_projection() << " & "
                           << sine_wave.get_sin_projection() << " & "
                           << sine_wave.get_magnitude() << " & "
                           << sine_wave.get_cos_phase_rad() << " & "
                           << sine_wave.get_cos_phase_deg() << " "
                              "\\\\\n";
                    }

                    ss << "    \\hline\n";
                }

                ss << "\\end{tabular}\n"
                      "\\end{center}\n"
                      "\n";

                return ss.str();
            }

            std::vector<std::string> latex_dependencies(void) override { return {"amsmath","mathtools","gensymb","longtable"}; }

            std::vector<time_series> get_data(gof::rng* rng) override {
                auto observation_times = time_simulator->simulate(rng);
                std::cout << "observation_times.size() == " << observation_times.size() << std::endl;
                std::cout << "multiseries.size() == " << multiseries.size() << std::endl;
                auto dimensions = std::min(multiseries.size(), observation_times.size());
                std::vector<time_series> ret;
                ret.reserve(dimensions);
                for (std::size_t i = 0; i != dimensions; ++i) {
                    auto time_series_size = observation_times[i].size();
                    std::cout << "time_series_size == " << time_series_size << std::endl;
                    time_series ret_i {std::vector<double>(), std::move(observation_times[i])};
                    ret_i.y.reserve(time_series_size);
                    const auto& series = multiseries[i];
                    for (auto iter = ret_i.t.cbegin(); iter != ret_i.t.cend(); ++iter) {
                        auto time = *iter;
                        ret_i.y.emplace_back(sine_series_at_time(series,time) + noise_dist(*rng));
                    }
                    ret.emplace_back(std::move(ret_i));
                }
                return ret;
            }

        private:
            std::vector<sine_series_polar_cos<double>> multiseries;
            std::unique_ptr<gof::plugin_loader<gof::observation_times_simulator>> time_simulator_plugin_loader;
            std::unique_ptr<observation_times_simulator> time_simulator;
            std::normal_distribution<double> noise_dist;
    };

}

extern "C" std::unique_ptr<gof::dataset> gof_plugin_factory(int argc, const char * const argv[]) {
    const char *lib_file_name = argv[0];

    auto print_help = [lib_file_name]() {
        std::cout <<
            lib_file_name << " simulates a Model One process (see requirements analysis).\n"
            "Usage: " << lib_file_name << " (-F/--series <frequency cos_magnitude cos_phase>...)... "
            "-t/--times <observation_times_simulator> -v/--variance <noise variance>." << std::endl;
    };

    gof::assert_argc_geq(argc, 8, lib_file_name, print_help);

    boost::optional<std::vector<sine_series_polar_cos<double>>> sine_series_vec;
    boost::optional<std::string> time_simulator_plugin_filename;
    std::unique_ptr<gof::plugin_loader<gof::observation_times_simulator>> time_simulator_plugin_loader;
    std::unique_ptr<gof::observation_times_simulator> time_simulator;
    boost::optional<double> noise_variance;
    auto option_indices = gof::get_option_indices(
        argc,
        argv,
        {"-S","--series","-t","--times","-v","--variance"}
    );
    for (size_t ii = 0; ii != option_indices.size(); ++ii) {
        auto i = option_indices[ii];
        if (gof::check_option(argv[i], "-S", "--series")) {
            if (option_indices.size() - ii > 1 && (option_indices[ii+1] - i - 1) % 3 == 0) {
                sine_series_polar_cos<double> series;
                for (auto j = i+1; j != option_indices[ii+1]; j+=3) {
                    auto frequency = gof::get_arg<double>(argv[j], lib_file_name, print_help, gof::CLI_ARG_NONNEG);
                    auto magnitude = gof::get_arg<double>(argv[j+1], lib_file_name, print_help, gof::CLI_ARG_POS);
                    auto phase = gof::get_arg<double>(argv[j+2], lib_file_name, print_help);
                    series.emplace_back(gof::two_pi*frequency, magnitude, phase);
                }

                if (!sine_series_vec) sine_series_vec.emplace();

                sine_series_vec->emplace_back(std::move(series));
            } else {
                print_help();
                throw std::invalid_argument("Fourier series arguments were not a multiple of three.");
            }
        } else if (gof::check_and_get_option(time_simulator_plugin_filename, i, argc, argv, "-t", "--times", print_help)) {
            auto argc_nested = argc-i;
            time_simulator_plugin_loader = std::make_unique<gof::plugin_loader<gof::observation_times_simulator>>(*time_simulator_plugin_filename);
            time_simulator = time_simulator_plugin_loader->factory(argc_nested, &argv[i]);
        } else if (gof::check_and_get_option(noise_variance, i, argc, argv, "-v", "--variance", print_help)) {
        } else gof::unrecognized_argument(argv[i], lib_file_name, print_help);
    }
    gof::assert_arg_set(sine_series_vec, "-S/--series", lib_file_name, print_help);
    gof::assert_arg_set(time_simulator_plugin_filename, "-t/--times", lib_file_name, print_help);
    gof::assert_arg_set(noise_variance, "-v/--variance", lib_file_name, print_help);

    return std::make_unique<gof::fourier_series_simulation>(
        std::move(*sine_series_vec),
        std::move(time_simulator_plugin_loader),
        std::move(time_simulator),
        *noise_variance
    );

}

