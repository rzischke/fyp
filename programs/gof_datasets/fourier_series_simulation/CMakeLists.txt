cmake_minimum_required( VERSION 3.5 )

find_package( goodness_of_fit REQUIRED )
find_package( Boost REQUIRED )
find_package( kahan_sum REQUIRED )
find_package( sine_series REQUIRED )

add_library( fourier_series_simulation SHARED src/fourier_series_simulation.cpp )
target_include_directories( fourier_series_simulation
    PRIVATE
        "${Boost_INCLUDE_DIR}"
)
target_link_libraries( fourier_series_simulation
    gof::cli_utilities
    gof::dataset
    sine_series
)
set_target_properties( fourier_series_simulation PROPERTIES
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON
)
install_gof_plugin( gof::dataset fourier_series_simulation )

