#include "cli_utilities.hpp"
#include "report.hpp"
#include "chartpp/chart_xy.hpp"
#include <boost/filesystem.hpp>
#include <fstream>
#include <functional>
#include <iostream>
#include <set>
#include <stdexcept>
#include <string>
#include <vector>

namespace fs = boost::filesystem;

namespace gof {

    class latex_report : public report {

        public:
            latex_report(std::string report_dir_str): report_dir(report_dir_str) { }

            std::string name(void) override { return "Latex Report"; }
            std::string description(void) override { return "A latex report."; }

            void make(dataset* dset, model* mdl, gof::rng* rng) override {
                if (fs::exists(report_dir)) {
                    if (!fs::is_directory(report_dir)) {
                        throw(std::runtime_error(report_dir.native() + " exists, and is not a directory."));
                    }
                } else {
                    fs::create_directory(report_dir);
                }

                auto data = dset->get_data(rng);
                auto results = mdl->fit(data, rng);
                auto estimates = results->estimate(data);
                auto residuals = results->residuals(data);   // Inefficient, we already have the estimates, should just subtract.

                {
                    std::vector<std::reference_wrapper<std::vector<double>>> Y_plot_handle;
                    std::vector<std::reference_wrapper<std::vector<double>>> T_plot_handle;
                    std::vector<std::string> data_legend;
                    for (std::size_t j = 0; j != data.size(); ++j) {
                        Y_plot_handle.emplace_back(std::ref(data[j].y));
                        T_plot_handle.emplace_back(std::ref(data[j].t));
                        data_legend.emplace_back("j = " + std::to_string(j));
                    }

                    chartpp::chart_xy data_plot;
                    //data_plot.title(dset->name());
                    //data_plot.title("Dataset");
                    data_plot.title("Data");
                    data_plot.xlabel("t");
                    data_plot.ylabel("y");
                    data_plot.legend(std::move(data_legend));
                    data_plot.width_in_pixels(1920);
                    data_plot.height_in_pixels(1080);
                    data_plot.plot((report_dir/"data_plot").native(), T_plot_handle, Y_plot_handle);
                }

                {
                    std::vector<std::reference_wrapper<std::vector<double>>> Y_plot_handle;
                    std::vector<std::reference_wrapper<std::vector<double>>> T_plot_handle;
                    std::vector<std::string> estimate_legend;
                    for (std::size_t j = 0; j != estimates.size(); ++j) {
                        Y_plot_handle.emplace_back(std::ref(estimates[j].y));
                        T_plot_handle.emplace_back(std::ref(estimates[j].t));
                        estimate_legend.emplace_back("j = " + std::to_string(j));
                    }

                    chartpp::chart_xy estimates_plot;
                    estimates_plot.title("Estimates");
                    estimates_plot.xlabel("t");
                    estimates_plot.ylabel("y");
                    estimates_plot.legend(std::move(estimate_legend));
                    estimates_plot.width_in_pixels(1920);
                    estimates_plot.height_in_pixels(1080);
                    estimates_plot.plot((report_dir/"estimates_plot").native(), T_plot_handle, Y_plot_handle);
                }


                for (std::size_t j = 0; j != data.size(); ++j) {
                    auto j_str = std::to_string(j);

                    std::vector<std::reference_wrapper<std::vector<double>>> Y_plot_handle;
                    std::vector<std::reference_wrapper<std::vector<double>>> T_plot_handle;
                    
                    Y_plot_handle.emplace_back(std::ref(data[j].y));
                    T_plot_handle.emplace_back(std::ref(data[j].t));

                    Y_plot_handle.emplace_back(std::ref(estimates[j].y));
                    T_plot_handle.emplace_back(std::ref(estimates[j].t));

                    chartpp::chart_xy results_plot_j;
                    results_plot_j.title("Data and Estimates - j = " + j_str);
                    results_plot_j.xlabel("t");
                    results_plot_j.ylabel("y");
                    results_plot_j.legend({"Data", "Model Estimate"});
                    results_plot_j.width_in_pixels(1920);
                    results_plot_j.height_in_pixels(1080);
                    results_plot_j.plot((report_dir/("comparison_plot_" + j_str)).native(), T_plot_handle, Y_plot_handle);
                }

                {
                    std::vector<std::reference_wrapper<std::vector<double>>> Y_plot_handle;
                    std::vector<std::reference_wrapper<std::vector<double>>> T_plot_handle;
                    std::vector<std::string> residuals_legend;
                    for (std::size_t j = 0; j != data.size(); ++j) {
                        Y_plot_handle.emplace_back(std::ref(residuals[j].y));
                        T_plot_handle.emplace_back(std::ref(residuals[j].t));
                        residuals_legend.emplace_back("j = " + std::to_string(j));
                    }
                    chartpp::chart_xy residuals_plot;
                    residuals_plot.title("Residuals");
                    residuals_plot.xlabel("t");
                    residuals_plot.ylabel("y_t - y_hat_t");      // Verify that this is the equation used in results.residuals(...)
                    residuals_plot.legend(std::move(residuals_legend));
                    residuals_plot.width_in_pixels(1920);
                    residuals_plot.height_in_pixels(1080);
                    residuals_plot.plot((report_dir/"residuals_plot").native(), T_plot_handle, Y_plot_handle);
                }

                auto dataset_content_path = report_dir/"dataset_content.tex";
                std::ofstream dataset_content(dataset_content_path.native());
                dataset_content << "\\section*{Dataset}\n"
                                   "\\subsection*{" << dset->name() << "}\n"
                                << dset->description() << "\n";

                auto model_content_path = report_dir/"model_content.tex";
                std::ofstream model_content(model_content_path.native());
                model_content << "\\section*{Model}\n"
                                 "\\subsection*{" << mdl->name() << "}\n"
                              << mdl->description() << "\n";

                auto results_content_path = report_dir/"results_content.tex";
                std::ofstream results_content(results_content_path.native());
                results_content << "\\section*{Results}\n"
                                   "\\subsection*{" << results->name() << "}\n"
                                << results->description() << "\n";
                /*
                std::ofstream report_content(report_content_path.native());
                report_content << "\\section*{Dataset}\n"
                                  "\\subsection*{" << dset->name() << "}\n"
                               << dset->description() << "\n\n"
                                  "\\section*{Model}\n"
                                  "\\subsection*{" << mdl->name() << "}\n"
                               << mdl->description() << "\n\n"
                               << "\\section*{Fit Results}\n"
                                  "\\subsection*{" << results->name() << "}\n"
                               << results->description() << "\n\n";
                */

                auto dataset_latex_dependencies = dset->latex_dependencies();
                std::set<std::string> latex_dependencies(dataset_latex_dependencies.cbegin(), dataset_latex_dependencies.cend());
                auto model_latex_dependencies = mdl->latex_dependencies();
                latex_dependencies.insert(model_latex_dependencies.cbegin(), model_latex_dependencies.cend());
                auto results_latex_dependencies = results->latex_dependencies();
                latex_dependencies.insert(results_latex_dependencies.cbegin(), results_latex_dependencies.cend());
                latex_dependencies.emplace("graphicx");
                latex_dependencies.emplace("float");
                //latex_dependencies.emplace("caption");

                std::ofstream report((report_dir/report_dir.stem()).native() + ".tex");
                report << "\\documentclass[10pt]{article}\n"
                          "\\usepackage[margin=2cm]{geometry}\n";
                for (auto iter = latex_dependencies.cbegin(); iter != latex_dependencies.cend(); ++iter) {
                    report << "\\usepackage{" << *iter << "}\n";
                }
                report << "\\begin{document}\n"
                          "\\title{Goodness of Fit Report}\n"
                          "\\maketitle\n"
                          "\n"
                          "\\input{" << dataset_content_path.stem() << "}\n"
                          "\n"
                          "\\includegraphics[width=\\textwidth]{data_plot}\n"
                          "\n"
                          "\\input{" << model_content_path.stem() << "}\n"
                          "\n"
                          "\\input{" << results_content_path.stem() << "}\n"
                          "\n"
                          "\\includegraphics[width=\\textwidth]{estimates_plot}\n"
                          "\n";

                for (std::size_t j = 0; j != data.size(); ++j) {
                    report << "\\includegraphics[width=\\textwidth]{comparison_plot_" << j << "}\n"
                              "\n";
                }
                report << "\\includegraphics[width=\\textwidth]{residuals_plot}\n"
                          "\n"
                          "\\end{document}\n";
            }

            ~latex_report() { }

        private:
            fs::path report_dir;

    };

}

extern "C" std::unique_ptr<gof::report> gof_plugin_factory(int argc, const char * const argv[]) {
    const char *lib_file_name = argv[0];

    auto print_help = [lib_file_name]() {
        std::cout <<
            lib_file_name << " outputs a latex report of the models goodness of fit to the data.\n"
            "Usage: " << lib_file_name << " <output directory name>" << std::endl;
    };

    gof::assert_argc_eq(argc, 2, lib_file_name, print_help);

    return std::make_unique<gof::latex_report>(argv[1]);
}

