#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE fourier_series
#include <boost/test/included/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>
#include <boost/math/constants/constants.hpp>

#include <iostream>

#include <cmath>
#include <random>

#include "sine_wave_rectangular.hpp"
#include "sine_wave_polar_cos.hpp"
#include "sine_wave_polar_sin.hpp"

#include "sine_series.hpp"

template<class fpT>
bool eq(fpT x, fpT y) {
    auto eps = static_cast<fpT>(1.0e-6l);
    return (abs(x-y)<eps);
}

template<class fpT>
void single_wave_tests(
    fpT w_a,
    fpT f_a,
    fpT A_a,
    fpT phi_c_rad_a,
    fpT phi_c_deg_a,
    fpT phi_s_rad_a,
    fpT phi_s_deg_a,
    fpT x_a,
    fpT y_a,
    fpT w_b,
    fpT f_b,
    fpT A_b,
    fpT phi_c_rad_b,
    fpT phi_c_deg_b,
    fpT phi_s_rad_b,
    fpT phi_s_deg_b,
    fpT x_b,
    fpT y_b
) {
    constexpr fpT pi = boost::math::constants::pi<fpT>();
    constexpr fpT two_pi = static_cast<fpT>(2.0l)*pi;
    constexpr fpT pi_inv = static_cast<fpT>(1.0l)/pi;
    constexpr fpT two_pi_inv = static_cast<fpT>(2.0l)*pi_inv;
    constexpr fpT deg_per_rad = static_cast<fpT>(180.0l)*pi_inv;

    BOOST_CHECK(eq(w_a,two_pi*f_a));
    BOOST_CHECK(eq(A_a,hypot(x_a,y_a)));
    BOOST_CHECK(eq(phi_c_deg_a,phi_c_rad_a*deg_per_rad));
    BOOST_CHECK(eq(phi_s_deg_a,phi_s_rad_a*deg_per_rad));
    BOOST_CHECK(eq(phi_c_rad_a,atan2(-y_a,x_a)));
    BOOST_CHECK(eq(phi_s_rad_a,atan2(x_a,y_a)));
    BOOST_CHECK(eq(x_a,A_a*sin(phi_s_rad_a)));
    BOOST_CHECK(eq(x_a,A_a*cos(phi_c_rad_a)));
    BOOST_CHECK(eq(y_a,A_a*cos(phi_s_rad_a)));
    BOOST_CHECK(eq(y_a,-A_a*sin(phi_c_rad_a)));

    BOOST_CHECK(eq(w_a, w_b));
    BOOST_CHECK(eq(f_a, f_b));
    BOOST_CHECK(eq(A_a, A_b));
    BOOST_CHECK(eq(phi_c_rad_a, phi_c_rad_b));
    BOOST_CHECK(eq(phi_c_deg_a, phi_c_deg_b));
    BOOST_CHECK(eq(phi_s_rad_a, phi_s_rad_b));
    BOOST_CHECK(eq(phi_s_deg_a, phi_s_deg_b));
    BOOST_CHECK(eq(x_a, x_b));
    BOOST_CHECK(eq(y_a, y_b));
}



template<class sineA, class sineB>
void single_wave_tests(sineA a, sineB b) {
    single_wave_tests(
        a.get_angular_frequency(),
        a.get_frequency(),
        a.get_magnitude(),
        a.get_cos_phase_rad(),
        a.get_cos_phase_deg(),
        a.get_sin_phase_rad(),
        a.get_sin_phase_deg(),
        a.get_cos_projection(),
        a.get_sin_projection(),
        b.get_angular_frequency(),
        b.get_frequency(),
        b.get_magnitude(),
        b.get_cos_phase_rad(),
        b.get_cos_phase_deg(),
        b.get_sin_phase_rad(),
        b.get_sin_phase_deg(),
        b.get_cos_projection(),
        b.get_sin_projection()
    );
}

using wave_test_types = boost::mpl::list<
    sine_wave_rectangular<double>,
    sine_wave_polar_cos<double>,
    sine_wave_polar_sin<double>
>;

BOOST_AUTO_TEST_CASE_TEMPLATE( sine_wave, T, wave_test_types ) {
    using floating_point_type = typename T::floating_point_type;
    sine_wave_rectangular<floating_point_type> wave_original(
        static_cast<floating_point_type>(2.0l),
        static_cast<floating_point_type>(5.0l),
        static_cast<floating_point_type>(7.0l)
    );
    T wave(wave_original);
    single_wave_tests(wave, wave_original);
}

using series_test_types = boost::mpl::list<
    sine_series_rectangular<double>,
    sine_series_polar_cos<double>,
    sine_series_polar_sin<double>
>;

std::random_device device;
std::default_random_engine gen(device());
std::chi_squared_distribution<double> chi;
std::bernoulli_distribution bern;

double pos_rand(void) {
    return chi(gen) + 0.1;
}

double pos_neg_rand(void) {
    return bern(gen) ? pos_rand() : -pos_rand();
}

BOOST_AUTO_TEST_CASE_TEMPLATE( sine_series, T, series_test_types ) {
    using floating_point_type = typename T::value_type::floating_point_type;
    sine_series_rectangular<floating_point_type> series_original;
    for (std::size_t i = 0; i != 10; ++i) {
        series_original.emplace_back(sine_wave_rectangular<floating_point_type>(
            pos_rand(),
            pos_rand(),
            pos_rand()
        ));
    }
    auto series = sine_series_to<T>(series_original);
    
    for (std::size_t i = 0; i != series.size(); ++i) {
        single_wave_tests(series[i], series_original[i]);
    }

    auto w_a = get_angular_frequencies(series);
    auto f_a = get_frequencies(series);
    auto A_a = get_magnitudes(series);
    auto phi_c_rad_a = get_cos_phases_rad(series);
    auto phi_c_deg_a = get_cos_phases_deg(series);
    auto phi_s_rad_a = get_sin_phases_rad(series);
    auto phi_s_deg_a = get_sin_phases_deg(series);
    auto x_a = get_cos_projections(series);
    auto y_a = get_sin_projections(series);

    auto w_b = get_angular_frequencies(series_original);
    auto f_b = get_frequencies(series_original);
    auto A_b = get_magnitudes(series_original);
    auto phi_c_rad_b = get_cos_phases_rad(series_original);
    auto phi_c_deg_b = get_cos_phases_deg(series_original);
    auto phi_s_rad_b = get_sin_phases_rad(series_original);
    auto phi_s_deg_b = get_sin_phases_deg(series_original);
    auto x_b = get_cos_projections(series_original);
    auto y_b = get_sin_projections(series_original);

    for (std::size_t i = 0; i != series.size(); ++i) {
        single_wave_tests(
            w_a[i],
            f_a[i],
            A_a[i],
            phi_c_rad_a[i],
            phi_c_deg_a[i],
            phi_s_rad_a[i],
            phi_s_deg_a[i],
            x_a[i],
            y_a[i],
            w_b[i],
            f_b[i],
            A_b[i],
            phi_c_rad_b[i],
            phi_c_deg_b[i],
            phi_s_rad_b[i],
            phi_s_deg_b[i],
            x_b[i],
            y_b[i]
        );
    }
}

