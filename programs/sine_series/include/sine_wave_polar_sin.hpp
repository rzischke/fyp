#ifndef SINE_WAVE_POLAR_SIN_GUARD
#define SINE_WAVE_POLAR_SIN_GUARD

#include <cmath>
#include <boost/math/constants/constants.hpp>

#include "sine_wave_interface.hpp"

template<class T> class sine_wave_polar_sin;

template<class T>
struct sine_wave_traits<sine_wave_polar_sin<T>> {
    using floating_point_type = typename std::remove_cv<typename std::remove_reference<T>::type>::type;
};

template <class T>
class sine_wave_polar_sin: public sine_wave_interface<sine_wave_polar_sin<T>> {
    public:
        using floating_point_type = typename sine_wave_traits<sine_wave_polar_sin<T>>::floating_point_type;

        sine_wave_polar_sin(
            T angular_frequency_in,
            T magnitude_in,
            T sin_phase_rad_in
        ):
            angular_frequency(angular_frequency_in),
            magnitude(magnitude_in),
            sin_phase_rad(remainder(
                sin_phase_rad_in,
                static_cast<floating_point_type>((2.0l)*boost::math::constants::pi<floating_point_type>())
            ))
        { }

        template<class derivedT>
        sine_wave_polar_sin(const sine_wave_interface<derivedT>& rhs):
            angular_frequency(rhs.get_angular_frequency()),
            magnitude(rhs.get_magnitude()),
            sin_phase_rad(rhs.get_sin_phase_rad())
        { }

        template<class derivedT>
        sine_wave_polar_sin<T>& operator=(const sine_wave_interface<derivedT>& rhs) {
            angular_frequency = rhs.get_angular_frequency();
            magnitude = rhs.get_magnitude();
            sin_phase_rad = rhs.get_sin_phase_rad();
        }

        floating_point_type operator()(floating_point_type time) const {
            return magnitude*sin(angular_frequency*time + sin_phase_rad);
        }

        floating_point_type get_angular_frequency(void) const {
            return angular_frequency;
        }

        floating_point_type get_frequency(void) const {
            static const auto two_pi_inv = static_cast<floating_point_type>(0.5l/boost::math::constants::pi<long double>());
            return angular_frequency*two_pi_inv;
        }

        floating_point_type get_magnitude(void) const {
            return magnitude;
        }

        floating_point_type get_cos_phase_rad(void) const {
            static const auto half_pi = static_cast<floating_point_type>(0.5l*boost::math::constants::pi<long double>());
            static const auto two_pi = static_cast<floating_point_type>(2.0l*boost::math::constants::pi<long double>());
            return remainder(sin_phase_rad - half_pi, two_pi);
        }

        floating_point_type get_cos_phase_deg(void) const {
            static const auto deg_per_rad = static_cast<floating_point_type>(180.0l/boost::math::constants::pi<long double>());
            return get_cos_phase_rad()*deg_per_rad;
        }

        floating_point_type get_sin_phase_rad(void) const {
            return sin_phase_rad;
        }

        floating_point_type get_sin_phase_deg(void) const {
            static const auto deg_per_rad = static_cast<floating_point_type>(180.0l/boost::math::constants::pi<long double>());
            return sin_phase_rad*deg_per_rad;
        }

        floating_point_type get_cos_projection(void) const {
            return magnitude*sin(sin_phase_rad);
        }

        floating_point_type get_sin_projection(void) const {
            return magnitude*cos(sin_phase_rad);
        }

    private:
        T angular_frequency;
        T magnitude;
        T sin_phase_rad;
};

#endif

