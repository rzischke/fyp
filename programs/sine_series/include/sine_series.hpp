#ifndef FOURIER_SERIES_GUARD
#define FOURIER_SERIES_GUARD

#include <algorithm>
#include <iterator>
#include <stdexcept>
#include <utility>
#include <vector>

#include "kahan_sum.hpp"
#include "sine_wave_rectangular.hpp"
#include "sine_wave_polar_cos.hpp"
#include "sine_wave_polar_sin.hpp"

template<class fpT, class Alloc = std::allocator<sine_wave_rectangular<fpT>>>
using sine_series_rectangular = std::vector<sine_wave_rectangular<fpT>, Alloc>;

template<class fpT, class Alloc = std::allocator<sine_wave_polar_cos<fpT>>>
using sine_series_polar_cos = std::vector<sine_wave_polar_cos<fpT>, Alloc>;

template<class fpT, class Alloc = std::allocator<sine_wave_polar_sin<fpT>>>
using sine_series_polar_sin = std::vector<sine_wave_polar_sin<fpT>, Alloc>;

template<class seriesT, class fpT>
seriesT construct_sine_series(
    const std::vector<fpT>& a,
    const std::vector<fpT>& b,
    const std::vector<fpT>& c
) {
    typename std::vector<fpT>::size_type size = a.size();

    if (size != b.size() || size != c.size()) {
        throw std::logic_error("Attempted to construct_sine_series with differently sized vectors.");
    }

    seriesT series_out;
    series_out.reserve(size);

    for(decltype(size) i = 0; i != size; ++i) {
        series_out.emplace_back(a[i],b[i],c[i]);
    }

    return series_out;
}

template<class seriesOutT, class seriesInT>
void sine_series_to(seriesOutT& out, const seriesInT& in) {
    out.reserve(in.size());
    std::copy(in.cbegin(), in.cend(), std::back_inserter(out));
}

template<class seriesOutT, class seriesInT>
auto sine_series_to(const seriesInT& in) {
    seriesOutT out;
    sine_series_to(out, in);
    return out;
}

template<class seriesInT>
auto sine_series_to_rectangular(const seriesInT& in) {
    using fp = typename seriesInT::value_type::floating_point_type;
    return sine_series_to<sine_series_rectangular<fp>>(in);
}

template<class seriesInT>
auto sine_series_to_polar_cos(const seriesInT& in) {
    using fp = typename seriesInT::value_type::floating_point_type;
    return sine_series_to<sine_series_polar_cos<fp>>(in);
}

template<class seriesInT>
auto sine_series_to_polar_sin(const seriesInT& in) {
    using fp = typename seriesInT::value_type::floating_point_type;
    return sine_series_to<sine_series_polar_sin<fp>>(in);
}

template<class seriesT>
typename seriesT::value_type::floating_point_type sine_series_at_time(
    const seriesT& series,
    typename seriesT::value_type::floating_point_type time)
{
    if (series.empty()) {
        throw std::runtime_error("Attempted to evaluate an empty sine series in sine_series_at_time.");
    }

    kahan_summer<decltype(time)> kahan_summer;
    for (auto iter = series.cbegin(); iter != series.cend(); ++iter) {
        const auto& sine_wave = *iter;
        kahan_summer.add(sine_wave(time));
    }
    return kahan_summer.get_sum();
}

template<class seriesT, class UniaryFn>
auto get_from_sine_series(const seriesT& series, UniaryFn&& transformer) {
    std::vector<typename seriesT::value_type::floating_point_type> ret;
    ret.reserve(series.size());

    std::transform(
        series.cbegin(),
        series.cend(),
        std::back_inserter(ret),
        transformer
    );

    return ret;
}

template<class seriesT>
auto get_angular_frequencies(const seriesT& series) {
    auto transformer = [](const typename seriesT::value_type& val) {
        return val.get_angular_frequency();
    };
    return get_from_sine_series(series, std::move(transformer));
}

template<class seriesT>
auto get_frequencies(const seriesT& series) {
    auto transformer = [](const typename seriesT::value_type& val) {
        return val.get_frequency();
    };
    return get_from_sine_series(series, std::move(transformer));
}

template<class seriesT>
auto get_magnitudes(const seriesT& series) {
    auto transformer = [](const typename seriesT::value_type& val) {
        return val.get_magnitude();
    };
    return get_from_sine_series(series, std::move(transformer));
}

template<class seriesT>
auto get_cos_phases_rad(const seriesT& series) {
    auto transformer = [](const typename seriesT::value_type& val) {
        return val.get_cos_phase_rad();
    };
    return get_from_sine_series(series, std::move(transformer));
}

template<class seriesT>
auto get_cos_phases_deg(const seriesT& series) {
    auto transformer = [](const typename seriesT::value_type& val) {
        return val.get_cos_phase_deg();
    };
    return get_from_sine_series(series, std::move(transformer));
}

template<class seriesT>
auto get_sin_phases_rad(const seriesT& series) {
    auto transformer = [](const typename seriesT::value_type& val) {
        return val.get_sin_phase_rad();
    };
    return get_from_sine_series(series, std::move(transformer));
}

template<class seriesT>
auto get_sin_phases_deg(const seriesT& series) {
    auto transformer = [](const typename seriesT::value_type& val) {
        return val.get_sin_phase_deg();
    };
    return get_from_sine_series(series, std::move(transformer));
}

template<class seriesT>
auto get_cos_projections(const seriesT& series) {
    auto transformer = [](const typename seriesT::value_type& val) {
        return val.get_cos_projection();
    };
    return get_from_sine_series(series, std::move(transformer));
}

template<class seriesT>
auto get_sin_projections(const seriesT& series) {
    auto transformer = [](const typename seriesT::value_type& val) {
        return val.get_sin_projection();
    };
    return get_from_sine_series(series, std::move(transformer));
}

#endif

