#ifndef SINE_WAVE_RECTANGULAR_GUARD
#define SINE_WAVE_RECTANGULAR_GUARD

#include <cmath>
#include <boost/math/constants/constants.hpp>

#include "sine_wave_interface.hpp"

template<class T> class sine_wave_rectangular;

template<class T>
struct sine_wave_traits<sine_wave_rectangular<T>> {
    using floating_point_type = typename std::remove_cv<typename std::remove_reference<T>::type>::type;
};

template<class T>
class sine_wave_rectangular: public sine_wave_interface<sine_wave_rectangular<T>> {
    public:
        using floating_point_type = typename sine_wave_traits<sine_wave_rectangular<T>>::floating_point_type;

        sine_wave_rectangular(
            T angular_frequency_in,
            T cos_projection_in,
            T sin_projection_in
        ):
            angular_frequency(angular_frequency_in),
            cos_projection(cos_projection_in),
            sin_projection(sin_projection_in)
        { }

        template<class derivedT>
        sine_wave_rectangular(const sine_wave_interface<derivedT>& rhs):
            angular_frequency(rhs.get_angular_frequency()),
            cos_projection(rhs.get_cos_projection()),
            sin_projection(rhs.get_sin_projection())
        { }

        template<class derivedT>
        sine_wave_rectangular<T>& operator=(const sine_wave_interface<derivedT>& rhs) {
            angular_frequency = rhs.get_angular_frequency();
            cos_projection = rhs.get_cos_projection();
            sin_projection = rhs.get_sin_projection();
            return *this;
        }

        floating_point_type operator()(floating_point_type time) const {
            return cos_projection*cos(angular_frequency*time) + sin_projection*sin(angular_frequency*time);
        }

        floating_point_type get_angular_frequency(void) const {
            return angular_frequency;
        }

        floating_point_type get_frequency(void) const {
            static const auto two_pi_inv = static_cast<floating_point_type>(0.5l/boost::math::constants::pi<long double>());
            return angular_frequency*two_pi_inv;
        }

        floating_point_type get_magnitude(void) const {
            auto ret = static_cast<floating_point_type>(hypot(cos_projection,sin_projection));
            return hypot(cos_projection,sin_projection);
        }

        floating_point_type get_cos_phase_rad(void) const {
            return atan2(-sin_projection,cos_projection);
        }

        floating_point_type get_cos_phase_deg(void) const {
            static const auto deg_per_rad = static_cast<floating_point_type>(180.0l/boost::math::constants::pi<long double>());
            return get_cos_phase_rad()*deg_per_rad;
        }

        floating_point_type get_sin_phase_rad(void) const {
            return atan2(cos_projection,sin_projection);
        }

        floating_point_type get_sin_phase_deg(void) const {
            static const auto deg_per_rad = static_cast<floating_point_type>(180.0l/boost::math::constants::pi<long double>());
            return get_sin_phase_rad()*deg_per_rad;
        }

        floating_point_type get_cos_projection(void) const {
            return cos_projection;
        }

        floating_point_type get_sin_projection(void) const {
            return sin_projection;
        }

    private:
        T angular_frequency;
        T cos_projection;
        T sin_projection;
};

#endif
