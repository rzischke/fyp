#ifndef SINE_WAVE_INTERFACE_GUARD
#define SINE_WAVE_INTERFACE_GUARD

#include <type_traits>

template<class T> struct sine_wave_traits;

template<class derivedT>
struct sine_wave_interface {
    using floating_point_type = typename sine_wave_traits<derivedT>::floating_point_type;

    static_assert(
        std::is_floating_point<floating_point_type>::value,
        "The floating_point_type of a sine wave was not a floating point type."
    );

    floating_point_type operator()(floating_point_type time) const {
        return static_cast<const derivedT*>(this)->operator()(time);
    }

    floating_point_type get_angular_frequency(void) const {
        return static_cast<const derivedT*>(this)->get_angular_frequency();
    }

    floating_point_type get_frequency(void) const {
        return static_cast<const derivedT*>(this)->get_frequency();
    }

    floating_point_type get_magnitude(void) const {
        return static_cast<const derivedT*>(this)->get_magnitude();
    }

    floating_point_type get_cos_phase_rad(void) const {
        return static_cast<const derivedT*>(this)->get_cos_phase_rad();
    }

    floating_point_type get_cos_phase_deg(void) const {
        return static_cast<const derivedT*>(this)->get_cos_phase_deg();
    }

    floating_point_type get_sin_phase_rad(void) const {
        return static_cast<const derivedT*>(this)->get_sin_phase_rad();
    }

    floating_point_type get_sin_phase_deg(void) const {
        return static_cast<const derivedT*>(this)->get_sin_phase_deg();
    }

    floating_point_type get_cos_projection(void) const {
        return static_cast<const derivedT*>(this)->get_cos_projection();
    }

    floating_point_type get_sin_projection(void) const {
        return static_cast<const derivedT*>(this)->get_sin_projection();
    }

};

#endif
