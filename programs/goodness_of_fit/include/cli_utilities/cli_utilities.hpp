#ifndef CLI_UTILITIES_GUARD
#define CLI_UTILITIES_GUARD

#include <bitset>
#include <cstring>
#include <sstream>
#include <stdexcept>
#include <typeinfo>
#include <boost/optional.hpp>

namespace gof {

    constexpr unsigned long CLI_ARG_POS = 0x1;
    constexpr unsigned long CLI_ARG_NEG = 0x2;
    constexpr unsigned long CLI_ARG_NONPOS = 0x4;
    constexpr unsigned long CLI_ARG_NONNEG = 0x8;

    template <class F>
    void assert_argc_geq(int argc, int target, const char *cmd_name, const F& print_help) {
        if (argc < target) {
            std::ostringstream ss;
            ss << cmd_name << " requires at least " << (target-1) << " arguments, " << (argc-1) << " provided.";
            print_help();
            throw std::invalid_argument(ss.str());
        }
    }

    template <class F>
    void assert_argc_eq(int argc, int target, const char *cmd_name, const F& print_help) {
        if (argc != target) {
            std::ostringstream ss;
            ss << cmd_name << " requires exactly" << (target-1) << " arguments, " << (argc-1) << " provided.";
            print_help();
            throw std::invalid_argument(ss.str());
        }
    }

    template <class T, class F>
    void assert_arg_unset(boost::optional<T> arg, const char *option, const char *cmd_name, const F& print_help) {
        if (arg) {
            std::ostringstream ss;
            ss << option << " option in " << cmd_name << " was set twice, or before another option with which it is incompatible.";
            print_help();
            throw std::invalid_argument(ss.str());
        }
    }

    template <class T, class F>
    void assert_arg_set(boost::optional<T> arg, const char *option, const char *cmd_name, const F& print_help) {
        if (!arg) {
            std::ostringstream ss;
            ss << "Compulsory option " << option << " in " << cmd_name << " was not set.";
            print_help();
            throw std::invalid_argument(ss.str());
        }
    }

    template <class T>
    bool try_get_arg(T& arg, const char *arg_str) {
        std::stringstream ss;
        T arg_temp;
        ss << arg_str;

        try {
            ss >> arg_temp;
        } catch (const std::ios_base::failure& e) {
            if (ss.bad()) {
                throw;
            } else if (ss.fail()) {
                return false;
            } else {
                throw;
            }
        }

        if (!ss.eof()) {
            return false;
        }

        arg = arg_temp;

        return true;
    }

    template <class T, class F>
    T get_arg(const char *arg_str, const char *cmd_name, const F& print_help) {
        auto get_invalid_argument_message = [&arg_str, &cmd_name]() {
            auto type_name = typeid(T).name();
            std::ostringstream ss;
            ss << "Argument \"" << arg_str << " passed to " << cmd_name << " is not a valid " << type_name << ".";
            return ss.str();
        };

        std::stringstream ss;
        T arg;
        ss << arg_str;

        try {
            ss >> arg;
        } catch (const std::ios_base::failure& e) {
            if (ss.bad()) {
                throw;
            } else if (ss.fail()) {
                print_help();
                throw std::invalid_argument(get_invalid_argument_message());
            } else {
                throw;
            }
        }

        if (!ss.eof()) {
            print_help();
            throw std::invalid_argument(get_invalid_argument_message());
        }

        return arg;
    }

    template <class T, class F>
    T get_arg(const char *arg_str, const char *cmd_name, const F& print_help, unsigned long condition) {
        auto get_out_of_domain_message = [arg_str, cmd_name](const char *cond_str) {
            std::ostringstream ss;
            ss << "Argument \"" << arg_str << " passed to " << cmd_name << " wasn't " << cond_str << ".";
            return ss.str();
        };

        auto arg = get_arg<T>(arg_str, cmd_name, print_help);

        if (condition == CLI_ARG_POS && arg <= 0) {
            print_help();
            throw std::domain_error(get_out_of_domain_message("positive"));
        } else if (condition == CLI_ARG_NEG && arg >= 0) {
            print_help();
            throw std::domain_error(get_out_of_domain_message("negative"));
        } else if (condition == CLI_ARG_NONPOS && arg > 0) {
            print_help();
            throw std::domain_error(get_out_of_domain_message("non-positive"));
        } else if (condition == CLI_ARG_NONNEG && arg < 0) {
            print_help();
            throw std::domain_error(get_out_of_domain_message("non-negative"));
        }

        return arg;
    }

    bool check_option(const char *arg_str, const char *short_flag, const char *long_flag) {
        return (strcmp(arg_str, short_flag)==0 || strcmp(arg_str, long_flag)==0);
    }

    template <class T, class F1, class F2>
    bool check_and_get_option(
        T& arg,
        int& idx,
        int argc,
        const char * const argv[],
        const char *short_flag,
        const char *long_flag,
        const F1& print_help,
        const F2& get_arg_fn
    ) {
        bool ret;
        auto cmd_name = argv[0];
        if (check_option(argv[idx], short_flag, long_flag)) {
            assert_argc_geq(argc, idx+2, cmd_name, print_help);
            auto arg_str = argv[idx+1];
            arg = get_arg_fn(arg_str);
            ++idx;
            ret = true;
        } else {
            ret = false;
        }
        return ret;
    }

    template <class T, class F1>
    bool check_and_get_option(
        T& arg,
        int& idx,
        int argc,
        const char * const argv[],
        const char *short_flag,
        const char *long_flag,
        const F1& print_help
    ) {
        auto cmd_name = argv[0];
        auto get_arg_fn = [cmd_name, &print_help](const char *arg_str) { return get_arg<T>(arg_str, cmd_name, print_help); };
        return check_and_get_option(arg, idx, argc, argv, short_flag, long_flag, print_help, get_arg_fn);
    }

    template <class T, class F1>
    bool check_and_get_option(
        T& arg,
        int& idx,
        int argc,
        const char * const argv[],
        const char *short_flag,
        const char *long_flag,
        const F1& print_help,
        unsigned long condition
    ) {
        auto cmd_name = argv[0];
        auto get_arg_fn = [cmd_name, &print_help, condition](const char *arg_str) { return get_arg<T>(arg_str, cmd_name, print_help, condition); };
        return check_and_get_option(arg, idx, argc, argv, short_flag, long_flag, print_help, get_arg_fn);
    }

    template <class T, class F>
    bool check_and_get_option(
        boost::optional<T>& arg,
        int& idx,
        int argc,
        const char * const argv[],
        const char *short_flag,
        const char *long_flag,
        const F& print_help
    ) {
        bool ret;
        T arg_raw;
        auto cmd_name = argv[0];
        if (check_and_get_option(arg_raw, idx, argc, argv, short_flag, long_flag, print_help)) {
            assert_arg_unset(arg, long_flag, cmd_name, print_help);
            arg = arg_raw;
            ret = true;
        } else {
            ret = false;
        }
        return ret;
    }

    template <class T, class F>
    bool check_and_get_option(
        boost::optional<T>& arg,
        int& idx,
        int argc,
        const char * const argv[],
        const char *short_flag,
        const char *long_flag,
        const F& print_help,
        unsigned long condition
    ) {
        bool ret;
        T arg_raw;
        auto cmd_name = argv[0];
        if (check_and_get_option(arg_raw, idx, argc, argv, short_flag, long_flag, print_help, condition)) {
            assert_arg_unset(arg, long_flag, cmd_name, print_help);
            arg = arg_raw;
            ret = true;
        } else {
            ret = false;
        }
        return ret;
    }

    template <class F>
    void unrecognized_argument(const char *arg_str, const char *cmd_name, const F& print_help) {
        std::ostringstream ss;
        ss << "Unrecognized argument \"" << arg_str << "\" to " << cmd_name << ".";
        print_help();
        throw std::invalid_argument(ss.str());
    }

    std::vector<int> get_option_indices(int argc, const char * const argv[], std::vector<const char *> options) {
        std::vector<int> ret;
        for (int i = 0; i != argc; ++i) {
            for (auto iter = options.cbegin(); iter != options.cend(); ++iter) {
                if (strcmp(argv[i], *iter) == 0) {
                    ret.push_back(i);
                    break;
                }
            }
        }
        return ret;
    }
}

#endif
