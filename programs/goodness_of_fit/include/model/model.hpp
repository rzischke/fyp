#ifndef GOF_MODEL_GUARD
#define GOF_MODEL_GUARD

#include "plugin.hpp"
#include "time_series.hpp"
#include "rng.hpp"
#include "kahan_sum.hpp"
#include <memory>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

namespace gof {

    class model_result;
    // The result of fitting a model to a dataset, typically by estimating model parameters.

    class model : public plugin {
        public:
            std::string type(void) override { return "Model"; }

            virtual std::unique_ptr<model_result> fit(const std::vector<time_series>&, rng*) = 0;

            static std::string default_lib_directory(void) {
                return GOF_MODEL_PLUGIN_DIRECTORY;
            }
    };


    class model_result : public plugin {    // "plugin" now no longer describes this abstract class. Perhaps a re-factor to be more descriptive?
        protected:
            model_result(std::size_t process_dimension_in): process_dimension(process_dimension_in) { }

            virtual double estimate_implementation(std::size_t, double) = 0;
            const std::size_t process_dimension {};

        public:
            std::string type(void) override { return "Model Result"; }

            //double estimate(std::size_t i , double t);
            //Estimate the ith feature of the process at time t.

            //std::vector<double> estimate(const std::vector<std::size_t>& i, double t);
            // Estimate the features of the process with indices in i at time t.
            
            //std::vector<double> estimate(double t);
            // Estimate all features of the process at time t.

            //time_series estimate(std::size_t i, std::vector<double> t);
            // Estimate the ithe feature of the process at the times in t.

            //std::vector<time_series> estimate(const std::vector<std::size_t>& i, std::vector<double> t);
            // Estimate the features of the process that are in i at the times in t.

            //std::vector<time_series> estimate(std::vector<double> t);
            // Estimate all features of the process at the times in t.
            
            //std::vector<time_series> estimate(const std::vector<std::size_t>& i, std::vector<std::vector<double>> t);
            // Estimate the features in i at the times in the corresponding vector in t.

            //std::vector<time_series> estimate(std::vector<std::vector<double>> t);
            // Estimate all features at the times in their corresponding vector in t.

            //std::vector<time_series> estimate(const std::vector<time_series>& dataset);
            // Get estimates of each feature of the process at the same times as their corresponding observations.

            //time_series residuals(std::size_t i, const time_series& dataset);
            // Returns the residuals of feature i against the dataset.

            //std::vector<time_series> residuals(const std::vector<std::size_t>& i, const std::vector<time_series>& dataset);
            // Returns the residuals of the features in i with the corresponding time_series in dataset.

            //std::vector<time_series> residuals(const std::vector<time_series>& dataset);
            // Returns the residuals of each observation of each feature.

            //double sum_of_squared_residuals(std::size_t i, const time_series& dataset);
            // Returns the sum of squared residuals of feature i.

            //std::vector<double> sum_of_squared_residuals(const std::vector<time_series>& dataset);
            // Retrusn the sum of squared residuals of each feature.


            double estimate(std::size_t i, double t) {
                // Estimate the ith feature of the process at time t.

                if (i >= process_dimension) {
                    std::ostringstream ss;
                    ss << "In model_result::estimate(double t, size_t i): i >= process_dimension "
                          "(" << i << " >= " << process_dimension << ")";
                    throw std::invalid_argument(ss.str());
                }

                return estimate_implementation(i, t);
            }

            std::vector<double> estimate(const std::vector<std::size_t>& i, double t) {
                // Estimate the features of the process with indices in i at time t.

                std::vector<double> estimated_values;
                estimated_values.reserve(i.size());
                for (auto iter =  i.cbegin(); iter != i.cend(); ++iter) {
                    estimated_values.emplace_back(estimate(*iter, t));
                }

                return estimated_values;
            }

            std::vector<double> estimate(double t) {
                // Estimate all features of the process at time t.

                std::vector<double> estimated_values(process_dimension);
                for (std::size_t i = 0; i != process_dimension; ++i) {
                    estimated_values[i] = estimate(i, t);
                }

                return estimated_values;
            }

            time_series estimate(std::size_t i, std::vector<double> t) {
                // Estimate the ith feature of the process at the times in t.

                std::vector<double> estimated_values;
                estimated_values.reserve(t.size());
                for (auto iter = t.cbegin(); iter != t.cend(); ++iter) {
                    estimated_values.emplace_back(estimate(i, *iter));
                }

                return time_series{std::move(estimated_values), std::move(t)};
            }

            std::vector<time_series> estimate(const std::vector<std::size_t>& i, std::vector<double> t) {
                // Estimate the features of the process that are in i at the times in t.

                std::vector<time_series> estimated_values;
                estimated_values.reserve(i.size());

                for (auto iter_i = i.cbegin();;) {
                    std::vector<double> estimated_values_i;
                    estimated_values_i.reserve(t.size());

                    for (auto iter_t = t.cbegin(); iter_t != t.cend(); ++iter_t) {
                        estimated_values_i.emplace_back(estimate(*iter_i, *iter_t));
                    }
                    
                    // For final feature, move construct t and break, otherwise copy construct t.
                    if (++iter_i != i.cend()) {
                        estimated_values.emplace_back(std::move(estimated_values_i), t);
                    } else {
                        estimated_values.emplace_back(std::move(estimated_values_i), std::move(t));
                        break;
                    }
                }

                return estimated_values;
            }

            std::vector<time_series> estimate(std::vector<double> t) {
                // Estimate all features of the process at the times in t.

                std::vector<time_series> estimated_values;
                estimated_values.reserve(process_dimension);

                for (std::size_t i = 0;;) {
                    std::vector<double> estimated_values_i;
                    estimated_values_i.reserve(t.size());

                    for (auto iter_t = t.cbegin(); iter_t != t.cend(); ++iter_t) {
                        estimated_values_i.emplace_back(estimate(i, *iter_t));
                    }

                    // For final feature, move construct t and break, otherwise copy construct t.
                    if (++i != process_dimension) {
                        estimated_values.emplace_back(std::move(estimated_values_i), t);
                    } else {
                        estimated_values.emplace_back(std::move(estimated_values_i), std::move(t));
                        break;
                    }
                }

                return estimated_values;
            }

            std::vector<time_series> estimate(const std::vector<std::size_t>& i, std::vector<std::vector<double>> t) {
                // Estimate the features in i at the times in the corresponding vector in t.

                if (t.size() != i.size()) {
                    throw std::invalid_argument("model_result::estimate(vec<vec<dbl>> t, vec<sz> i) has t.sz() != i.sz()");
                }

                std::vector<time_series> estimated_values;
                estimated_values.reserve(i.size());

                for (std::size_t j = 0; j != i.size(); ++j) {
                    auto ij = i[j];
                    std::vector<double> estimated_values_i;
                    estimated_values_i.reserve(t.size());

                    for (auto iter_t = t[j].cbegin(); iter_t != t[j].cend(); ++iter_t) {
                        estimated_values_i.emplace_back(estimate(ij, *iter_t));
                    }

                    estimated_values.emplace_back(std::move(estimated_values_i), std::move(t[j]));
                }

                return estimated_values;
            }

            std::vector<time_series> estimate(std::vector<std::vector<double>> t) {
                // Estimate all features at the times in their corresponding vector in t.

                if (t.size() > process_dimension) {
                    throw std::invalid_argument("model_result::estimate(vec<vec<dbl>> t) has t.sz() > process_dimension");
                }

                std::vector<time_series> estimated_values;
                estimated_values.reserve(process_dimension);

                for (std::size_t i = 0; i != t.size(); ++i) {
                    std::vector<double> estimated_values_i;
                    estimated_values_i.reserve(t[i].size());

                    for (auto iter_t = t[i].cbegin(); iter_t != t[i].cend(); ++iter_t) {
                        estimated_values_i.emplace_back(estimate(i, *iter_t));
                    }

                    estimated_values.emplace_back(std::move(estimated_values_i), std::move(t[i]));
                }

                return estimated_values;
            }

            std::vector<time_series> estimate(const std::vector<time_series>& dataset) {
                // Get estimates of each feature of the process at the same times as their corresponding observations.

                if (dataset.size() != process_dimension) {
                    throw std::invalid_argument("model_results::estimate(vec<time_series>) has dataset.sz() != process_dimension");
                }

                std::vector<time_series> estimated_values;
                estimated_values.reserve(process_dimension);

                for (std::size_t i = 0; i != process_dimension; ++i) {
                    std::vector<double> estimated_values_i;
                    estimated_values_i.reserve(dataset[i].t.size());

                    for (auto iter_t = dataset[i].t.cbegin(); iter_t != dataset[i].t.cend(); ++iter_t) {
                        estimated_values_i.emplace_back(estimate(i, *iter_t));
                    }

                    estimated_values.emplace_back(std::move(estimated_values_i), dataset[i].t);
                }

                return estimated_values;
            }

            time_series residuals(std::size_t i, const time_series& dataset) {
                // Returns the residuals of feature i against the dataset.

                std::vector<double> residuals_i;
                residuals_i.reserve(dataset.t.size());

                auto estimates = estimate(i, dataset.t);

                for (std::size_t j = 0; j != dataset.t.size(); ++j) {
                    residuals_i.emplace_back(dataset.y[j] - estimates.y[j]);
                }

                return time_series(std::move(residuals_i), dataset.t);
            }

            std::vector<time_series> residuals(const std::vector<std::size_t>& i, const std::vector<time_series>& dataset) {
                // Returns the residuals of the features in i with the corresponding time_series in dataset.

                if (dataset.size() != i.size()) {
                    throw std::invalid_argument("model_result::residuals(vec<sz> i, vec<time_series> dset) has dset.sz() != i.sz()");
                }

                std::vector<time_series> residuals_vec;
                residuals_vec.reserve(dataset.size());

                for (std::size_t ii = 0; ii != dataset.size(); ++ii) {
                    std::size_t model_idx = i[ii];
                    std::size_t dataset_idx = ii;
                    residuals_vec.emplace_back(residuals(model_idx, dataset[dataset_idx]));
                }

                return residuals_vec;

            }

            std::vector<time_series> residuals(const std::vector<time_series>& dataset) {
                // Returns the residuals of each observation of each feature.

                if (dataset.size() != process_dimension) {
                    throw std::invalid_argument("model_results::residual(vec<time_series>) has dataset.sz() != process_dimension");
                }

                std::vector<time_series> residuals_vec;
                residuals_vec.reserve(process_dimension);

                for (std::size_t i = 0; i != dataset.size(); ++i) {
                    residuals_vec.emplace_back(residuals(i, dataset[i]));
                }

                return residuals_vec;

            }

            double sum_of_squared_residuals(std::size_t i, const time_series& dataset) {
                // Returns the sum of squared residuals of feature i.
                
                auto ssr_summer = make_kahan_summer(0.0);

                for (std::size_t j = 0; j != dataset.t.size(); ++j) {
                    auto residual = dataset.y[j] - estimate(i, dataset.t[j]);
                    ssr_summer.add(residual*residual);
                }

                return ssr_summer.get_sum();
            }

            std::vector<double> sum_of_squared_residuals(const std::vector<time_series>& dataset) {
                // Returns the sum of squared residuals of each feature.

                if (dataset.size() != process_dimension) {
                    throw std::invalid_argument("model_results::sum_of_squared_residuals(vec<time_series>) has dataset.sz() != process_dimension");
                }

                std::vector<double> ssr_vec;
                ssr_vec.reserve(process_dimension);

                for (std::size_t i = 0; i != process_dimension; ++i) {
                    ssr_vec.emplace_back(sum_of_squared_residuals(i, dataset[i]));
                }

                return ssr_vec;
            }
    };
}

#endif
