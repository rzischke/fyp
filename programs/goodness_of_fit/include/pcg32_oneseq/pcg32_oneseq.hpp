#ifndef GOF_PCG32_ONESEQ_GUARD
#define GOF_PCG32_ONESEQ_GUARD

#include "rng.hpp"
#include "pcg_random.hpp"
#include <string>

namespace gof {
    
    class pcg32_oneseq : public rng {
        public:
            pcg32_oneseq();
            explicit pcg32_oneseq(::pcg32_oneseq::state_type);

            std::string name(void) override;
            std::string description(void) override;
            uint32_t operator()(void) override;
            uint32_t min(void) override;
            uint32_t max(void) override;
            std::string seed(void) override;

        private:
            ::pcg32_oneseq::state_type pcg32_oneseq_seed;
            ::pcg32_oneseq pcg32_oneseq_rng;
    };

}

#endif
