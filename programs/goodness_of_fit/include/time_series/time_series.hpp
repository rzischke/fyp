#ifndef GOF_TIME_SERIES
#define GOF_TIME_SERIES

#include <utility>
#include <vector>

namespace gof {

    struct time_series {
        time_series() = default;

        time_series(
            std::vector<double> y_in,
            std::vector<double> t_in
        ):
            y(std::move(y_in)),
            t(std::move(t_in))
        { }

        void sort(void);

        std::vector<double> y;
        std::vector<double> t;
    };

}

#endif
