#ifndef GOF_DATASET_GUARD
#define GOF_DATASET_GUARD

#include "plugin.hpp"
#include "rng.hpp"
#include "time_series.hpp"
#include <string>
#include <utility>
#include <vector>

namespace gof {

    class dataset : public plugin {
        public:
            std::string type(void) override { return std::string("Dataset"); }
            virtual std::vector<time_series> get_data(gof::rng*) = 0;

            static std::string default_lib_directory(void) {
                return GOF_DATASET_PLUGIN_DIRECTORY;
            }
    };

}

#endif
