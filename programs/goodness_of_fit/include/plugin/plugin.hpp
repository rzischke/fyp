#ifndef GOF_PLUGIN_GUARD
#define GOF_PLUGIN_GUARD

#include <dlfcn.h>
#include <exception>
#include <functional>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

#include <boost/filesystem.hpp>

namespace gof {

    class dlopen_error: public std::runtime_error {
        public:
            explicit dlopen_error(const std::string& what_arg): std::runtime_error(what_arg) { }
            explicit dlopen_error(const char* what_arg): std::runtime_error(what_arg) { }
    };

    class dlsym_error: public std::runtime_error {
        public:
            explicit dlsym_error(const std::string& what_arg): std::runtime_error(what_arg) { }
            explicit dlsym_error(const char* what_arg): std::runtime_error(what_arg) { }
    };

    class plugin {
        public:
            virtual std::string type(void) = 0;
            virtual std::string name(void) = 0;
            virtual std::string description(void) = 0;

            virtual std::vector<std::string> latex_dependencies(void) {
                std::vector<std::string> empty = {};
                return empty;
            }

            virtual ~plugin() { };
    };

    template<class T>
    class plugin_loader {
        private:
            using factory_return_type = std::unique_ptr<T>;
            using factory_type = factory_return_type (*)(int, const char * const []);

        public:
            plugin_loader(): library_handle(nullptr), factory_fn_ptr(nullptr) { }
            
            explicit plugin_loader(const std::string& file_path): library_handle(nullptr), factory_fn_ptr(nullptr) { load(file_path); }
            
            explicit plugin_loader(
                const std::string& file_path,
                const std::string& plugin_default_directory
            ):
                library_handle(nullptr),
                factory_fn_ptr(nullptr)
            {
                load(file_path, plugin_default_directory);
            }

            plugin_loader(const plugin_loader&) = delete;
            plugin_loader& operator=(const plugin_loader&) = delete;

            plugin_loader(plugin_loader&& rhs):
                library_handle(rhs.library_handle),
                factory_fn_ptr(rhs.factory_fn_ptr)
            {
                rhs.library_handle = nullptr;
                rhs.factory_fn_ptr = nullptr;
            }
            plugin_loader& operator=(plugin_loader&&) = delete;

            ~plugin_loader() {
                if (library_handle != nullptr) unload();
            }

            void load(const std::string& cmd_arg_file_path) {
                if (library_handle != nullptr) {
                    throw std::logic_error("A gof::plugin_loader was double loaded.");
                } else {
                    auto file_path = get_plugin_path(cmd_arg_file_path, T::default_lib_directory());
                    library_handle = dlopen(file_path.c_str(), RTLD_LAZY);
                    auto error_message = dlerror();
                    if (error_message != nullptr) {
                        throw dlopen_error(error_message);
                    } else {
                        factory_fn_ptr = reinterpret_cast<factory_type>(dlsym(library_handle, "gof_plugin_factory"));
                        error_message = dlerror();
                        if (error_message != nullptr) {
                            unload();
                            throw dlsym_error(error_message);
                        }
                    }
                }
            }

            void unload(void) {
                auto dlclose_error = dlclose(library_handle);
                if (dlclose_error) {
                    auto error_message = dlerror();
                    if (error_message) {
                        std::cerr << "dlclose error: " << error_message << std::endl;
                    } else {
                        std::cerr << "dlclose error, no message." << std::endl;
                    }
                    std::terminate(); // Unload is a destructor operation, and an error leaves
                                      // a plugin_loader in a perminantly invalid and unrecoverable
                                      // state, so terminate.
                }
            }

            template <class... Args>
            factory_return_type factory(Args&&... args) {
                return factory_fn_ptr(std::forward<Args>(args)...);
            }

        private:
            void *library_handle;
            factory_type factory_fn_ptr;

            std::string get_plugin_path(const std::string& file_path, const std::string& plugin_default_directory) {
                // 1. If file_path has a parent path (i.e. the path contains directories), then return file_path
                //    Otherwise, continue to 2.
                // 2. Return the path for the first of the following files that exists
                //    and is an ordinary file. If none exist, continue to 3. Replace
                //    <file_path> with the contents of the file_path passed to this function, and
                //    replace <plugin_default_directory> likewise.
                //    a. <file_path>
                //    b. lib<file_path>.so
                //    c. <plugin_default_directory>/<file_path>
                //    d. <plugin_default_directory>/lib<file_path>.so
                // 3. Return <file_path>.

                namespace fs = boost::filesystem;

                fs::path boost_file_path(file_path);

                if (fs::is_regular_file(boost_file_path) || boost_file_path.has_parent_path()) {
                    return file_path;
                } else {
                    auto file_path_lib_so = std::string("lib") + file_path + ".so";
                    fs::path boost_file_path_lib_so(file_path_lib_so);
                    if (fs::is_regular_file(boost_file_path_lib_so)) {
                        return boost_file_path_lib_so.native();
                    } else {
                        fs::path boost_plugin_default_directory(plugin_default_directory);
                        fs::path boost_file_path_designated = boost_plugin_default_directory/file_path;
                        if (fs::is_regular_file(boost_file_path_designated)) {
                            return boost_file_path_designated.native();
                        } else {
                            fs::path boost_file_path_designated_lib_so = boost_plugin_default_directory/file_path_lib_so;
                            if (fs::is_regular_file(boost_file_path_designated_lib_so)) {
                                return boost_file_path_designated_lib_so.native();
                            } else {
                                return file_path;
                            }  // boost_file_path_designated_lib_so - else
                        }  // boost_file_path_designated - else
                    }  // boost_file_path_lib_so - else
                }  // boost_file_path - else

            }  // search_and_load(const std::string&, const std::string&)

    };
}

#endif
