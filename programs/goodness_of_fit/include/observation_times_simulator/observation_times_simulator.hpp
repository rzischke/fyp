#include "plugin.hpp"
#include "rng.hpp"
#include <string>
#include <vector>

namespace gof {

    class observation_times_simulator : public plugin {
        public:
            std::string type(void) { return std::string("Observation Times Simulator"); }

            virtual std::vector<std::vector<double>> simulate(gof::rng*) = 0;

            static std::string default_lib_directory(void) {
                return GOF_OBSERVATION_TIMES_SIMULATOR_PLUGIN_DIRECTORY;
            }
    };

}

