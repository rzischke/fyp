#ifndef GOF_REPORT_GUARD
#define GOF_REPORT_GUARD

#include "plugin.hpp"
#include "rng.hpp"
#include "dataset.hpp"
#include "model.hpp"

namespace gof {
    
    class report : public plugin {
        public:
            std::string type(void) override { return std::string("Report"); }

            virtual void make(dataset*, model*, rng*) = 0;

            static std::string default_lib_directory(void) {
                return GOF_REPORT_PLUGIN_DIRECTORY;
            }
    };

}

#endif

