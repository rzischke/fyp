#ifndef GOF_RNG_GUARD
#define GOF_RNG_GUARD

#include "plugin.hpp"
//#include "pcg_random/pcg_random.hpp"
#include <cstdint>
#include <iostream>
#include <limits>
#include <memory>
#include <random>
#include <sstream>
#include <string>

namespace gof {

    class rng : public plugin {
        public:
            std::string type(void) override { return std::string("Random Number Generator"); }
            virtual uint32_t operator()(void) = 0;
            virtual uint32_t min(void) = 0;
            virtual uint32_t max(void) = 0;
            virtual std::string seed(void) = 0;

            static std::string default_lib_directory(void) {
                return GOF_RNG_PLUGIN_DIRECTORY;
            }

        protected:
            
            uint64_t generate_64bit_seed(void) {
                std::random_device rd;
                auto seed64 = static_cast<uint64_t>(rd());
                if (rd.max() != std::numeric_limits<uint64_t>::max()) {
                    seed64 |= (static_cast<uint64_t>(rd()) << 32);
                }
                return seed64;
            }
    };

}

//#include "pcg32_oneseq.hpp"

//namespace gof {
//    using default_rng = pcg32_oneseq;
//}

#endif

