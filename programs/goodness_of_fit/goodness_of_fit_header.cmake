cmake_minimum_required( VERSION 3.5 )

set( gof_namespace "gof::" PARENT_SCOPE )
set( gof_namespace "gof::" )

function( gof_plugin_dir_compiler_def_name target output )
	string( FIND "${target}" "${gof_namespace}" NAMESPACE_LOC )
	if( "${NAMESPACE_LOC}" GREATER -1 )
    	string( REGEX REPLACE "${gof_namespace}" "" target_trimmed "${target}")
	else()
		set( target_trimmed "${target}" )
	endif()
    string( TOUPPER "${target_trimmed}" TARGET )
    set( ${output} "GOF_${TARGET}_PLUGIN_DIRECTORY" PARENT_SCOPE )
endfunction()

macro( install_gof_plugin abstract_plugin_target concrete_plugin_target )
    get_target_property(
        GOF_PLUGIN_COMPILE_DEFINITIONS
        "${abstract_plugin_target}"
        INTERFACE_COMPILE_DEFINITIONS
    )

    gof_plugin_dir_compiler_def_name( "${abstract_plugin_target}" DEF_NAME )
	
    string( REGEX MATCH
        "${DEF_NAME}=\"(.*)\""
        DUMMY_VAR
        "${GOF_PLUGIN_COMPILE_DEFINITIONS}"
    )
    set( GOF_CONCRETE_PLUGIN_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_MATCH_1}" )

    if( GOF_CONCRETE_PLUGIN_LIBRARY_OUTPUT_DIRECTORY )
        install( TARGETS "${concrete_plugin_target}"
            LIBRARY DESTINATION "${GOF_CONCRETE_PLUGIN_LIBRARY_OUTPUT_DIRECTORY}"
        )
    else()
        message( FATAL_ERROR "${DEF_NAME} compiler definition not found for target ${abstract_plugin_target}" )
    endif()
endmacro()

