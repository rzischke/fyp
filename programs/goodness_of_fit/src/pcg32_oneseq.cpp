#include "cli_utilities.hpp"
#include "pcg32_oneseq.hpp"
#include "rng.hpp"
#include <iostream>
#include <memory>
#include <boost/optional.hpp>

#include "pcg32_oneseq_static.cpp"

#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"

extern "C" std::unique_ptr<gof::rng> gof_plugin_factory(int argc, const char * const argv[]) {
    const char *lib_file_name = argv[0];

    auto print_help = [lib_file_name]() {
        std::cout <<
            lib_file_name << " contains a PCG random number generator.\n"
            "Usage: " << lib_file_name << " [-s/--seed <64bit-seed>]" << std::endl;
    };

    boost::optional<uint64_t> seed;
    if (argc >= 3) { int i = 1; gof::check_and_get_option(seed, i, argc, argv, "-s", "--seed", print_help); }

    if (seed) return std::make_unique<gof::pcg32_oneseq>(*seed);
    else return std::make_unique<gof::pcg32_oneseq>();
}
