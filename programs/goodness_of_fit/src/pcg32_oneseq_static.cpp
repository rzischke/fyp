#include "pcg32_oneseq.hpp"
#include "pcg_random.hpp"
#include <sstream>
#include <string>

namespace gof {

    pcg32_oneseq::pcg32_oneseq(): pcg32_oneseq_seed(static_cast<::pcg32_oneseq::state_type>(generate_64bit_seed())), pcg32_oneseq_rng(pcg32_oneseq_seed) { }
    pcg32_oneseq::pcg32_oneseq(::pcg32_oneseq::state_type seed_in): pcg32_oneseq_seed(seed_in), pcg32_oneseq_rng(seed_in) { }

    std::string pcg32_oneseq::name(void) { return std::string("pcg32_oneseq"); }

    std::string pcg32_oneseq::description(void) {
        std::ostringstream ss;
            ss << "A permuted Congruential Generator (PCG) with a 32 bit output and one stream. "
                  "Seeded by \"" << seed() << "\". "
                  "See \"http://www.pcg-random.org/\".";
        return ss.str();
    }

    uint32_t pcg32_oneseq::operator()(void) { return static_cast<uint32_t>(pcg32_oneseq_rng()); }
    uint32_t pcg32_oneseq::min(void) { return static_cast<uint32_t>(pcg32_oneseq_rng.min()); }
    uint32_t pcg32_oneseq::max(void) { return static_cast<uint32_t>(pcg32_oneseq_rng.max()); }

    std::string pcg32_oneseq::seed(void) {
        std::ostringstream ss;
        ss << pcg32_oneseq_seed;
        return ss.str();
    }

}

