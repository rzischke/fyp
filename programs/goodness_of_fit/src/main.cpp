#include "cli_utilities.hpp"
#include "plugin.hpp"
#include "dataset.hpp"
#include "report.hpp"
#include "model.hpp"
#include "rng.hpp"
#include "pcg32_oneseq.hpp"
#include <boost/optional.hpp>
#include <iostream>
#include <memory>

int main(int argc, char *argv[]) {
    const char *cmd_name = argv[0];

    auto print_help = [cmd_name]() {
        std::cout <<
            cmd_name << " produces a report on the goodness of fit of a model to a dataset.\n"
            "Usage: " << cmd_name << " --dataset <gof::dataset .so and options> --model <gof::model .so and options> --report <gof::report .so and options> [[--rng <gof::rng .so and options>] OR [--seed <64-bit rng seed>]" << std::endl;
    };

    gof::assert_argc_geq(argc, 5, cmd_name, print_help);

    boost::optional<std::string> dataset_plugin_libname;
    gof::plugin_loader<gof::dataset> dataset_plugin_loader;
    std::unique_ptr<gof::dataset> dataset_plugin;

    boost::optional<std::string> model_plugin_libname;
    gof::plugin_loader<gof::model> model_plugin_loader;
    std::unique_ptr<gof::model> model_plugin;

    boost::optional<std::string> report_plugin_libname;
    gof::plugin_loader<gof::report> report_plugin_loader;
    std::unique_ptr<gof::report> report_plugin;

    boost::optional<std::string> rng_plugin_libname;
    gof::plugin_loader<gof::rng> rng_plugin_loader;
    std::unique_ptr<gof::rng> rng_plugin;

    boost::optional<pcg32_oneseq::state_type> seed;

    auto option_indices = gof::get_option_indices(argc, argv, {"--dataset", "--model", "--report", "--rng", "--seed"});
    auto get_argc_nested = [&option_indices,argc](int i, std::size_t ii) { return (ii+1==option_indices.size()) ? (argc - i) : (option_indices[ii+1] - i); };

    for (std::size_t ii = 0; ii != option_indices.size(); ++ii) {
        auto i = option_indices[ii];
        if (gof::check_and_get_option(dataset_plugin_libname, i, argc, argv, "", "--dataset", print_help)) {
            auto argc_nested = get_argc_nested(i,ii);
            dataset_plugin_loader.load(*dataset_plugin_libname);
            dataset_plugin = dataset_plugin_loader.factory(argc_nested, &argv[i]);
        } else if (gof::check_and_get_option(model_plugin_libname, i, argc, argv, "", "--model", print_help)) {
            auto argc_nested = get_argc_nested(i,ii);
            model_plugin_loader.load(*model_plugin_libname);
            model_plugin = model_plugin_loader.factory(argc_nested, &argv[i]);
        } else if (gof::check_and_get_option(report_plugin_libname, i, argc, argv, "", "--report", print_help)) {
            auto argc_nested = get_argc_nested(i,ii);
            report_plugin_loader.load(*report_plugin_libname);
            report_plugin = report_plugin_loader.factory(argc_nested, &argv[i]);
        } else if (gof::check_and_get_option(rng_plugin_libname, i, argc, argv, "", "--rng", print_help)) {
            auto argc_nested = get_argc_nested(i,ii);
            rng_plugin_loader.load(*rng_plugin_libname);
            rng_plugin = rng_plugin_loader.factory(argc_nested, &argv[i]);
        } else if (gof::check_and_get_option(seed, i, argc, argv, "", "--seed", print_help)) {
            // Seed is parsed into seed boost option in if condition, handle afterwards.
        }
    }

    gof::assert_arg_set(dataset_plugin_libname, "--dataset", cmd_name, print_help);
    gof::assert_arg_set(model_plugin_libname, "--model", cmd_name, print_help);
    gof::assert_arg_set(report_plugin_libname, "--report", cmd_name, print_help);

    if (!rng_plugin_libname) {
        if (seed) {
            rng_plugin = std::make_unique<gof::pcg32_oneseq>(*seed);
        } else {
            rng_plugin = std::make_unique<gof::pcg32_oneseq>();
        }
    }

    report_plugin->make(dataset_plugin.get(),model_plugin.get(), rng_plugin.get());

    std::cout << "About to exit" << std::endl;

    return EXIT_SUCCESS;
}
