cmake_minimum_required( VERSION 3.5 )

include( "${CMAKE_CURRENT_SOURCE_DIR}/goodness_of_fit_header.cmake" )

set( gof_include_prefix "include/goodness_of_fit" )
set( gof_library_prefix "lib/goodness_of_fit" )

add_subdirectory( include )

add_subdirectory( src )

install( EXPORT goodness_of_fit_targets
    FILE goodness_of_fit_targets.cmake
    NAMESPACE "${gof_namespace}"
    DESTINATION lib/cmake/goodness_of_fit
)

install(
    FILES
        goodness_of_fit_header.cmake
        goodness_of_fit-config.cmake
    DESTINATION
        lib/cmake/goodness_of_fit
)

