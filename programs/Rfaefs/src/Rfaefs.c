#include <complex.h>
#include <string.h> // memcpy

#include <R.h>
#include <Rinternals.h>
#include <R_ext/Print.h>

#include "faefs.h"

extern SEXP Rfaefs(SEXP Rf0, SEXP Rt, SEXP Ry, SEXP Ren_eps_abs, SEXP Ren_eps_rel) {
    //SEXP t_real = PROTECT(coerceVector(t, REALSXP));
    //SEXP y_real = PROTECT(coerceVector(y, REALSXP));
    // Do coercion and error checking in calling R code.

    double f0 = REAL(Rf0)[0];
    double *y = REAL(Ry);
    double *t = REAL(Rt);
    size_t num_obs = (size_t)(length(Rt));
    double _Complex *f;
    size_t *h;
    size_t num_sig_freq;
    double *P;
    size_t P_size;
    double p_crit;
    double en_eps_abs = REAL(Ren_eps_abs)[0];
    double en_eps_rel = REAL(Ren_eps_rel)[0];

    faefs(
        f0,
        y,
        t,
        num_obs,
        &f,
        &h,
        &num_sig_freq,
        &P,
        &P_size,
        &p_crit,
        en_eps_abs,
        en_eps_rel
    );

    // Allocate return value memory, it is an R list type.
    SEXP ans = PROTECT(allocVector(VECSXP,9));

    // Set the class attribute to faefs.
    SEXP class = PROTECT(allocVector(STRSXP, 1));
    SET_STRING_ELT(class, 0, mkChar("faefs"));
    classgets(ans, class);

    // Name items of R list.
    SEXP ans_names = PROTECT(allocVector(STRSXP, 9));
    SET_STRING_ELT(ans_names, 0, mkChar("fundamental.frequency"));
    SET_STRING_ELT(ans_names, 1, mkChar("t"));
    SET_STRING_ELT(ans_names, 2, mkChar("y"));
    SET_STRING_ELT(ans_names, 3, mkChar("harmonics"));
    SET_STRING_ELT(ans_names, 4, mkChar("fourier.coefficients"));
    SET_STRING_ELT(ans_names, 5, mkChar("negative.log10.false.alarm.probabilities"));
    SET_STRING_ELT(ans_names, 6, mkChar("negative.log10.critical.probability"));
    SET_STRING_ELT(ans_names, 7, mkChar("epsilon.absolute"));
    SET_STRING_ELT(ans_names, 8, mkChar("epsilon.relative"));
    setAttrib(ans, R_NamesSymbol, ans_names);

    // Set fundamental.frequency.
    SET_VECTOR_ELT(ans, 0, Rf0);

    // Set t.
    SET_VECTOR_ELT(ans, 1, Rt);

    // Set y.
    SET_VECTOR_ELT(ans, 2, Ry);

    // Set harmonics.
    SEXP Rharmonic = PROTECT(allocVector(INTSXP, (int)num_sig_freq));
    int *r_harmonic = INTEGER(Rharmonic);
    for (size_t i = 0; i != num_sig_freq; ++i) {
        r_harmonic[i] = (int)h[i];
    }
    SET_VECTOR_ELT(ans, 3, Rharmonic);

    // Set fourier.coefficients.
    SEXP RfourierCoefficients = PROTECT(allocVector(CPLXSXP, (int)num_sig_freq));
    Rcomplex *r_fourier_coefficients = COMPLEX(RfourierCoefficients);
    for (size_t i = 0; i != num_sig_freq; ++i) {
        r_fourier_coefficients[i].r = creal(f[i]);
        r_fourier_coefficients[i].i = cimag(f[i]);
    }
    SET_VECTOR_ELT(ans, 4, RfourierCoefficients);

    // Set negative.log10.false.alarm.probabilities.
    SEXP RnegativeLog10FalseAlarmProbabilities = PROTECT(allocVector(REALSXP, (int)P_size));
    double *r_negative_log_10_false_alarm_probabilities = REAL(RnegativeLog10FalseAlarmProbabilities);
    memcpy(r_negative_log_10_false_alarm_probabilities, P, P_size*sizeof(double));
    SET_VECTOR_ELT(ans, 5, RnegativeLog10FalseAlarmProbabilities);

    // Set negative.log10.critical.probability.
    SEXP RnegativeLog10CriticalProbability = PROTECT(allocVector(REALSXP, 1));
    REAL(RnegativeLog10CriticalProbability)[0] = p_crit;
    SET_VECTOR_ELT(ans, 6, RnegativeLog10CriticalProbability);

    // Set epsilon.absolute.
    SET_VECTOR_ELT(ans, 7, Ren_eps_abs);

    // Set epsilon.relative.
    SET_VECTOR_ELT(ans, 8, Ren_eps_rel);

    /* Unprotect
     * ans
     * class
     * ans_names
     * Rharmonic
     * RfourierCoefficients
     * RnegativeLog10FalseAlarmProbabilities
     * RnegativeLog10CriticalProbability
     */
    UNPROTECT(7);

    // Free faefs C++ memory.
    faefs_free(f, h, P);

    return ans;
}

