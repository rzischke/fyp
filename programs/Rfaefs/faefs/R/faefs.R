.onLoad <- function(libname, pkgname) {
    dyn.load("/usr/local/lib/libRfaefs.so", local = FALSE)
}

.onUnload <- function(dir) {
    dyn.unload("/usr/local/lib/libRfaefs.so")
}

faefs <- function(fundamental.frequency, t, y, epsilon.absolute = 1e-6, epsilon.relative = 1e-2) {
    fundamental.frequency.coerced <- as.numeric(fundamental.frequency)
    t.coerced <- as.numeric(t)
    y.coerced <- as.numeric(y)
    epsilon.absolute.coerced <- as.numeric(epsilon.absolute)
    epsilon.relative.coerced <- as.numeric(epsilon.relative)

    if (length(t) != length(y)) {
        stop("t and y must be identical sized vectors")
    }

    return(.Call(
        "Rfaefs",
        fundamental.frequency.coerced,
        t.coerced,
        y.coerced,
        epsilon.absolute.coerced,
        epsilon.relative.coerced,
        PACKAGE = "libRfaefs"
    ));
}

predict.faefs <- function(model, new_t) {
    if (missing(new_t)) {
        new_t <- model$t
    }

    if (!identical(class(model)[1],"faefs")) {
        stop("The first argument to predict.faefs must be an object of class faefs.")
    }

    f0 <- model$fundamental.frequency

    f <- matrix(
        model$fourier.coefficients,
        nrow = length(model$fourier.coefficients),
        ncol = 1
    )

    h <- model$harmonics

    t <- new_t

    return(Re(exp(-2i*pi*f0*(t %o% h)) %*% f))
}

