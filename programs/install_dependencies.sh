#!/bin/bash

# Installs dependencies for goodness_of_fit on an ubuntu based linux distro.
# TODO: more fine-grained requirements on boost and vtk, rather than just
# grabbing everything.

if [ ! -f ./install_dependencies.sh ]; then
    echo "install_dependencies.sh not run from its containing directory, aborting."
    exit 1
fi

REPO_OS="ubuntu xenial"

sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
sudo add-apt-repository "deb [arch=amd64,i386] https://cloud.r-project.org/bin/linux/${REPO_OS}/"

sudo apt-get update

sudo apt-get install \
    cmake \
    build-essential \
    libboost-all-dev \
    libeigen3-dev \
    libvtk6-dev \
    libproj-dev \
    libsoci-dev \
    texlive-full \
    libfftw3-dev \
    libnfft3-dev \
    r-base \
    r-base-dev

git clone https://bitbucket.org/rzischke/chartpp.git
git clone https://github.com/vmorariu/figtree.git
