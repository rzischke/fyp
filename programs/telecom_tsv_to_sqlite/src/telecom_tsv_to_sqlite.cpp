#include <algorithm>
#include <cstdint>
#include <cstdlib>
#include <fstream>
#include <ios>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

#include <dlib/sqlite.h>

#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

bool table_exists(dlib::database& db, const std::string& tablename) {
    return dlib::query_int(db, "SELECT count(*) FROM sqlite_master WHERE name = '" + tablename + "'")==1;
}

template<unsigned long N, class T>
void bind_and_execute(dlib::statement& st, const T& arg) {
    st.bind(N, arg);
    st.exec();
}

template<unsigned long N = 1ul, class T, class... Ts>
void bind_and_execute(dlib::statement& st, const T& arg, const Ts&... args) {
    st.bind(N, arg);
    bind_and_execute<N + 1ul>(st, args...);
}

void assert_read_only(const fs::path& pth) {
    bool publically_writable = ((fs::status(pth).permissions() & fs::perms::others_write) != fs::perms::no_perms);
    if (publically_writable) {
        std::cerr << "Abort for injection vulnerability: " << pth << " writable by world." << std::endl;
        std::exit(EXIT_FAILURE);
    }
}

void assert_not_empty(const fs::path& pth) {
    if (!fs::exists(pth) || fs::is_empty(pth)) {
        std::cerr << "Abort for " << pth << " is empty or does not exist." << std::endl;
        std::exit(EXIT_FAILURE);
    }
}

std::string get_file_contents(const fs::path& pth) {
    assert_not_empty(pth);
    assert_read_only(pth);
    std::ifstream file_handle(pth.string());
    std::istreambuf_iterator<char> begin(file_handle);
    std::istreambuf_iterator<char> end;
    return std::string(begin, end);
}

std::vector<std::string> split(const std::string& str, char delimiter) {
    std::vector<std::string> ret;

    auto ret_elem_begin = str.cbegin();
    auto ret_elem_end = std::find(ret_elem_begin, str.cend(), delimiter);
    ret.push_back(std::string(ret_elem_begin, ret_elem_end));
    ret_elem_begin = ret_elem_end;
    while (ret_elem_begin != str.cend()) {
        ret_elem_begin++;
        ret_elem_end = std::find(ret_elem_begin, str.cend(), delimiter);
        ret.push_back(std::string(ret_elem_begin, ret_elem_end));
        ret_elem_begin = ret_elem_end;
    }

    return ret;
}

void execute_from_csv_line(dlib::statement& st, const std::string& file_line, char delimiter) {
    auto parameter_begin = file_line.cbegin();
    for (size_t i = 1; true; ++i) {
        auto parameter_end = std::find(parameter_begin, file_line.cend(), delimiter);
        std::string argument = (parameter_begin==parameter_end ? std::string("NULL") : std::string(parameter_begin, parameter_end));   // Data is ommitted if its value is zero
        st.bind(i, argument);
        if (parameter_end == file_line.cend()) break;
        parameter_begin = parameter_end;
        parameter_begin++;
    }
    st.exec();
}

int main(int argc, char **argv) {
    fs::path operating_directory(argc==2 ? fs::path(argv[1]) : fs::current_path());
    if (!fs::is_directory(operating_directory)) {
        std::cerr << "Abort for " << operating_directory << " is not a directory." << std::endl;
        return EXIT_FAILURE;
    }

    fs::path db_file = operating_directory/"fyp.sqlite3";
    std::string create_table_sql = get_file_contents(operating_directory/"sql/telecom/create_table.sql");
    std::string insert_row_sql = get_file_contents(operating_directory/"sql/telecom/insert.sql");
    std::string min_time_interval_sql = get_file_contents(operating_directory/"sql/telecom/min_time_interval.sql");
    fs::path data_directory = operating_directory/"data/telecom";

    dlib::database db(db_file.c_str());

    if (table_exists(db, "telecom")) {
        std::cout << "Table already exists, take no action." << std::endl;
    } else {
        db.exec(create_table_sql);

        dlib::statement begin_transaction(db, "BEGIN TRANSACTION");
        dlib::statement end_transaction(db, "COMMIT TRANSACTION");
        dlib::statement insert_row(db, insert_row_sql);
        
        for (auto& dir_entry: fs::recursive_directory_iterator(data_directory)) {
            if (is_regular_file(dir_entry) && dir_entry.path().extension().string() == ".txt") {
                std::cout << "Importing " << dir_entry.path().filename() << "..." << std::flush;
                std::ifstream file_handle(dir_entry.path().string());
                begin_transaction.exec();
                std::string file_line;
                while (!std::getline(file_handle, file_line).eof()) {
                    execute_from_csv_line(insert_row, file_line, '\t');
                }
                end_transaction.exec();
                std::cout << " Done." << std::endl;
            }
        }
    }

    return EXIT_SUCCESS;
}
