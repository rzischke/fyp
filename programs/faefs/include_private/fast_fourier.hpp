#ifndef FAEFS_FAST_FOURIER_GUARD
#define FAEFS_FAST_FOURIER_GUARD

#include <cmath>
#include <complex>
#include <stdexcept>
#include <boost/math/constants/constants.hpp>
#include "time_series.hpp"
#include "nfft3.h"

class faefs_adjoint_nfft {  // Nonequispaced Fast Fourier Transform
    public:
        faefs_adjoint_nfft(
            std::size_t initial_harmonic_end,   // One plus the highest harmonic.
            std::size_t number_of_nodes
        ):
            harmonic_end(initial_harmonic_end)
        {
            if (initial_harmonic_end < 5) {
                // nfft3's memory allocation is broken for harmonic_end < 5.
                std::logic_error("faefs_adjoint_nfft constructor failed on: initial_harmonic_end < 5");
            }

            nfft_init_1d(
                &p,
                2*harmonic_end,
                number_of_nodes
            );
        }

        ~faefs_adjoint_nfft() {
            nfft_finalize(&p);
        }

        void reconstruct(
            std::size_t harmonic_end_in,
            std::size_t number_of_nodes
        ) {
            nfft_finalize(&p);
            nfft_init_1d(
                &p,
                2*harmonic_end_in,
                number_of_nodes
            );
            harmonic_end = harmonic_end_in;
        }

        void node_coordinates(
            double fundamental_angular_frequency,
            const std::vector<double>& t
        ) {
            static const double two_pi_inv = 1/(2*boost::math::constants::pi<double>());
            
            if (t.size() != p.M_total) {
                throw std::logic_error("node_coordinates run on faefs_adjoint_nfft object with the wrong number of nodes passed.");
            }

            double fundamental_frequency = two_pi_inv*fundamental_angular_frequency;

            for (std::size_t j = 0; j != t.size(); ++j) {
                double normalised_phase = remainder(fundamental_frequency*t[j],1.0);
                if (normalised_phase >= 0.5) normalised_phase = -0.5;
                p.x[j] = normalised_phase;
            }

            if (p.flags & PRE_ONE_PSI) {
                nfft_precompute_one_psi(&p);
            }
        }

        template<class F>
        auto fft(const F& f) {
            for (std::size_t j = 0; j != p.M_total; ++j) {
                p.f[j][0] = f(j);   // real
                p.f[j][1] = 0.0;    // imaginary
            }

            nfft_adjoint(&p);

            std::vector<std::complex<double>> transform;
            transform.reserve(harmonic_end);
            for (std::size_t harmonic = 0; harmonic != harmonic_end; ++harmonic) {
                transform.emplace_back(
                    p.f_hat[harmonic + harmonic_end][0],
                    p.f_hat[harmonic + harmonic_end][1]
                );
            }

            return transform;
        }

        template<class F>
        auto fft(
            double fundamental_angular_frequency,
            const F& f,
            std::vector<double>& t
        ) {
            node_coordinates(
                fundamental_angular_frequency,
                t
            );

            return fft(f);
        }

        auto fft(const std::vector<double>& y) {
            if (y.size() != p.M_total) {
                throw std::logic_error("fft run on faefs_adjoint_nfft object with the wrong number of nodes passed.");
            }

            return fft([&y](std::size_t j) -> double { return y[j]; });
        }

        auto fft(
            double fundamental_angular_frequency,
            const std::vector<double>& y,
            const std::vector<double>& t
        ) {
            node_coordinates(
                fundamental_angular_frequency,
                t
            );
            return fft(y);
        }

        auto fft(
            double fundamental_angular_frequency,
            const time_series& data
        ) {
            return fft(
                fundamental_angular_frequency,
                data.y,
                data.t
            );
        }

    private:
        std::size_t harmonic_end;
        nfft_plan p;
};

#endif

