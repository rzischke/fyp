#ifndef FAEFS_TIME_SERIES_GUARD
#define FAEFS_TIME_SERIES_GUARD

#include <algorithm>
#include <iterator>
#include <utility>
#include <vector>

struct time_series {
    time_series() = default;

    time_series(
        std::vector<double> y_in,
        std::vector<double> t_in
    ):
        y(std::move(y_in)),
        t(std::move(t_in))
    { }

    void sort(void) {
        // This uses unnecesary memory, the sort should be performed in place.
        // This should be changed when sorting a pair of (zipped) arrays is
        // better supported (e.g. when the ranges v3 capability is
        // standardised).

        // Setup indices array {0, 1, ... , y.size()-1}
        std::size_t i = 0;
        std::vector<std::size_t> indices;
        indices.reserve(y.size());
        std::generate_n(
            std::back_inserter(indices),
            y.size(),
            [&i]() { return i++; }
        );

        // Sort indices array based on time of observation.
        std::sort(
            indices.begin(),
            indices.end(),
            [this](std::size_t lhs, std::size_t rhs) {
                return t[lhs] < t[rhs];
            }
        );

        // Sort y.
        std::vector<double> y_new;
        y_new.reserve(y.size());
        for (auto iter = indices.begin(); iter != indices.end(); ++iter) {
            y_new.push_back(y[*iter]);
        }
        std::swap(y, y_new);

        // Sort t.
        std::sort(t.begin(), t.end());
    }

    std::vector<double> y;
    std::vector<double> t;
};

#endif

