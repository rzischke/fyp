#ifndef FAEFS_LEAST_SQUARES_GUARD
#define FAEFS_LEAST_SQUARES_GUARD

#include <algorithm>
#include <vector>

#include <Eigen/Cholesky>
#include <Eigen/Core>
#include <Eigen/LU>
#include <Eigen/SVD>

#include "dot_product_store.hpp"

template<class DerivedY, class DerivedX>
auto ordinary_least_squares_estimate_by_svd(const Eigen::MatrixBase<DerivedY>& y, const Eigen::MatrixBase<DerivedX>& X) {
    using Scalar = typename Eigen::MatrixBase<DerivedY>::Scalar;
    return static_cast<Eigen::Matrix<Scalar,Eigen::Dynamic,1>>(
        X.bdcSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(y)
    );
}

template<class DerivedGram, class DerivedXty>
auto ordinary_least_squares_estimate_by_normal_equations(
    const Eigen::MatrixBase<DerivedGram>& XtX,
    const Eigen::MatrixBase<DerivedXty>& Xty
) {
    using Scalar = typename Eigen::MatrixBase<DerivedXty>::Scalar;
    return static_cast<Eigen::Matrix<Scalar,Eigen::Dynamic,1>>(XtX.ldlt().solve(Xty));
}

template<
    class GramType,
    class DesignTNewFreqType,
    class NewFreqGramType,
    class NewGramType
>
void rolling_gram(
    const Eigen::MatrixBase<GramType>& gram,
    const Eigen::MatrixBase<DesignTNewFreqType>& design_t_new_freq,
    const Eigen::MatrixBase<NewFreqGramType>& new_freq_gram,
    Eigen::MatrixBase<NewGramType>& new_gram
) {
    new_gram.block(0, 0, gram.rows(), gram.cols()).noalias() = gram;
    new_gram.block(0, gram.cols(), design_t_new_freq.rows(), design_t_new_freq.cols()).noalias() = design_t_new_freq;
    new_gram.block(gram.rows(), 0, design_t_new_freq.cols(), design_t_new_freq.rows()).noalias() = design_t_new_freq.transpose();
    new_gram.block(gram.rows(), gram.cols(), new_freq_gram.rows(), new_freq_gram.cols()).noalias() = new_freq_gram;
}

struct rolling_gram_inverse_intermediate_memory {
    // Holds the memory for the intermediate storage necessary
    // to do the block inverse required for finding the new gram
    // inverse when adding two columns to the design matrix in
    // function "rolling_gram_inverse". This is done in particular
    // to allow for preallocating the required dynamic memory
    // before a large loop where pairs of columns (one for sin
    // and one for cos at each frequency) are added to the same
    // design matrix (for the currently pre-eminant list of
    // significant frequencies), as is done in the FAEFS
    // algorithm.

    rolling_gram_inverse_intermediate_memory(
        std::size_t current_design_matrix_cols
    ):
        A(current_design_matrix_cols, 2),
        B(),
        C(),
        D(),
        E(current_design_matrix_cols,2),
        lu()
    { }

    void resize(std::size_t current_design_matrix_cols) {
        A.resize(current_design_matrix_cols, Eigen::NoChange);
        E.resize(current_design_matrix_cols, Eigen::NoChange);
    }

    Eigen::Matrix<double, Eigen::Dynamic,2> A;
    Eigen::Matrix2d B;
    Eigen::Matrix2d C;
    Eigen::Matrix2d D;
    Eigen::Matrix<double, Eigen::Dynamic,2> E;
    Eigen::PartialPivLU<Eigen::Matrix2d> lu;

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

template<
    class GInvType,
    class DesignTNewFreqType,
    class NewFreqGramType,
    class NewGInvType
>
void rolling_gram_inverse(
    const Eigen::MatrixBase<GInvType>& gram_inverse,
    const Eigen::MatrixBase<DesignTNewFreqType>& design_t_new_freq,
    const Eigen::MatrixBase<NewFreqGramType>& new_freq_gram,
    rolling_gram_inverse_intermediate_memory& imtmem,
    Eigen::MatrixBase<NewGInvType>& new_gram_inverse
) {
    // Calculate the new_gram_inverse when the new_freq design columns
    // are appended to the right of the current design matrix. Takes
    // the block inverse of the new gram when it is expanded in terms
    // of the current gram, the current design matrix and the new
    // columns. This functionality allows the dot products
    // that make up the various matrices and matrix products to
    // be calculated in advance using a fast algorithm (in this case,
    // the FFT), improving computational complexity.

    auto& A = imtmem.A;
    auto& B = imtmem.B;
    auto& C = imtmem.C;
    auto& D = imtmem.D;
    auto& E = imtmem.E;
    auto& lu = imtmem.lu;

    A.noalias() = gram_inverse*design_t_new_freq;
    B.noalias() = design_t_new_freq.transpose()*A;
    C.noalias() = new_freq_gram - B;
    lu.compute(C);
    D.noalias() = lu.inverse();
    E.noalias() = A*D;

    new_gram_inverse.block(0, 0, gram_inverse.rows(), gram_inverse.cols()).noalias() = gram_inverse + E*A.transpose();
    new_gram_inverse.block(0, gram_inverse.cols(), E.rows(), E.cols()).noalias() = -E;
    new_gram_inverse.block(gram_inverse.rows(), 0, E.cols(), E.rows()).noalias() = -E.transpose();
    new_gram_inverse.block(gram_inverse.rows(), gram_inverse.cols(), D.rows(), D.cols()).noalias() = D;
}

void generate_gram(
    const std::vector<std::size_t>& harmonics,
    dot_product_store& dot_store,
    Eigen::MatrixXd& gram
) {
    if (harmonics.size() == 0) {
        throw std::logic_error("In generate_gram harmonics.size() == 0.");
    }

    std::size_t gram_size = (harmonics[0]==0 ? 2*harmonics.size()-1 : 2*harmonics.size());
    // We skip the gram entries which would be participated by the sin(h*w0*t) vector for h == 0,
    // since sin(0*w0*t) = 0 for all t, and this: a. provides nothing to regress onto (i.e. no
    // additional basis vector for the design matrix column vector space) and therefore b. would
    // render the gram matrix singular, breaking the regression on the other design matrix columns.

    gram.resize(gram_size, gram_size);

    std::size_t row = 0;
    for (std::size_t i = 0; i != harmonics.size(); ++i) {
        std::size_t h_row = harmonics[i];

        std::size_t col = 0;
        for (std::size_t j = 0; j != harmonics.size(); ++j) {
            std::size_t h_col = harmonics[j];

            gram(row,col) = dot_store.cos_dot_cos(h_row, h_col);
            ++col;

            if (h_col != 0) {
                gram(row,col) = dot_store.cos_dot_sin(h_row, h_col);
                ++col;
            }
        }
        ++row;

        if (h_row != 0) {
            col = 0;
            for (std::size_t j = 0; j != harmonics.size(); ++j) {
                std::size_t h_col = harmonics[j];
                
                gram(row,col) = dot_store.sin_dot_cos(h_row, h_col);
                ++col;

                if (h_col != 0){
                    gram(row,col) = dot_store.sin_dot_sin(h_row, h_col);
                    ++col;
                }
            }
            ++row;
        }
    }
}

void generate_design_t_observations_with_one_extra_frequency_space(
    const std::vector<std::size_t>& harmonics,
    dot_product_store& dot_store,
    Eigen::VectorXd& design_t_observations
) {
    if (harmonics.size() == 0) {
        throw std::logic_error("In generate_design_t_observations harmonics.size() == 0.");
    }

    std::size_t design_t_observations_size = (harmonics[0]==0 ? 2*harmonics.size()-1 : 2*harmonics.size()) + 2;
    // See comments under gram_size variable initialisation in generate_gram above.
    // The "+ 2" is the space for a significance test subject frequency.
    // See append_new_freq_design_t_observations for how this extra
    // spaced is used.

    design_t_observations.resize(design_t_observations_size);

    std::size_t row = 0;
    for (std::size_t i = 0; i != harmonics.size(); ++i) {
        std::size_t h = harmonics[i];

        design_t_observations(row) = dot_store.observations_dot_cos(h);
        ++row;

        if (h != 0) {
            design_t_observations(row) = dot_store.observations_dot_sin(h);
            ++row;
        }
    }

}

void generate_design_t_new_freq(
    std::size_t new_harmonic,
    dot_product_store& dot_store,
    const std::vector<std::size_t>& design_harmonics,
    Eigen::Matrix<double, Eigen::Dynamic, 2>& design_t_new_freq
) {
    if (design_harmonics.size() == 0) {
        throw std::logic_error("In generate_design_t_new_freq design_harmonic.size() == 0.");
    } else if (design_harmonics[0] == 0) {  // We skip the sine component of harmonic == 0 since sin(0*w0*t) = 0 for all t.
        if (design_t_new_freq.rows() != 2*design_harmonics.size() - 1) {
            throw std::logic_error("In generate_design_t_new_freq design_t_new_freq.rows() != 2*design_harmonics.size() - 1");
        }
    } else {
        if (design_t_new_freq.rows() != 2*design_harmonics.size()) {
            throw std::logic_error("In generate_design_t_new_freq design_t_new_freq.rows() != 2*design_harmonics.size()");
        }
    }

    std::size_t row = 0;
    for (std::size_t i = 0; i != design_harmonics.size(); ++i) {
        std::size_t d_harmonic = design_harmonics[i];

        design_t_new_freq(row,0) = dot_store.cos_dot_cos(d_harmonic, new_harmonic);
        design_t_new_freq(row,1) = dot_store.cos_dot_sin(d_harmonic, new_harmonic);
        ++row;

        if (d_harmonic != 0) {  // Since sin(0*w0*t) = 0 for all t, adding this dot product would make least squares intractable.
            design_t_new_freq(row,0) = dot_store.sin_dot_cos(d_harmonic, new_harmonic);
            design_t_new_freq(row,1) = dot_store.sin_dot_sin(d_harmonic, new_harmonic);
            ++row;
        }
    }
}

void generate_new_freq_gram(
    std::size_t new_harmonic,
    dot_product_store& dot_store,
    Eigen::Matrix2d& new_freq_gram
) {
    double cross_term = dot_store.cos_dot_sin(new_harmonic);
    new_freq_gram(0,0) = dot_store.cos_dot_cos(new_harmonic);
    new_freq_gram(0,1) = cross_term;
    new_freq_gram(1,0) = cross_term;
    new_freq_gram(1,1) = dot_store.sin_dot_sin(new_harmonic);
}

void append_new_freq_design_t_observations(
    std::size_t new_harmonic,
    dot_product_store& dot_store,
    Eigen::VectorXd& design_t_observations
) {
    std::size_t size = design_t_observations.rows();
    design_t_observations(size-2) = dot_store.observations_dot_cos(new_harmonic);
    design_t_observations(size-1) = dot_store.observations_dot_sin(new_harmonic);
}

#endif

