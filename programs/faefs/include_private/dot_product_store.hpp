#ifndef FAEFS_DOT_PRODUCT_MANAGER_GUARD
#define FAEFS_DOT_PRODUCT_MANAGER_GUARD

#include <algorithm>
#include <complex>
#include <iterator>
#include <stdexcept>
#include <unordered_map>
#include <utility>
#include <vector>

#include "time_series.hpp"
#include "fast_fourier.hpp"

class dot_product_store {
    public:
        dot_product_store(
            double fundamental_angular_frequency_in,
            const time_series& data_in,
            std::size_t initial_harmonic_end = 32
        ):
            fundamental_angular_frequency(fundamental_angular_frequency_in),
            harmonic_end(initial_harmonic_end),
            data(data_in),
            adj_nfft_f0(initial_harmonic_end, data_in.t.size()),
            adj_nfft_2f0(initial_harmonic_end, data_in.t.size())
        {
            extend_if_exceeded_constructor_portion();
        }

        double observations_dot_cos(std::size_t harmonic) {
            extend_if_exceeded(harmonic);
            return observations_nfft_harmonic.at(harmonic).real();
        }

        double observations_dot_sin(std::size_t harmonic) {
            extend_if_exceeded(harmonic);
            return observations_nfft_harmonic.at(harmonic).imag();
        }

        double cos_dot_cos(std::size_t sig_harmonic_lhs, std::size_t harmonic_rhs) {
            extend_if_exceeded(harmonic_rhs);
            auto& mp = cos_significant_harmonics_nfft_harmonic.at(sig_harmonic_lhs);
            return mp.at(harmonic_rhs).real();
        }

        double cos_dot_sin(std::size_t sig_harmonic_lhs, std::size_t harmonic_rhs) {
            extend_if_exceeded(harmonic_rhs);
            return cos_significant_harmonics_nfft_harmonic.at(sig_harmonic_lhs).at(harmonic_rhs).imag();
        }

        double sin_dot_cos(std::size_t sig_harmonic_lhs, std::size_t harmonic_rhs) {
            extend_if_exceeded(harmonic_rhs);
            return sin_significant_harmonics_nfft_harmonic.at(sig_harmonic_lhs).at(harmonic_rhs).real();
        }

        double sin_dot_sin(std::size_t sig_harmonic_lhs, std::size_t harmonic_rhs) {
            extend_if_exceeded(harmonic_rhs);
            return sin_significant_harmonics_nfft_harmonic.at(sig_harmonic_lhs).at(harmonic_rhs).imag();
        }

        double cos_dot_cos(std::size_t harmonic) {
            extend_if_exceeded(harmonic);
            return 0.5*(static_cast<double>(data.t.size()) + one_nfft_twice_fundamental[harmonic].real());
        }

        double cos_dot_sin(std::size_t harmonic) {
            extend_if_exceeded(harmonic);
            return 0.5*one_nfft_twice_fundamental[harmonic].imag();
        }

        double sin_dot_cos(std::size_t harmonic) {
            extend_if_exceeded(harmonic);
            return cos_dot_sin(harmonic);
        }

        double sin_dot_sin(std::size_t harmonic) {
            extend_if_exceeded(harmonic);
            return 0.5*(static_cast<double>(data.t.size()) - one_nfft_twice_fundamental[harmonic].real());
        }

        void set_significant_harmonics(const std::vector<std::size_t>& harmonics_in) {
            // Note: this function abides by: "once significant, always significant"
            // to avoid recalculating values for harmonics in the faefs algorithm
            // which are oscilating into and out of significance.

            extend_if_exceeded(harmonics_in.back());

            std::vector<std::size_t> new_significant_harmonics;
            if (significant_harmonics.empty()) {
                new_significant_harmonics = harmonics_in;
            } else {
                std::set_difference(
                    harmonics_in.cbegin(),
                    harmonics_in.cend(),
                    significant_harmonics.cbegin(),
                    significant_harmonics.cend(),
                    std::back_inserter(new_significant_harmonics)
                );
                // std::unique(new_significant_harmonics.begin(), new_significant_harmonics.end());
                // Note: it is assumed that each harmonic is only present once in any given
                // harmonics vector. Should this fail, one would expect to see nonsense matrix
                // inverses as a result of repeated columns in the design matrix, leading to a
                // singular gram matrix.
            }

            std::vector<std::size_t> all_significant_harmonics;
            all_significant_harmonics.reserve(significant_harmonics.size() + new_significant_harmonics.size());
            all_significant_harmonics.insert(all_significant_harmonics.cend(), significant_harmonics.cbegin(), significant_harmonics.cend());
            all_significant_harmonics.insert(all_significant_harmonics.cend(), new_significant_harmonics.cbegin(), new_significant_harmonics.cend());

            auto new_design_matrix_cols = generate_significant_harmonics_design_matrix_cols(new_significant_harmonics);
            auto new_cos_significant_harmonics_nfft_harmonic = generate_new_cos_significant_harmonics_nfft_harmonic(
                new_significant_harmonics,
                new_design_matrix_cols,
                adj_nfft_f0
            );
            auto new_sin_significant_harmonics_nfft_harmonic = generate_new_sin_significant_harmonics_nfft_harmonic(
                new_significant_harmonics,
                new_design_matrix_cols,
                adj_nfft_f0
            );

            for (auto h_iter = harmonics_in.cbegin(); h_iter != harmonics_in.cend(); ++h_iter) {
                // Allocate the memory for the new vectors in the maps, to then swap in without
                // fear of exceptions invalidating the object.

                std::size_t h = *h_iter;
                significant_harmonics_design_matrix_cols[h];
                cos_significant_harmonics_nfft_harmonic[h];
                sin_significant_harmonics_nfft_harmonic[h];
            }

            std::swap(all_significant_harmonics, significant_harmonics);
            swap_in(
                new_significant_harmonics,
                new_design_matrix_cols,
                significant_harmonics_design_matrix_cols
            );
            swap_in(
                new_significant_harmonics,
                new_cos_significant_harmonics_nfft_harmonic,
                cos_significant_harmonics_nfft_harmonic
            );
            swap_in(
                new_significant_harmonics,
                new_sin_significant_harmonics_nfft_harmonic,
                sin_significant_harmonics_nfft_harmonic
            );
        }
        
    private:
        double fundamental_angular_frequency;
        std::size_t harmonic_end;
        const time_series& data;
        double sample_size;
        faefs_adjoint_nfft adj_nfft_f0;
        faefs_adjoint_nfft adj_nfft_2f0;
        std::vector<std::size_t> significant_harmonics;
        std::unordered_map<std::size_t, std::vector<std::complex<double>>> significant_harmonics_design_matrix_cols;
        std::unordered_map<std::size_t, std::vector<std::complex<double>>> cos_significant_harmonics_nfft_harmonic;
        std::unordered_map<std::size_t, std::vector<std::complex<double>>> sin_significant_harmonics_nfft_harmonic;
        std::vector<std::complex<double>> observations_nfft_harmonic;
        std::vector<std::complex<double>> one_nfft_twice_fundamental;

        bool is_significant_harmonic(std::size_t harmonic) {
            return std::find(significant_harmonics.cbegin(), significant_harmonics.cend(), harmonic) != significant_harmonics.cend();
        }

        void swap_in(
            const std::vector<std::size_t>& harmonics,
            std::vector<std::vector<std::complex<double>>>& going_in,
            std::unordered_map<std::size_t, std::vector<std::complex<double>>>& to_here
        ) {
            if (harmonics.size() != going_in.size()) {
                throw std::logic_error("dot_product_store::swap_in has harmonics.size() != going_in.size()");
            }

            for (std::size_t i = 0; i != harmonics.size(); ++i) {
                std::size_t h = harmonics[i];
                std::swap(going_in[i], to_here.at(h));
            }
        }

        void swap_in(
            const std::vector<std::size_t>& harmonics,
            std::unordered_map<std::size_t, std::vector<std::complex<double>>>& going_in,
            std::unordered_map<std::size_t, std::vector<std::complex<double>>>& to_here
        ) {
            if (harmonics.size() != going_in.size()) {
                throw std::logic_error("dot_product_store::swap_in has harmonics.size() != going_in.size()");
            }

            for (auto h_iter = harmonics.cbegin(); h_iter != harmonics.cend(); ++h_iter) {
                std::size_t h = *h_iter;
                std::swap(going_in.at(h), to_here.at(h));
            }
        }

        std::unordered_map<std::size_t, std::vector<std::complex<double>>> generate_significant_harmonics_design_matrix_cols(
            const std::vector<std::size_t>& harmonics
        ) {
            std::unordered_map<std::size_t, std::vector<std::complex<double>>> new_significant_harmonics_design_matrix_cols;
            for (auto h_iter = harmonics.cbegin(); h_iter != harmonics.cend(); ++h_iter) {
                std::size_t h = *h_iter;
                std::vector<std::complex<double>> h_design_matrix_col;
                h_design_matrix_col.reserve(data.t.size());
                for (auto t_iter = data.t.cbegin(); t_iter != data.t.cend(); ++t_iter) {
                    double t = *t_iter;
                    new_significant_harmonics_design_matrix_cols[h].push_back(std::exp(std::complex<double>(0.0, h*fundamental_angular_frequency*t)));
                }
            }
            return new_significant_harmonics_design_matrix_cols;
        }

        template<class F>
        std::vector<std::complex<double>> generate_new_cos_or_sin_harmonic_nfft_harmonic(
            std::size_t harmonic,
            const std::vector<std::complex<double>>& harmonic_design_matrix_cols,
            faefs_adjoint_nfft& adj_nfft_f0,
            const F& real_or_imag
        ) {
            return adj_nfft_f0.fft(
                [&real_or_imag, &harmonic_design_matrix_cols] (std::size_t j) {
                    return real_or_imag(harmonic_design_matrix_cols.at(j));
                }
            );
        }

        template<class F>
        std::vector<std::complex<double>> generate_new_cos_harmonic_nfft_harmonic(
            std::size_t harmonic,
            const std::vector<std::complex<double>>& harmonic_design_matrix_cols,
            faefs_adjoint_nfft& adj_nfft_f0
        ) {
            return generate_new_cos_or_sin_harmonic_nfft_harmonic(
                harmonic,
                harmonic_design_matrix_cols,
                adj_nfft_f0,
                static_cast<double (*)(const std::complex<double>&)>(&std::real<double>)
            );
        }

        template<class F>
        std::vector<std::complex<double>> generate_new_sin_harmonic_nfft_harmonic(
            std::size_t harmonic,
            const std::vector<std::complex<double>>& harmonic_design_matrix_cols,
            faefs_adjoint_nfft& adj_nfft_f0
        ) {
            return generate_new_cos_or_sin_harmonic_nfft_harmonic(
                harmonic,
                harmonic_design_matrix_cols,
                adj_nfft_f0,
                static_cast<double (*)(const std::complex<double>&)>(&std::imag<double>)
            );
        }

        template<class F>
        std::unordered_map<std::size_t, std::vector<std::complex<double>>> generate_new_cos_or_sin_significant_harmonics_nfft_harmonic(
            const std::vector<std::size_t>& significant_harmonics_in,
            const std::unordered_map<std::size_t, std::vector<std::complex<double>>>& significant_harmonics_design_matrix_cols_in,
            faefs_adjoint_nfft& adj_nfft_f0,
            const F& real_or_imag
        ) {
            std::unordered_map<std::size_t, std::vector<std::complex<double>>> new_cos_or_sin_significant_harmonics_nfft_harmonic;
            for (auto iter = significant_harmonics_in.cbegin(); iter != significant_harmonics_in.cend(); ++iter) {
                std::size_t sig_harmonic = *iter;
                new_cos_or_sin_significant_harmonics_nfft_harmonic[sig_harmonic] = generate_new_cos_or_sin_harmonic_nfft_harmonic(
                    sig_harmonic,
                    significant_harmonics_design_matrix_cols_in.at(sig_harmonic),
                    adj_nfft_f0,
                    real_or_imag
                );
            }
            return new_cos_or_sin_significant_harmonics_nfft_harmonic;
        }

        std::unordered_map<std::size_t, std::vector<std::complex<double>>> generate_new_cos_significant_harmonics_nfft_harmonic(
            const std::vector<std::size_t>& significant_harmonics_in,
            const std::unordered_map<std::size_t, std::vector<std::complex<double>>>& significant_harmonics_design_matrix_cols_in,
            faefs_adjoint_nfft& adj_nfft_f0
        ) {
            return generate_new_cos_or_sin_significant_harmonics_nfft_harmonic(
                significant_harmonics_in,
                significant_harmonics_design_matrix_cols_in,
                adj_nfft_f0,
                static_cast<double (*)(const std::complex<double>&)>(&std::real<double>)
            );
        }

        std::unordered_map<std::size_t, std::vector<std::complex<double>>> generate_new_sin_significant_harmonics_nfft_harmonic(
            const std::vector<std::size_t>& significant_harmonics_in,
            const std::unordered_map<std::size_t, std::vector<std::complex<double>>>& significant_harmonics_design_matrix_cols_in,
            faefs_adjoint_nfft& adj_nfft_f0
        ) {
            return generate_new_cos_or_sin_significant_harmonics_nfft_harmonic(
                significant_harmonics_in,
                significant_harmonics_design_matrix_cols_in,
                adj_nfft_f0,
                static_cast<double (*)(const std::complex<double>&)>(&std::imag<double>)
            );
        }

        std::vector<std::complex<double>> generate_new_observations_nfft_harmonic(
            faefs_adjoint_nfft& adj_nfft_f0
        ) {
            return adj_nfft_f0.fft(data.y);
        }

        std::vector<std::complex<double>> generate_new_one_nfft_twice_fundamental(
            faefs_adjoint_nfft& adj_nfft_2f0
        ) {
            return adj_nfft_2f0.fft([](std::size_t) { return 1.0; });
        }

        void extend_if_exceeded_constructor_portion(void) {            
            adj_nfft_f0.node_coordinates(fundamental_angular_frequency, data.t);
            adj_nfft_2f0.node_coordinates(2*fundamental_angular_frequency, data.t);
            
            // Copy (or in this case, generate)...
            auto new_cos_significant_harmonics_nfft_harmonic = generate_new_cos_significant_harmonics_nfft_harmonic(
                significant_harmonics,
                significant_harmonics_design_matrix_cols,
                adj_nfft_f0
            );
            auto new_sin_significant_harmonics_nfft_harmonic = generate_new_sin_significant_harmonics_nfft_harmonic(
                significant_harmonics,
                significant_harmonics_design_matrix_cols,
                adj_nfft_f0
            );
            auto new_observations_nfft_harmonic = generate_new_observations_nfft_harmonic(adj_nfft_f0);
            auto new_one_nfft_twice_fundamental = generate_new_one_nfft_twice_fundamental(adj_nfft_2f0);

            // and swap.
            std::swap(new_cos_significant_harmonics_nfft_harmonic, cos_significant_harmonics_nfft_harmonic);
            std::swap(new_sin_significant_harmonics_nfft_harmonic, sin_significant_harmonics_nfft_harmonic);
            std::swap(new_observations_nfft_harmonic, observations_nfft_harmonic);
            std::swap(new_one_nfft_twice_fundamental, one_nfft_twice_fundamental);
        }
        
        void extend_if_exceeded(std::size_t harmonic) {
            // Extends the range of harmonics for which dot products are
            // stored in this object. Requires the re-calculation of each
            // fast fourier/cosine transform. Uses the copy and swap idiom.

            if (harmonic >= harmonic_end) {
                std::size_t new_harmonic_end = 2*harmonic_end;
                while (harmonic >= new_harmonic_end) new_harmonic_end *= 2;
                adj_nfft_f0.reconstruct(new_harmonic_end, data.t.size());
                adj_nfft_2f0.reconstruct(new_harmonic_end, data.t.size());
                //auto new_design_matrix_cols = generate_significant_harmonics_design_matrix_cols(significant_harmonics);
                extend_if_exceeded_constructor_portion();
                //std::swap(new_design_matrix_cols, significant_harmonics_design_matrix_cols);
                harmonic_end = new_harmonic_end;
            }
        }
};

#endif

