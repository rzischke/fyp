#ifndef FAEFS_PHASE_FOLD_GUARD
#define FAEFS_PHASE_FOLD_GUARD

#include <algorithm>
#include <cstdlib>
#include <cmath>
#include <utility>

#include "time_series.hpp"

template <class F>
time_series phase_fold(const time_series& data, double period, const F& kernel_inverse_right, double boundary_density) {
    // Fold (presumably periodic) data, and repeated at other periods as
    // described in Nonparametric estimation of a periodic function
    // (Hall et. al. 2000).

    time_series phased_data;

    // Calculate the extra periods required to satisfy the boundary weight.
    double boundary_phase_left = kernel_inverse_right(boundary_density)/period;
    auto extra_periods_oneside = std::lround(std::ceil(boundary_phase_left));
    auto extra_periods = 2l*extra_periods_oneside;
    auto total_periods = extra_periods + 1;

    // Size the phased_data storage appropriately.
    auto phased_data_size_per_phase = data.y.size();
    std::size_t phased_data_size = total_periods*phased_data_size_per_phase;
    phased_data.y.reserve(phased_data_size);
    phased_data.t.reserve(phased_data_size);

    std::copy(
        data.y.cbegin(),
        data.y.cend(),
        std::back_inserter(phased_data.y)
    );

    double left_most_t_shift = -extra_periods_oneside*period;

    std::transform(
        data.t.cbegin(),
        data.t.cend(),
        std::back_inserter(phased_data.t),
        [period, left_most_t_shift] (double t) {
            return std::fmod(t, period) + left_most_t_shift;
        }
    );

    phased_data.sort();

    for (std::size_t p = 1; p <= extra_periods; ++p) {
        auto right_t_shift_from_left = p*period;
        for (std::size_t i = 0; i != phased_data_size_per_phase; ++i) {
            auto y = phased_data.y[i];
            auto t = phased_data.t[i];
            phased_data.y.push_back(y);
            phased_data.t.push_back(t + right_t_shift_from_left);
        }
    }

    return phased_data;
}

#endif
