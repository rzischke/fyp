#ifndef FAEFS_SMOOTHING_GUARD
#define FAEFS_SMOOTHING_GUARD

#include <array>
#include <algorithm>
#include <cmath>
#include <iterator>
#include <limits>
#include <stdexcept>
#include <tuple>
#include <vector>
#include <boost/math/constants/constants.hpp>
#include <Eigen/Core>
#include <Eigen/Cholesky>

#include "time_series.hpp"
#include "figtree.h"
#include "kahan_sum.hpp"
#include "phase_fold.hpp"

double optimum_local_linear_gaussian_bandwidth(
    std::size_t sample_size,
    double variance,
    double fundamental_angular_frequency,
    double scaled_magnitude2s_sum
) {
    // Calculates the optimum gaussian bandwidth for a local linear smoother of a periodic function with a particular
    // fourier series. As per: Design-adaptive Nonparametric Regression (Fan, 1992).
    // This version is provided for use both for the initial bandwidth choice and for re-smoothing in the face of
    // new modelling information. Other versions below call this one, but take different parameters.

    auto constant_factor = static_cast<double>(4*boost::math::constants::root_two<long double>()*boost::math::constants::pi<long double>());

    
    return std::pow(
        (constant_factor*variance)/(sample_size*scaled_magnitude2s_sum),
        0.2
    )/fundamental_angular_frequency;
}

template <class Iter1, class Iter2>
double optimum_local_linear_gaussian_bandwidth(
    std::size_t sample_size,
    double variance,
    double fundamental_angular_frequency,
    Iter1 harmonics_begin,
    Iter1 harmonics_end,
    Iter2 cos_sin_projections_begin,
    Iter2 cos_sin_projections_end
) {
    auto scaled_magnitude2s_summer = make_kahan_summer(0.0);
    auto h_iter = harmonics_begin;
    auto cs_iter = cos_sin_projections_begin;
    while (h_iter != harmonics_end && cs_iter != cos_sin_projections_end) {
        auto square = [](auto x) { return x*x; };
        auto quart = [&square](auto x) { return square(square(x)); };

        auto harmonic4 = quart(*h_iter);

        scaled_magnitude2s_summer.add(harmonic4*square(*cs_iter));
        ++cs_iter;
        scaled_magnitude2s_summer.add(harmonic4*square(*cs_iter));

        ++h_iter;
        ++cs_iter;
    }

    if (h_iter != harmonics_end) {
        throw std::logic_error("There were more harmonics provided to gof::optimum_local_linear_gaussian_bandwidth than there were frequency components.");
    }

    if (cs_iter != cos_sin_projections_end) {
        throw std::logic_error("There were more frequency components provided to gof::optimum_local_linear_gaussian_bandwidth than there were harmonics.");
    }

    auto scaled_magnitude2s_sum = scaled_magnitude2s_summer.get_sum();

    return optimum_local_linear_gaussian_bandwidth(
        sample_size,
        variance,
        fundamental_angular_frequency,
        scaled_magnitude2s_sum
    );
}

double initial_local_linear_gaussian_bandwidth(
    double fundamental_angular_frequency,
    std::size_t sample_size
) {
    // The initial bandwidth is the optimum bandwidth for a process
    // with a single frequency component at the fundamental frequency
    // with a magnitude equal to the standard deviation of the noise.

    return optimum_local_linear_gaussian_bandwidth(
        sample_size,
        1.0,
        fundamental_angular_frequency,
        1.0
    );
}

double rk4_error_bounded_delta_per_unit_bandwidth(long double period, long double y_abs_max, long double error_bound) {
    constexpr auto e = boost::math::constants::e<long double>();
    constexpr auto e2 = e*e;

    auto numerator = 45*e2*error_bound;
    auto denominator = 18688*period;

    return static_cast<double>(std::pow(numerator/denominator, 0.25l)/std::max(y_abs_max*y_abs_max, 1.0l));
}

double runga_kutta_4_integral(const std::vector<double>& y, double dt) {
    // This is equivalent to simpsons rule.

    if (y.size()%2 == 0) throw std::logic_error("Vector y in function gof::runga_kutta_4_integral requires odd size, but has even.");

    auto running_integral_summer = make_kahan_summer(y.front());
    for (std::size_t i = 1; i < y.size()-1; i += 2) {
        running_integral_summer.add(4*y[i]);
        running_integral_summer.add(2*y[i+1]);
    }
    running_integral_summer.add(y.back());

    return (dt/3)*running_integral_summer.get_sum();
}

double index_to_time(std::size_t i, double n, double t_min, double range) {
    return t_min + ((i*range)/(n-1));
}

std::vector<double> fast_gauss_transform(
    const std::vector<double>& y,
    const std::vector<double>& t,
    double bandwidth,
    double t_min,
    double t_max,
    std::size_t number_of_targets,
    double error_bound
) {
    // Calculates the (improved) fast gauss transform of y(t) at equidistant points between the minimum time and maximum time.

    if (y.size() != t.size()) throw std::logic_error("In fast_gauss_transform: mismatch between sizes of t and y vectors.");

    auto t_range = t_max - t_min;

    std::vector<double> target_times;
    target_times.reserve(number_of_targets);
    for (std::size_t i = 0; i != number_of_targets; ++i) {
        target_times.push_back(index_to_time(i, number_of_targets, t_min, t_range));
    }

    std::vector<double> transform(number_of_targets);
    figtree(
        1,                  // Dimensionality of transform.
        t.size(),
        number_of_targets,
        1,                  // Number of weight vectors (in this case, data.y).
        const_cast<double*>(t.data()),
        bandwidth,
        const_cast<double*>(y.data()),
        target_times.data(),
        error_bound,
        transform.data()
    );
    // See here for annotated example of using this function:
    // https://github.com/vmorariu/figtree/blob/master/samples/sample.cpp

    return transform;
}

std::vector<double> fast_gauss_transform(
    const time_series& data,
    double bandwidth,
    double t_min,
    double t_max,
    std::size_t number_of_targets,
    double error_bound
) {
    return fast_gauss_transform(data.y, data.t, bandwidth, t_min, t_max, number_of_targets, error_bound);
}

std::tuple<std::vector<double>, std::vector<double>> fast_local_linear_smoothing_gaussian_kernel(
    const time_series& data,
    double bandwidth,
    double t_min,
    double t_max,
    std::size_t number_of_targets,
    double error_bound
) {
    auto t_range = t_max - t_min;

    auto loop_pow = [](double base, unsigned exponent) {
        double base_to_exponent = 1.0;
        for (unsigned r = 0; r != exponent; ++r) {
            base_to_exponent *= base;
        }
        return base_to_exponent;
    };

    auto vec_pow = [&loop_pow](const std::vector<double>& vec, unsigned exponent) {
        std::vector<double> ret;
        ret.reserve(vec.size());
        std::transform(
            vec.cbegin(),
            vec.cend(),
            std::back_inserter(ret),
            [&loop_pow,exponent] (double x) {
                return loop_pow(x, exponent);
            }
        );
        return ret;
    };

    auto g = [&vec_pow, &data, bandwidth, number_of_targets, error_bound, t_min, t_max](unsigned exponent) {
        if (exponent == 1) {
            return fast_gauss_transform(data.t, data.t, bandwidth, t_min, t_max, number_of_targets, error_bound);
        } else {
            auto t_pow = vec_pow(data.t, exponent);
            return fast_gauss_transform(t_pow, data.t, bandwidth, t_min, t_max, number_of_targets, error_bound);
        }
    };

    auto gy = [&loop_pow, &data, bandwidth, number_of_targets, error_bound, t_min, t_max] (unsigned exponent) {
        std::vector<double> t_pow_y;
        t_pow_y.reserve(data.t.size());
        for (std::size_t i = 0; i != data.t.size(); ++i) {
            t_pow_y.push_back(data.y[i]*loop_pow(data.t[i], exponent));
        }
        return fast_gauss_transform(t_pow_y, data.t, bandwidth, t_min, t_max, number_of_targets, error_bound);
    };

    auto g0 = g(0);
    auto g1 = g(1);
    auto g2 = g(2);
    auto gy0 = gy(0);
    auto gy1 = gy(1);

    std::vector<double> local_linear_estimate;
    local_linear_estimate.reserve(number_of_targets);

    std::vector<double> sum_of_squared_weights;
    sum_of_squared_weights.reserve(number_of_targets);

    for (std::size_t i = 0; i != number_of_targets; ++i) {
        auto t = index_to_time(i, number_of_targets, t_min, t_range);

        auto g0t = g0[i];
        auto g1t = g1[i];
        auto g2t = g2[i];

        auto gy0t = gy0[i];
        auto gy1t = gy1[i];

        Eigen::Matrix2d G_or_G_Cholesky;  // Gram matrix, later it's cholesky decomposition.
        G_or_G_Cholesky << g0t, g1t,
                           g1t, g2t;

        Eigen::Vector2d g;
        g << gy0t,
             gy1t;

        // Solve normal equations G*b = g for b.
        #if EIGEN_VERSION_AT_LEAST(3,3,0)
            // In place cholesky is supported.
            Eigen::LLT<Eigen::Ref<Eigen::Matrix2d>> llt(G_or_G_Cholesky);
        #else
            // In place cholesky is not supported.
            Eigen::LLT<Eigen::Matrix2d> llt(G_or_G_Cholesky);
        #endif
        
        Eigen::Matrix<double, 1, 2> x;
        x << 1.0, t;

        Eigen::Vector2d b = llt.solve(g);
        local_linear_estimate.push_back(x*b);

        Eigen::Vector2d l = llt.solve(x.transpose());
        sum_of_squared_weights.push_back(x*l);
    }

    return std::make_tuple(std::move(local_linear_estimate),std::move(sum_of_squared_weights));
}

double get_fast_gauss_transform_error_bound(double total_error_bound, double period, double y_abs_max) {
    // There are two sources of numeric errors: the fast gauss transform
    // and the runge kutta fourth order numerical integration. This
    // function returns the FGT error required to meet the total
    // error bound requirements.

    double other_root = -sqrt(4*period*period*y_abs_max*y_abs_max + 2*period*total_error_bound)/(2*period) - y_abs_max;

    return -total_error_bound/(2*period*other_root);
}

double get_runge_kutta_error_bound(double total_error_bound) {
    // See comments for get_fast_gauss_transform_error_bound.
    // This function returns the runge kutte error required
    // to meet the total error bound requirements.

    return 0.5*total_error_bound;
}

std::tuple<double,double> energy_estimate_local_linear(
    const time_series& data,
    double y_abs_max,
    double period,
    double kernel_bandwidth,
    double error_bound
) {
    auto data_folded = phase_fold(
        data,
        period,
        [kernel_bandwidth](double weight) {
            double h = kernel_bandwidth;
            return sqrt(-h*h*log(weight));
        },
        y_abs_max*std::numeric_limits<double>::epsilon()
    );

    // Pass in y_abs_max^2 since the rk4 integration is done on the squared smoothed signal, and not the
    // raw one.
    auto delta_per_bandwidth = rk4_error_bounded_delta_per_unit_bandwidth(period, y_abs_max*y_abs_max, error_bound);
    auto dt_max = kernel_bandwidth*delta_per_bandwidth;

    auto number_of_fgt_targets = std::llround(std::ceil((data.t.back() - data.t.front())/dt_max)) + 1;
    if (number_of_fgt_targets%2 == 0) ++number_of_fgt_targets;

    auto dt = period/(number_of_fgt_targets - 1);

    auto data_folded_smoothed_squared_with_weights_norm_squared = fast_local_linear_smoothing_gaussian_kernel(
        data_folded,
        kernel_bandwidth,
        0.0,
        period,
        number_of_fgt_targets,
        get_fast_gauss_transform_error_bound(error_bound, period, y_abs_max)
    );
    auto data_folded_smoothed_squared = std::move(std::get<0>(data_folded_smoothed_squared_with_weights_norm_squared));
    auto weights_norm_squared = std::move(std::get<1>(data_folded_smoothed_squared_with_weights_norm_squared));
    std::for_each(
        data_folded_smoothed_squared.begin(),
        data_folded_smoothed_squared.end(),
        [](double& x){
            double x2 = x*x;
            x = x2;
        }
    );

    std::vector<double> t_fss;
    t_fss.reserve(data_folded_smoothed_squared.size());
    for (std::size_t i = 0; i != data_folded_smoothed_squared.size(); ++i) {
        t_fss.push_back(dt*i);
    }

    double frequency = 1/period;
    double integral = runga_kutta_4_integral(data_folded_smoothed_squared, dt);

    double energy_estimate = 2*frequency*integral;
    double squared_standard_error_per_dgp_variance = 4*frequency*frequency*runga_kutta_4_integral(weights_norm_squared, dt);
    // DGP stands for Data Generating Process: the underlying (stochastic) equation that is assumed to
    // have generated the data, in this case a fourier series plus iid gaussian white noise.
    
    auto tpl = std::make_tuple(energy_estimate, squared_standard_error_per_dgp_variance);

    return tpl;
}

std::tuple<double, double> energy_estimate_local_linear(
    const time_series& data,
    double y_sample_var,
    double y_abs_max,
    double period,
    double kernel_bandwidth,
    double standard_error_guess,
    double absolute_error,
    double relative_error
) {
    std::tuple<double,double> tpl;
    double energy_estimate_error_bound;
    do {
        energy_estimate_error_bound = 0.5*std::max(absolute_error, relative_error*standard_error_guess);
        tpl = energy_estimate_local_linear(
            data,
            y_abs_max,
            period,
            kernel_bandwidth,
            energy_estimate_error_bound
        );
        double energy_estimate = std::get<0>(tpl);
        double squared_standard_error_per_dgp_variance = std::get<1>(tpl);
        standard_error_guess = sqrt(squared_standard_error_per_dgp_variance*y_sample_var);
    } while (energy_estimate_error_bound >= absolute_error && energy_estimate_error_bound >= relative_error*standard_error_guess);
    return tpl;
}

#endif

