#ifndef FAEFS_NYQUIST_GUARD
#define FAEFS_NYQUIST_GUARD

#include <cmath>
#include <limits>
#include <stdexcept>
#include <vector>
#include <boost/math/common_factor_rt.hpp>

double get_nyquist_frequency(const double *t, size_t num_obs, double eps) {
    // See Eyer, L. and Bartholdi, P. 1999 - Variable stars: Which Nyquist frequency?

    if (num_obs < 3) {
        throw std::logic_error("The nyquist_frequency cannot be found for a sample size of less than three.");
    }

    double t0 = t[0];
    double eps_inv = 1.0/eps;
    long long int rolling_gcd = std::llround(eps_inv*(t[1] - t0));
    for (size_t i = 2; i != num_obs; ++i) {
        rolling_gcd = boost::math::gcd(rolling_gcd, std::llround(eps_inv*(t[i] - t0)));
    }

    double p = eps*rolling_gcd;

    double nyquist_frequency = 1.0/(2*p);

    return nyquist_frequency;
}

std::size_t get_nyquist_harmonic(double f0, const double *t, size_t num_obs, double eps) {
    double nyquist_harmonic_exact = get_nyquist_frequency(t, num_obs, eps)/f0;
    std::size_t nyquist_harmonic;
    if (nyquist_harmonic_exact >= std::numeric_limits<std::size_t>::max()) {
        nyquist_harmonic = std::numeric_limits<std::size_t>::max();
    } else {
        nyquist_harmonic = static_cast<std::size_t>(std::llround(std::floor(nyquist_harmonic_exact)));
    }
    return nyquist_harmonic;
}

#endif
