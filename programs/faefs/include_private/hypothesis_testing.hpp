#ifndef FAEFS_HYPOTHESIS_TESTING_GUARD
#define FAEFS_HYPOTHESIS_TESTING_GUARD

#include <cmath>
#include <stdexcept>
#include <boost/math/distributions/fisher_f.hpp>
#include <boost/math/distributions/normal.hpp>
#include <Eigen/Core>
#include <Eigen/Cholesky>

class f_crit_sig_freq_generator {
    public:
        f_crit_sig_freq_generator(
            std::size_t sample_size_in,
            std::size_t sample_size_0_in,
            double test_size_0_in
        ):
            sample_size(sample_size_in),
            fourth_root_sample_size(std::pow(static_cast<double>(sample_size),0.25)),
            sample_size_0(sample_size_0_in),
            test_size_0(test_size_0_in)
        {
            if (sample_size <= sample_size_0) {
                test_size = test_size_0;
            } else {
                constexpr double number_of_linear_restrictions = 2.0;
                // One for sin(wt)=0, and one for cos(wt)=0.

                double f_crit_per_4th_root_n = boost::math::quantile(
                    boost::math::complement(
                        boost::math::fisher_f_distribution<double>(
                            number_of_linear_restrictions,
                            static_cast<double>(sample_size_0)
                        ),
                        test_size_0
                    )
                );

                double f_crit_for_sample_size_df = f_crit_per_4th_root_n*fourth_root_sample_size;

                test_size = boost::math::cdf(
                     boost::math::complement(
                         boost::math::fisher_f_distribution<double>(
                             number_of_linear_restrictions,
                             static_cast<double>(sample_size)
                         ),
                         f_crit_for_sample_size_df
                     )
                 );
            }
        }

        double operator()(std::size_t number_of_regressors) {
            std::size_t dynp_index = number_of_regressors_to_dynp_index(number_of_regressors);

            if (dynp_index >= f_crits_dynamic_programming.size()) {
                f_crits_dynamic_programming.resize(dynp_index+1, 0.0);
                return get_f_crit_and_populate_dynp(number_of_regressors, dynp_index);
            } else if (f_crits_dynamic_programming[dynp_index] == 0.0) {
                return get_f_crit_and_populate_dynp(number_of_regressors, dynp_index);
            } else {
                return f_crits_dynamic_programming[dynp_index];
            }
        }

        double get_test_size(void) { return test_size; }

    private:
        std::size_t sample_size;
        double fourth_root_sample_size;
        double test_size;
        std::size_t sample_size_0;
        double test_size_0;
        std::vector<double> f_crits_dynamic_programming;

        std::size_t number_of_regressors_to_dynp_index(std::size_t number_of_regressors) {
            if (number_of_regressors%2 == 0 || number_of_regressors < 3) {
                throw std::logic_error("The dynamic programming vector in sig_freq_f_crit_generator only supports an odd number of regressors 3 or greater.");
            }

            return (number_of_regressors - static_cast<std::size_t>(3))/static_cast<std::size_t>(2);
        }

        double get_f_crit_and_populate_dynp(std::size_t number_of_regressors, std::size_t dynp_index) {
            if (number_of_regressors >= sample_size) {
                throw std::logic_error("In class sig_freq_f_crit_generator, the number of regressors is greater than or equal to the sample size (i.e. no degrees of freedom left).");
            }

            constexpr double number_of_linear_restrictions = 2.0;
            // One for sin(wt)=0, and one for cos(wt)=0.

            std::size_t degrees_of_freedom = sample_size - number_of_regressors;

            double f_crit = boost::math::quantile(
                boost::math::complement(
                    boost::math::fisher_f_distribution<double>(
                        number_of_linear_restrictions,
                        static_cast<double>(degrees_of_freedom)
                    ),
                    test_size
                )
            );

            f_crits_dynamic_programming.at(dynp_index) = f_crit;

            return f_crit;
        }
};

template<typename GramInverseMatrixType, typename CoefficientsMatrixType>
double f_stat_freq_sig(
    const Eigen::MatrixBase<GramInverseMatrixType>& gram_inverse,
    const Eigen::MatrixBase<CoefficientsMatrixType>& coefficient_estimates,
    std::size_t cos_index,
    std::size_t sin_index,
    double sample_variance
) {
    Eigen::Matrix<double, 2, 1> freq_projections;
    freq_projections << coefficient_estimates(cos_index),
                        coefficient_estimates(sin_index);

    Eigen::Matrix2d R_gram_inverse_Rt;
    R_gram_inverse_Rt << gram_inverse(cos_index, cos_index), gram_inverse(cos_index, sin_index),
                         gram_inverse(sin_index, cos_index), gram_inverse(sin_index, sin_index);

    #if EIGEN_VERSION_AT_LEAST(3,3,0)
        // In place cholesky is supported, store decomposition in R_gram_inverse_Rt.
        Eigen::LLT<Eigen::Ref<Eigen::Matrix2d>> cholesky_R_gram_inverse_Rt(R_gram_inverse_Rt);
    #else
        // In place cholesky is not supported, store decomposition in Eigen::LLT object.
        Eigen::LLT<Eigen::Matrix2d> cholesky_R_gram_inverse_Rt(R_gram_inverse_Rt);
    #endif

    Eigen::Matrix<double, 2, 1> f_star = cholesky_R_gram_inverse_Rt.solve(freq_projections);

    double f = static_cast<Eigen::Matrix<double,1,1>>(freq_projections.transpose()*f_star)(0,0)/sample_variance;

    return f;
}

double f_stat_p_value(double f_stat, std::size_t sample_size, std::size_t number_of_regressors) {
    constexpr double number_of_linear_restrictions = 2.0;
    // One for sin(wt)=0, and one for cos(wt)=0.

    return boost::math::cdf(
        boost::math::complement(
            boost::math::fisher_f_distribution<double>(
                number_of_linear_restrictions,
                static_cast<double>(sample_size - number_of_regressors)
            ),
            f_stat
        )
    );
}

double f_stat_neg_log10_p_value(double f_stat, std::size_t sample_size, std::size_t number_of_regressors) {
    // See https://en.wikipedia.org/wiki/F-distribution
    // and https://en.wikipedia.org/wiki/Beta_function#Incomplete_beta_function
    // Note: number of linear restrictions is 2, already
    // taken into account in algebra.

    static const double half_ln10inv = 0.5/log(10.0);
    double degrees_of_freedom = static_cast<double>(sample_size - number_of_regressors);
    return half_ln10inv*degrees_of_freedom*log1p(2.0*f_stat/degrees_of_freedom);
}

double n_stat_budget_extension(
    double estimate_energy_smoothed,
    double null_energy_smoothed,
    double standard_error
) {
    return (estimate_energy_smoothed-null_energy_smoothed)/standard_error - standard_error;
}

double n_crit_budget_extension(double test_size) {
    return boost::math::quantile(
        boost::math::complement(
            boost::math::normal_distribution<double>(0.0,1.0),
            test_size
        )
    );
}

double p_val_n_stat(double n_stat) {
    return boost::math::cdf(
        boost::math::complement(
            boost::math::normal_distribution<double>(0.0,1.0),
            n_stat
        )
    );
}

#endif

