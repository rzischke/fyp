#ifndef FAEFS_MOMENTS_GUARD
#define FAEFS_MOMENTS_GUARD

#include <vector>
#include "kahan_sum.hpp"

double get_sample_mean(const double *y, std::size_t n) {
    return kahan_sum(y, y+n, static_cast<double>(0.0))/n;
}

double get_sample_mean(const std::vector<double>& y) {
    return get_sample_mean(y.data(), y.size());
}

double get_sample_variance(const double *y, std::size_t n, double sample_mean) {
    auto summer = make_kahan_summer(static_cast<double>(0.0));
    for (const double *iter = y; iter != y+n; ++iter) {
        auto deviation = *iter - sample_mean;
        summer.add(deviation*deviation);
    }
    return summer.get_sum()/(n-1);
}

double get_sample_variance(const std::vector<double>& y, double sample_mean) {
    return get_sample_variance(y.data(), y.size(), sample_mean);
}

double get_sample_variance(const double *y, std::size_t n) {
    return get_sample_variance(y, n, get_sample_mean(y, n));
}

double get_sample_variance(const std::vector<double>& y) {
    return get_sample_variance(y.data(), y.size());
}

double get_norm_squared(const double *y, std::size_t n) {
    auto y_norm_squared_summer = make_kahan_summer(0.0);
    for (const double *iter = y; iter != y+n; ++iter) {
        double yi = *iter;
        y_norm_squared_summer.add(yi*yi);
    }
    return y_norm_squared_summer.get_sum();
}

double get_norm_squared(const std::vector<double>& y) {
    return get_norm_squared(y.data(), y.size());
}

#endif
