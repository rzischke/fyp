#ifndef FAEFS_PIPELINE_GUARD
#define FAEFS_PIPELINE_GUARD

#include <algorithm>
#include <cmath>
#include <iterator>
#include <limits>
#include <set>
#include <utility>
#include <vector>
#include <boost/functional/hash.hpp>
#include <boost/math/constants/constants.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/member.hpp>
#include <Eigen/Core>
#include <Eigen/QR>
#include <Eigen/SVD>

#include "dot_product_store.hpp"
#include "hypothesis_testing.hpp"
#include "least_squares.hpp"
#include "moments.hpp"
#include "smoothing.hpp"

template<class T>
std::size_t hash_vector(const std::vector<T>& vec) {
    std::size_t hash = 0;
    for (auto iter = vec.cbegin(); iter != vec.cend(); ++iter) {
        boost::hash_combine(hash,*iter);
    }
    return hash;
}

struct candidate_model {
    candidate_model(
        std::vector<std::size_t> significant_harmonics_in,
        std::size_t iteration_in
    ):
        significant_harmonics(std::move(significant_harmonics_in)),
        iteration(iteration_in),
        hash(hash_vector(significant_harmonics))
    { }

    std::vector<std::size_t> significant_harmonics;
    std::size_t iteration;
    std::size_t hash;
};

using candidate_model_set = boost::multi_index_container<
    candidate_model,
    boost::multi_index::indexed_by<
        // Sort by iteration...
        boost::multi_index::ordered_unique<boost::multi_index::member<candidate_model, std::size_t, &candidate_model::iteration>>,
        // and by hash!
        boost::multi_index::ordered_unique<boost::multi_index::member<candidate_model, std::size_t, &candidate_model::hash>>
    >
>;

struct faefs_state {
    faefs_state(
        double f0,
        time_series data_in,
        double y_sample_mean_in,
        double y_sample_var_in,
        double y_sample_stddev_in,
        double en_eps_abs_in,
        double en_eps_rel_in
    ):
        sample_size(data_in.y.size()),
        fundamental_frequency(f0),
        fundamental_angular_frequency(2*boost::math::constants::pi<double>()*fundamental_frequency),
        period(1.0/fundamental_frequency),
        data(std::move(data_in)),
        current_significant_harmonics({0}),
        next_significant_harmonics({0}),
        models_considered(),
        dot_store(fundamental_angular_frequency, data),
        y_sample_mean(y_sample_mean_in),
        y_sample_var(y_sample_var_in),
        y_sample_stddev(y_sample_stddev_in),
        y_norm_squared(get_norm_squared(data.y)),
        y_abs_max(std::abs(*std::max_element(data.y.cbegin(), data.y.cend(), [](double l, double r) { return std::abs(l) < std::abs(r); }))),
        smoothing_kernel_bandwidth(initial_local_linear_gaussian_bandwidth(fundamental_angular_frequency, sample_size)),
        number_of_regressors(1),    // Intercept only at start.
        b_sig(number_of_regressors),
        b_sig_test(number_of_regressors+2),
        gram_sig(number_of_regressors,number_of_regressors),
        gram_sig_inv(number_of_regressors,number_of_regressors),
        gram_sig_test(number_of_regressors+2, number_of_regressors+2),
        gram_sig_test_inv(number_of_regressors+2,number_of_regressors+2),
        design_t_test(),
        design_t_obs(),
        rginvmem(number_of_regressors),
        sum_of_squared_errors(),
        noise_variance_estimate(y_sample_var),
        //noise_variance_estimate(0.0),
        f_crit_generator(data.y.size(), 100, 0.05),
        energy_estimate(0.0),
        energy_estimate_standard_error(y_sample_stddev),
        energy_estimate_extension_n_crit(n_crit_budget_extension(0.05)),
        energy_summer(0.0),
        periodogram(),
        en_eps_abs(en_eps_abs_in),
        en_eps_rel(en_eps_rel_in)
    {
        calculate_energy_estimate();

        b_sig(0) = y_sample_mean;               // First model is y^hat = y_mean.
        gram_sig(0,0) = sample_size;
        gram_sig_inv(0,0) = 1.0/sample_size;
        generate_design_t_observations_with_one_extra_frequency_space(
            current_significant_harmonics,
            dot_store,
            design_t_obs
        );

        models_considered.emplace(current_significant_harmonics, 0);

        dot_store.set_significant_harmonics(current_significant_harmonics);
    }

    void calculate_energy_estimate(void) {
        // Calculate the energy estimate of the process without updating the
        // kernel bandwidth.

        auto tpl = energy_estimate_local_linear(
            data,
            noise_variance_estimate,
            y_abs_max,
            period,
            smoothing_kernel_bandwidth,
            energy_estimate_standard_error,
            en_eps_abs,
            en_eps_rel
        );
        energy_estimate = std::get<0>(tpl);

        energy_estimate_standard_error = std::get<1>(tpl);
    }

    bool update_smoothed_energy_estimate_for_current_candidate_model(void) {
        // Updates the smoothed energy estimate using an optimum bandwidth
        // for a data generating process equal to the current candidate
        // model. Returns true if the update was successful, and false
        // if the updated energy estimate is not significantly
        // different from the previous one. If false is returned, the
        // variables energy_estimate and energy_estimate_standard_error
        // remain unchanged.

        if (current_significant_harmonics.size() == 1) {
            return false;
        }

        auto tuple = energy_estimate_local_linear(
            data,
            noise_variance_estimate,
            y_abs_max,
            period,
            smoothing_kernel_bandwidth,
            energy_estimate_standard_error,
            en_eps_abs,
            en_eps_rel
        );
        double energy_estimate_candidate = std::get<0>(tuple);
        double energy_estimate_standard_error_candidate = std::get<1>(tuple);

        double budget_extension_statistic = n_stat_budget_extension(
            energy_estimate_candidate,
            energy_estimate,
            energy_estimate_standard_error_candidate
        );

        if (budget_extension_statistic > energy_estimate_extension_n_crit) {
            energy_estimate = energy_estimate_candidate;
            energy_estimate_standard_error = energy_estimate_standard_error_candidate;
            return true;
        } else {
            return false;
        }
    }

    void allocate_wald_test_least_squares_memory(void) {
        // Preallocates the memory for performing least squares
        // to wald test each harmonic based on the current
        // significant harmonics.

        size_t test_number_of_regressors = number_of_regressors+2;
        b_sig_test.resize(test_number_of_regressors);
        gram_sig_test.resize(test_number_of_regressors, test_number_of_regressors);
        gram_sig_test_inv.resize(test_number_of_regressors, test_number_of_regressors);
        design_t_test.resize(number_of_regressors, Eigen::NoChange);
        rginvmem.resize(number_of_regressors);
    }

    void reset_energy_sum(void) {
        // Resets the energy counter to the energy in the
        // intercept estimate of the current model in
        // b_sig.

        double intercept_estimate = b_sig(0);
        energy_summer.reset();
        energy_summer.add(2*intercept_estimate*intercept_estimate);
    }

    void add_harmonic_to_energy_sum(std::vector<std::size_t>::const_iterator h_sig_iter) {
        double cos_proj;
        double sin_proj;
        if (h_sig_iter == current_significant_harmonics.cend()) {
            cos_proj = b_sig_test(number_of_regressors);
            sin_proj = b_sig_test(number_of_regressors+1);
        } else {
            std::size_t cos_proj_location = 2*(h_sig_iter - current_significant_harmonics.cbegin()) - 1;
            cos_proj = b_sig(cos_proj_location);
            sin_proj = b_sig(cos_proj_location+1);
        }

        energy_summer.add(cos_proj*cos_proj);
        energy_summer.add(sin_proj*sin_proj);
    }

    bool energy_budget_exceeded(void) {
        return energy_summer.get_sum() >= energy_estimate;
    }

    auto find_significant_harmonic(std::size_t harmonic) {
        return std::find(
            current_significant_harmonics.cbegin(),
            current_significant_harmonics.cend(),
            harmonic
        );
    }

    void fold_harmonic_into_current_model(std::size_t harmonic, std::vector<std::size_t>::const_iterator h_sig_iter) {
        if (h_sig_iter == current_significant_harmonics.cend()) {
            generate_design_t_new_freq(
                harmonic,
                dot_store,
                current_significant_harmonics,
                design_t_test
            );

            generate_new_freq_gram(
                harmonic,
                dot_store,
                gram_test
            );

            rolling_gram(
                gram_sig,
                design_t_test,
                gram_test,
                gram_sig_test
            );

            rolling_gram_inverse(
                gram_sig_inv,
                design_t_test,
                gram_test,
                rginvmem,
                gram_sig_test_inv
            );

            append_new_freq_design_t_observations(
                harmonic,
                dot_store,
                design_t_obs
            );

            b_sig_test = gram_sig_test_inv*design_t_obs;
        }
    }

    bool harmonic_has_significantly_nonzero_magnitude(
        std::size_t harmonic,
        std::vector<std::size_t>::const_iterator h_sig_iter,
        bool fill_periodogram
    ) {
        double f_stat;
        double f_crit;

        if (h_sig_iter == current_significant_harmonics.cend()) {
            // Harmonic being tested is not in the current model.

            double cross_term = -2*static_cast<Eigen::Matrix<double, 1, 1>>(b_sig_test.transpose()*design_t_obs)(0,0);
            double bt_gram_b = static_cast<double>(b_sig_test.transpose()*gram_sig_test*b_sig_test);
            double test_sum_of_squared_errors = y_norm_squared + cross_term + bt_gram_b;
            double test_noise_variance_estimate = test_sum_of_squared_errors/(sample_size - number_of_regressors - 2);

            f_stat = f_stat_freq_sig(
                gram_sig_test_inv,
                b_sig_test,
                number_of_regressors,
                number_of_regressors+1,
                test_noise_variance_estimate
            );

            std::size_t test_number_of_regressors = number_of_regressors+2;

            f_crit = f_crit_generator(test_number_of_regressors);

            if (fill_periodogram) {
                if (harmonic >= periodogram.size()) periodogram.resize(harmonic+1);
                    periodogram[harmonic] = f_stat_neg_log10_p_value(f_stat, sample_size, test_number_of_regressors);
                    //periodogram[harmonic] = -log10(f_stat_p_value(f_stat, sample_size, test_number_of_regressors));
            }

            return f_stat > f_crit;

        } else {
            // Harmonic being tested is in the current model.

            std::size_t cos_proj_location = 2*(h_sig_iter - current_significant_harmonics.cbegin()) - 1;

            f_stat = f_stat_freq_sig(
                gram_sig_inv,
                b_sig,
                cos_proj_location,
                cos_proj_location+1,
                noise_variance_estimate
            );

            f_crit = f_crit_generator(number_of_regressors);

            if (fill_periodogram) {
                if (harmonic >= periodogram.size()) periodogram.resize(harmonic+1);
                periodogram[harmonic] = f_stat_neg_log10_p_value(f_stat, sample_size, number_of_regressors);
            }

            return f_stat > f_crit;
        }
    }

    void add_harmonic_to_next_model(std::size_t harmonic) {
        next_significant_harmonics.push_back(harmonic);
    }

    bool consolidate_if_repeated_model() {
        // Checks whether the candidate model has been seen before. If it has,
        // then conduct least squares including all harmonics which have
        // featured in candidate models in the cycle, setting this to the
        // current significant harmonics, and return true*. Otherwise
        // return false.

        auto candidate_model_hash = hash_vector(next_significant_harmonics);
        const auto& hash_index = models_considered.get<1>();
        auto hash_iter = hash_index.find(candidate_model_hash);

        if (hash_iter == hash_index.end()) {
            return false;   // No repeated model, no consolidation.
        } else {
            const auto& iter_index = models_considered.get<0>();
            auto iter_iter = iter_index.find(hash_iter->iteration);
            ++iter_iter;    // next_significant_harmonics already contains the harmonics in the first *iter_iter.
            if (iter_iter == iter_index.end()) {
                // Repition was in most recent two iterations,
                // least squares already in b_sig etc. Reset
                // next_significant_harmonics and return true.
                next_significant_harmonics.resize(1);
                next_significant_harmonics[0] = 0;
                return true;
            } else {
                // Consolidation needed, iterate through candidate models in
                // cycle, creating a new model with the combined frequencies.

                // After this, next_significant_harmonics will contain all of the frequencies in the cycle.
                for (; iter_iter != iter_index.end(); ++iter_iter) {
                    next_significant_harmonics.insert(next_significant_harmonics.end(), iter_iter->significant_harmonics.cbegin(), iter_iter->significant_harmonics.cend());
                }
                std::sort(next_significant_harmonics.begin(), next_significant_harmonics.end());
                auto new_end = std::unique(next_significant_harmonics.begin(), next_significant_harmonics.end());
                next_significant_harmonics.erase(new_end, next_significant_harmonics.end());

                // This new repetition might form a cycle of its own...
                consolidate_if_repeated_model();

                // Fill out least squares parameters.
                update_candidate_model_for_next_iteration();

                // Update the periodogram if we've been using it.
                if (!periodogram.empty()) {
                    allocate_wald_test_least_squares_memory();
                    for (std::size_t h = 1; h <= current_significant_harmonics.back(); ++h) {
                        constexpr bool fill_periodogram = true;
                        auto loc = find_significant_harmonic(h);
                        fold_harmonic_into_current_model(h, loc);
                        harmonic_has_significantly_nonzero_magnitude(h, loc, fill_periodogram);
                    }
                }

                return true;
            }
        }
    }

    bool update_candidate_model_for_next_iteration(void) {
        // Updates the candidate model to the harmonics listed in next_significant_harmonics.

        // Update list of significant harmonics.
        current_significant_harmonics = std::move(next_significant_harmonics);
        next_significant_harmonics.resize(1);
        next_significant_harmonics[0] = 0;

        // Update store of dot products for these new harmonics.
        dot_store.set_significant_harmonics(current_significant_harmonics);
        
        number_of_regressors = 2*current_significant_harmonics.size() - 1;

        // Update set of previously considered models.
        // By boost documentation:
        // https://www.boost.org/doc/libs/1_58_0/libs/multi_index/doc/reference/ord_indices.html
        // emplacement only happens if an object with a similar key doesn't already exist.
        // consolidate_if_repeated_model relies on this behaviour to not re-consider union models
        // (in the case of a iteration loop) as part of two separate iterations if the union model
        // has also appeared before.
        std::size_t iteration = models_considered.size();

        models_considered.emplace(current_significant_harmonics, iteration);

        // Generate normal equation components for new candidate model.
        // These will be used to fold harmonics in for testing for
        // significantly non-zero magnitude in the fourier series
        // of the underlying process.

        generate_gram(
            current_significant_harmonics,
            dot_store,
            gram_sig
        );

        Eigen::ColPivHouseholderQR<decltype(gram_sig)> qr(gram_sig);

        gram_sig_inv = qr.inverse();
        auto gram_sig_inv_rank = qr.rank();
        if (gram_sig_inv_rank != gram_sig.rows() || gram_sig_inv_rank != gram_sig.cols()) {
            throw std::logic_error("Gram matrix singular.");
        }

        generate_design_t_observations_with_one_extra_frequency_space(
            current_significant_harmonics,
            dot_store,
            design_t_obs
        );

        b_sig.noalias() = gram_sig_inv*design_t_obs.block(0,0,number_of_regressors,1);

        // Update estimates on noise.
        double cross_term = -2*static_cast<Eigen::Matrix<double, 1, 1>>(b_sig.transpose()*design_t_obs.block(0,0,number_of_regressors,1))(0,0);
        double bt_gram_b = static_cast<double>(b_sig.transpose()*gram_sig*b_sig);
        sum_of_squared_errors = y_norm_squared + cross_term + bt_gram_b;
        noise_variance_estimate = sum_of_squared_errors/(sample_size - number_of_regressors);

        // Update optimum kernel bandwidth.
        smoothing_kernel_bandwidth = optimum_local_linear_gaussian_bandwidth(
            sample_size,
            noise_variance_estimate,
            fundamental_angular_frequency,
            current_significant_harmonics.cbegin()+1,   // avoid intercept, which has no bearing on optimum bandwidth.
            current_significant_harmonics.cend(),
            b_sig.data()+1,             // Using raw pointers to Eigen
            b_sig.data()+b_sig.rows()   // matrix buffers as iterators.
        );

        // Resize the periodogram to discard invalid values in the
        // case that the last candidate model did not get as far
        // in the frequency space.
        if (periodogram.size() > current_significant_harmonics.size()) {
            periodogram.resize(current_significant_harmonics.size());
        }
    }

    std::size_t sample_size;
    double fundamental_frequency;
    double fundamental_angular_frequency;
    double period;
    time_series data;
    std::vector<std::size_t> current_significant_harmonics;
    std::vector<std::size_t> next_significant_harmonics;
    candidate_model_set models_considered;
    dot_product_store dot_store;
    double y_sample_mean;
    double y_sample_var;
    double y_sample_stddev;
    double y_norm_squared;
    double y_abs_max;
    double smoothing_kernel_bandwidth;
    std::size_t number_of_regressors;
    Eigen::VectorXd b_sig;
    Eigen::VectorXd b_sig_test;
    Eigen::MatrixXd gram_sig;
    Eigen::MatrixXd gram_sig_inv;
    Eigen::MatrixXd gram_sig_test;
    Eigen::MatrixXd gram_sig_test_inv;
    Eigen::Matrix2d gram_test;
    Eigen::Matrix<double, Eigen::Dynamic, 2> design_t_test;
    Eigen::Matrix<double, Eigen::Dynamic, 1> design_t_obs;
    rolling_gram_inverse_intermediate_memory rginvmem;
    double sum_of_squared_errors;
    double noise_variance_estimate;
    f_crit_sig_freq_generator f_crit_generator;
    double energy_estimate;
    double energy_estimate_standard_error;
    double energy_estimate_extension_n_crit;
    kahan_summer<double> energy_summer;
    std::vector<double> periodogram;
    double en_eps_abs;
    double en_eps_rel;

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

#endif

