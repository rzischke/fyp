#ifndef KAHANSUM_GUARD
#define KAHANSUM_GUARD

#include <cmath>
#include <numeric>
#include <utility>

// Uses the kahan sum, with improvements by Neumaier.
// https://en.wikipedia.org/wiki/Kahan_summation_algorithm#Further_enhancements

template <class T>
class kahan_summer {
    public:
        kahan_summer() = default;
        explicit kahan_summer(T init): sum(std::move(init)) { }

        void add(const T& increment) {
            auto new_sum = sum + increment;
            if (std::abs(sum) >= std::abs(increment)) {
                compensation += (sum - new_sum) + increment;
            } else {
                compensation += (increment - new_sum) + sum;
            }
            sum = new_sum;
        }

        T get_sum(void) { return sum + compensation; }

        void reset(void) {
            sum = static_cast<T>(0.0l);
            compensation = static_cast<T>(0.0l);
        }

    private:
        T sum {};
        T compensation {};
};

/*
template <class T>
class kahan_sum_incrementer {
    public:
        T operator()(const T& sum, const T& increment) {
            auto new_sum = sum + increment;
            if (std::abs(sum) >= std::abs(increment)) {
                compensation += (sum - new_sum) + increment;
            } else {
                compensation += (increment - new_sum) + sum;
            }
            return new_sum;
        }

        T add_compensation(const T& sum) { return sum + compensation; }

    private:
        T compensation {};
};

template <class T>
class kahan_summer {
    public:
        kahan_summer() = default;
        explicit kahan_summer(T init): sum(std::move(init)) { }

        void add(const T& increment) {
            auto temp = sum;
            sum = incrementer(temp, increment);
        }

        T get_sum(void) { return incrementer.add_compensation(sum); }
    private:
        T sum {};
        kahan_sum_incrementer<T> incrementer;
};
*/

template <class T>
kahan_summer<T> make_kahan_summer(T init) { return kahan_summer<T>(std::move(init)); }

template <class InputIterator, class T>
T kahan_sum(InputIterator first, InputIterator last, T init) {
    auto summer = make_kahan_summer(init);
    while (++first != last) summer.add(*first);
    return summer.get_sum();
}

#endif
