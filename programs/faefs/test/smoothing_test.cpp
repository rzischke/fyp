#define EIGEN_RUNTIME_NO_MALLOC

#include "chartpp/chart_xy.hpp"
#include "sine_wave_polar_cos.hpp"
#include "smoothing.hpp"
#include "pcg_random.hpp"
#include "time_series.hpp"
#include <functional>
#include <iostream>
#include <random>
#include <utility>
#include <vector>
#include <boost/math/constants/constants.hpp>


int main(int argc, char **argv) {
    Eigen::internal::set_is_malloc_allowed(true);

    double T = 2.3;
    double f = 1/T;
    double w = 2*boost::math::constants::pi<double>()*f;
    double A = 1.0;
    sine_wave_polar_cos<double> wave(w,A,0.0);

    double standard_deviation = 0.1;
    double variance = standard_deviation*standard_deviation;
    pcg_extras::seed_seq_from<std::random_device> seed_source;
    pcg32 rng(seed_source);
    std::normal_distribution<double> norm_dist(0.0, 0.1);

    std::size_t sample_size = 1000;
    double t_max = 7.2;
    std::vector<double> y_true;
    y_true.reserve(sample_size);
    std::vector<double> y;
    y.reserve(sample_size);
    std::vector<double> t;
    t.reserve(sample_size);
    for (std::size_t i = 0; i != sample_size; ++i) {
        double t_i = index_to_time(i,sample_size,0.0,t_max);
        double y_i_true = wave(t_i);
        double y_i = y_i_true + norm_dist(rng);
        t.push_back(t_i);
        y.push_back(y_i);
        y_true.push_back(y_i_true);
    }
    time_series data(std::move(y), t);
    time_series truth(std::move(y_true), std::move(t));

    double kernel_bandwidth = optimum_local_linear_gaussian_bandwidth(
        sample_size,
        variance,
        w,
        A*A
    );

    std::cout << "optimum_bandwidth: " << kernel_bandwidth << '\n';

    double y_abs_max = std::abs(*std::max_element(
            data.y.cbegin(),
            data.y.cend(),
            [](double l, double r) { return std::abs(l) < std::abs(r); } ));
    
    auto y_smoothed_with_weights_squared = fast_local_linear_smoothing_gaussian_kernel(
        data,
        kernel_bandwidth,
        0.0,
        t_max,
        sample_size,
        0.001
    );
    auto y_smoothed = std::move(std::get<0>(y_smoothed_with_weights_squared));
    time_series data_smoothed(std::move(y_smoothed), data.t);

    std::vector<std::reference_wrapper<std::vector<double>>> Y_plot_handle;
    std::vector<std::reference_wrapper<std::vector<double>>> T_plot_handle;
    Y_plot_handle.emplace_back(std::ref(data.y));
    T_plot_handle.emplace_back(std::ref(data.t));
    Y_plot_handle.emplace_back(std::ref(data_smoothed.y));
    T_plot_handle.emplace_back(std::ref(data_smoothed.t));
    Y_plot_handle.emplace_back(std::ref(truth.y));
    T_plot_handle.emplace_back(std::ref(truth.t));

    chartpp::chart_xy plot;
    plot.title("Smoothed Sine Wave");
    plot.xlabel("t");
    plot.ylabel("y");
    plot.plot("smoothed_sine_wave", T_plot_handle, Y_plot_handle);

    Eigen::internal::set_is_malloc_allowed(false);
    auto energy_estimate_with_squared_standard_error_per_dgp_variance = energy_estimate_local_linear(data, y_abs_max, T, kernel_bandwidth, 0.001);
    Eigen::internal::set_is_malloc_allowed(true);


    auto energy_estimate = std::move(std::get<0>(energy_estimate_with_squared_standard_error_per_dgp_variance));
    auto standard_error_per_dgp_stddev = std::move(std::get<1>(energy_estimate_with_squared_standard_error_per_dgp_variance));
    std::cout << "energy_estimate: " << energy_estimate << '\n';
    std::cout << "standard_error_per_stddev: " << standard_error_per_dgp_stddev << '\n';

    return 0;
}

