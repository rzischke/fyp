#include <cstdlib>
#include <ctime>
#include <iostream>
#include <vector>

#include "pcg_random.hpp"
#include "dot_product_store.hpp"
#include "time_series.hpp"

#define EIGEN_RUNTIME_NO_MALLOC

#include "least_squares.hpp"

int main(int argc, char **argv) {
    Eigen::internal::set_is_malloc_allowed(true);

    auto print_vec = [](const std::string& name, const auto& vec) {
        std::cout << name << " =\n";
        std::for_each(
            vec.cbegin(),
            vec.cend(),
            [](const auto& elm) {
                std::cout << '\t' << elm << '\n';
            }
        );
        std::cout << '\n';
    };

    constexpr std::size_t n = 50;
    constexpr std::size_t num_freq = 8;
    constexpr std::size_t k = 2*num_freq + 1;
    constexpr double fundamental_angular_frequency = 0.9;

    pcg_extras::seed_seq_from<std::random_device> seed_source;
    pcg32 rng(seed_source);
    std::normal_distribution<double> norm_dist(0.0,0.1);
    std::exponential_distribution<double> exp_dist(6.0/fundamental_angular_frequency);
    std::poisson_distribution<int> pois_dist(3);
    srand((unsigned int)time(0));

    std::vector<double> t;
    double t_prev = 0.0;
    for (std::size_t i = 0; i != n; ++i) {
        double dt = exp_dist(rng);
        double t_i = t_prev + dt;
        t.push_back(t_i);
        t_prev = t_i;
    }

    std::vector<std::size_t> significant_harmonics;
    significant_harmonics.push_back(0);
    std::size_t i = 1;
    while (i <= num_freq) {
        int df = pois_dist(rng);
        if (df != 0) {
            significant_harmonics.push_back(significant_harmonics[i-1] + df);
            ++i;
        }
    }

    using BType = Eigen::Matrix<double,k,1>;
    BType B = BType::Random();

    using XType = Eigen::Matrix<double,Eigen::Dynamic,Eigen::Dynamic>; // Dynamic needed for svd.
    XType X(n,k);
    for (std::size_t i = 0; i != n; ++i) {
        double t_i = t[i];
        X(i,0) = 1.0;
        for (std::size_t f = 1; f <= num_freq; ++f) {
            double angular_frequency = significant_harmonics[f]*fundamental_angular_frequency;
            X(i,2*f-1) = cos(angular_frequency*t_i);
            X(i,2*f) = sin(angular_frequency*t_i);
        }
    }

    using YType = Eigen::Matrix<double,n,1>;
    YType y = X*B;
    for (std::size_t i = 0; i != n; ++i) {
        y(i) += norm_dist(rng);
    }
    
    std::vector<double> y_std_vec;
    for (std::size_t i = 0; i != n; ++i) {
        y_std_vec.push_back(y(i));
    }

    Eigen::VectorXd b_svd = ordinary_least_squares_estimate_by_svd(y,X);
    
    Eigen::MatrixXd Gram = X.transpose()*X;
    Eigen::VectorXd XtY = X.transpose()*y;
    Eigen::VectorXd b_normal = ordinary_least_squares_estimate_by_normal_equations(Gram,XtY);

    Eigen::MatrixXd y_hat = X*b_svd;

    std::cout << "B = \n" << B << "\n\n";
    std::cout << "y = \n" << y << "\n\n";
    std::cout << "X = \n" << X << "\n\n";
    std::cout << "b = \n" << b_svd << "\n\n";
    std::cout << "b_normal = \n" << b_normal << "\n\n";
    std::cout << "y_hat = \n" << y_hat << "\n\n";

    Eigen::MatrixXd Xm2(X.rows(), X.cols()-2);
    Xm2 << X.block(0, 0, X.rows(), X.cols()-2);
    Eigen::MatrixXd GramXm2 = Xm2.transpose()*Xm2;
    Eigen::MatrixXd GramXm2Inv = GramXm2.fullPivHouseholderQr().inverse();
    Eigen::MatrixXd x2(X.rows(), 2);
    x2 << X.block(0, X.cols()-2, X.rows(), 2);
    Eigen::MatrixXd Xm2tx2 = Xm2.transpose()*x2;
    Eigen::MatrixXd Gramx2 = x2.transpose()*x2;
    Eigen::MatrixXd GramInv(X.cols(), X.cols());
    rolling_gram_inverse_intermediate_memory rginvmem(Xm2.cols());

    Eigen::internal::set_is_malloc_allowed(false);
    rolling_gram_inverse(
        GramXm2Inv,
        Xm2tx2,
        Gramx2,
        rginvmem,
        GramInv
    );
    Eigen::internal::set_is_malloc_allowed(true);

    Eigen::MatrixXd b_rolling_gram = GramInv*X.transpose()*y;

    std::cout << "Xm2 = \n" << Xm2 << "\n\n";
    std::cout << "GramXm2 = \n" << GramXm2 << "\n\n";
    std::cout << "GramXm2Inv = \n" << GramXm2Inv << "\n\n";
    std::cout << "x2 = \n" << x2 << "\n\n";
    std::cout << "Xm2tx2 = \n" << Xm2tx2 << "\n\n";
    std::cout << "Gramx2 = \n" << Gramx2 << "\n\n";
    std::cout << "b_rolling_gram = \n" << b_rolling_gram << "\n\n";

    time_series data(y_std_vec, t);
    dot_product_store dot_store(fundamental_angular_frequency, data, 5);
    dot_store.set_significant_harmonics(significant_harmonics);

    std::vector<std::size_t> significant_harmonics_less_one(significant_harmonics.cbegin(), significant_harmonics.cend() - 1);

    Eigen::MatrixXd fft_GramXm2;
    generate_gram(
        significant_harmonics_less_one,
        dot_store,
        fft_GramXm2
    );
    Eigen::MatrixXd fft_GramXm2Inv = fft_GramXm2.fullPivHouseholderQr().inverse();

    Eigen::VectorXd fft_Xm2ty;
    generate_design_t_observations_with_one_extra_frequency_space(
        significant_harmonics_less_one,
        dot_store,
        fft_Xm2ty
    );

    Eigen::Matrix2d fft_Gramx2;
    generate_new_freq_gram(
        significant_harmonics.back(),
        dot_store,
        fft_Gramx2
    );

    Eigen::Matrix<double, Eigen::Dynamic, 2> fft_Xm2tx2(fft_GramXm2.rows(),2);
    generate_design_t_new_freq(
        significant_harmonics.back(),
        dot_store,
        significant_harmonics_less_one,
        fft_Xm2tx2
    );

    Eigen::MatrixXd fft_GramInv(X.cols(), X.cols());
    rolling_gram_inverse_intermediate_memory fft_rginvmem(X.cols() - 2);

    Eigen::internal::set_is_malloc_allowed(false);
    rolling_gram_inverse(
        fft_GramXm2Inv,
        fft_Xm2tx2,
        fft_Gramx2,
        fft_rginvmem,
        fft_GramInv
    );
    Eigen::internal::set_is_malloc_allowed(true);

    append_new_freq_design_t_observations(
        significant_harmonics.back(),
        dot_store,
        fft_Xm2ty
    );

    Eigen::MatrixXd fft_b_rolling_gram = fft_GramInv*fft_Xm2ty; // fft_Xm2ty should now be fft_Xty because off above append.

    std::cout << "fft_GramXm2 = \n" << fft_GramXm2 << "\n\n";
    std::cout << "fft_GramXm2Inv = \n" << fft_GramXm2Inv << "\n\n";
    std::cout << "fft_Xm2tx2 = \n" << fft_Xm2tx2 << "\n\n";
    std::cout << "fft_Gramx2 = \n" << fft_Gramx2 << "\n\n";
    std::cout << "fft_b_rolling_gram = \n" << fft_b_rolling_gram << "\n\n";

    Eigen::MatrixXd err = fft_b_rolling_gram - b_svd;
    Eigen::MatrixXd abs_err = err.array().abs().matrix();
    std::cout << "|fft_b_rolling_gram - b| = \n" << abs_err << "\n\n";

    return 0;
}

