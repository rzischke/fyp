#include <algorithm>
#include <complex>
#include <cmath>
#include <iostream>
#include <random>
#include <vector>
#include "pcg_random.hpp"
#include "fast_fourier.hpp"

int main(int argc, char **argv) {
    //pcg_extras::seed_seq_from<std::random_device> seed_source;
    pcg32 rng(12563789);
    std::normal_distribution<double> norm_dist(0.0,1.0);
    std::exponential_distribution<double> exp_dist(0.5);

    auto print_vec = [](const std::string& name, const auto& vec) {
        std::cout << name << " =\n";
        std::for_each(
            vec.cbegin(),
            vec.cend(),
            [](const auto& elm) {
                std::cout << '\t' << elm << '\n';
            }
        );
        std::cout << '\n';
    };

    std::vector<double> y;
    std::vector<double> t;
    double t_prev = 0.0;
    for (std::size_t i = 0; i != 25; ++i) {
        y.push_back(norm_dist(rng));
        double dt = exp_dist(rng);
        double t_i = t_prev + dt;
        t.push_back(t_i);
        t_prev = t_i;
    }

    double fundamental_angular_frequency = 1.0;
    std::size_t harmonic_end = 5;

    std::vector<double> yt_transform_naive_cos;
    std::vector<double> yt_transform_naive_sin;
    for (std::size_t h = 0; h != harmonic_end; ++h) {
        double sum_cos = 0.0;
        double sum_sin = 0.0;
        for (std::size_t i = 0; i != y.size(); ++i) {
            sum_cos += y[i]*cos(h*fundamental_angular_frequency*t[i]);
            sum_sin += y[i]*sin(h*fundamental_angular_frequency*t[i]);
        }
        yt_transform_naive_cos.push_back(sum_cos);
        yt_transform_naive_sin.push_back(sum_sin);
    }

    faefs_adjoint_nfft adj_nfft(harmonic_end, y.size());
    auto fourier_transform = adj_nfft.fft(
        fundamental_angular_frequency,
        y,
        t
    );

    print_vec("y", y);
    print_vec("t", t);
    print_vec("naive_cos_transform", yt_transform_naive_cos);
    print_vec("naive_sin_transform", yt_transform_naive_sin);
    print_vec("adjoint_nfft", fourier_transform);

    return 0;
}

