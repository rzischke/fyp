#include <cmath>
#include <iostream>

#include "time_series.hpp"

void print_time_series(const time_series& ts) {
    std::cout << "t,y\n";
    for (std::size_t i = 0; i != ts.t.size(); ++i) {
        std::cout << ts.t[i] << ',' << ts.y[i] << '\n';
    }
    std::cout << '\n';
}

#include "phase_fold.hpp"

int main(int argc, char **argv) {
    time_series data;
    for (std::size_t i = 0; i != 20; ++i) {
        data.t.push_back(0.3*i);
        data.y.push_back(static_cast<double>(i));
    }
    auto phased_data = phase_fold(
        data,
        1.0,
        [](double k) {
            return sqrt(-log(k));
        },
        exp(-1.5*1.5)
    );

    std::cout << "data.t.size() == " << data.t.size() << '\n';
    std::cout << "data.y.size() == " << data.y.size() << '\n';
    std::cout << "phased_data.t.size() == " << phased_data.t.size() << '\n';
    std::cout << "phased_data.y.size() == " << phased_data.y.size() << "\n\n\n";

    std::cout << "Raw Data:\n";
    print_time_series(data);

    std::cout << "Phased Data:\n";
    print_time_series(phased_data);

    return 0;
}

