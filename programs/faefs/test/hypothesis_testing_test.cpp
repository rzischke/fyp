#include <ctime>
#include <chrono>
#include <ratio>
#include <iostream>
#include "hypothesis_testing.hpp"

int main(int argc, char **argv) {
    f_crit_sig_freq_generator f_crit(100, 30, 0.05);
    std::chrono::high_resolution_clock::time_point start;
    std::chrono::high_resolution_clock::time_point stop;
    std::chrono::duration<double> time_taken;

    auto f_crit_test_loop = [&f_crit, &start, &stop, &time_taken](std::size_t begin, std::size_t end) {
        std::cout << "NumberOfRegressors,FCrit,Time(s)" << '\n';
        for (std::size_t reg = begin; reg < end; reg += 2) {
            start = std::chrono::high_resolution_clock::now();
            double f_crit_reg = f_crit(reg);
            stop = std::chrono::high_resolution_clock::now();
            time_taken = std::chrono::duration_cast<std::chrono::duration<double>>(stop-start);
            std::cout << reg << ',' << f_crit_reg << ',' << time_taken.count() << '\n';
        }
    };

    f_crit_test_loop(49, 80);
    f_crit_test_loop(3, 49);
    f_crit_test_loop(3, 80);

    return 0;
}

