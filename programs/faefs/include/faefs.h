#ifndef FAEFS_HEADER_GUARD
#define FAEFS_HEADER_GUARD

#include "complex.h"
#include "stdbool.h"

#ifdef __cplusplus
extern "C" {
#endif  

// Fast and Automatic Estimation of a Fourier Series (FAEFS).

// The faefs function estimates the fourier series of a periodic 1d
// process with a fundamental frequency of f0 using arbitrarily
// spaced observations.

void faefs(
    double f0,
    const double *y,
    const double *t,
    size_t num_obs,
    double _Complex **f,
    size_t **h,
    size_t *num_sig_freq,
    double **P,
    size_t *P_size,
    double *p_crit,
    double en_eps_abs,
    double en_eps_rel
);

void faefs_free(
    double _Complex *f,
    size_t *h,
    double *P
);

// faefs parameters
//
// f0   
// faefs assumes this is the true fundamental 
// frequency of the periodic process,
// or a factor of it. If this is not known, a
// sufficiently small f0 may result in a useful
// fourier series estimate. It is important to weigh
// this choice against the available information
// resources (i.e. the number of observations) and
// the available computing resources. A smaller
// factor of the true fundamental frequency will
// require a higher sample size and more computing
// resources to acquire an estimate of equivalent
// quality of an identical process: in terms of
// frequency component selection and in terms
// of model accuracy in both the time and
// frequency domain.
// 
// y
// An array containing observations (in noise) of the 1d
// periodic process.
//
// t
// An array containing the times of the observations in
// y. Observation y[i] was made at time t[i].
//
// num_obs
// The total number of observations of the periodic
// process, and the lengh of arrays y and t.
//
// f
// The address to store the location of the array of 
// fourier coefficients. Pass NULL if this result
// is not required from faefs, or to not have such
// memory freed by faefs_free while it frees others.
//
// h
// The address to store the location of the array of 
// harmonics corresponding to the fourier coefficients
// in *f. Pass NULL if this result is not required from
// faefs, or to not have such memory freed by faefs_free
// while it frees others.
//
// Since the number of frequencies that faefs will
// include in the resultant fourier series is unknown
// at the time of the function call, the arrays pointed
// to by f and h will be allocated by the faefs function
// call, and must be freed by faefs_free.
//
// num_sig_freq
// The address to store the number of frequencies that
// faefs has deemed significant to include in the 
// resultant fourier series. This is also the size of
// the arrays *f and *h. Pass NULL if this result is
// not required from faefs.
//
// The resultant fourier series is:
// y^hat(t) = sum_i=0^num_sig_freq {
//     creal((*f)[i])*cos(2*pi*(*h)[i]*f0*t)
//   + cimag((*f)[i])*sin(2*pi*(*h)[i]*f0*t)
// }
// Or, alternatively:
// y^hat(t) = sum_i=0^num_sig_freq {
//     creal((*f)[i]*cexp(-I*2*pi*(*h)[i]*f0*t))
// }
//
// P
// The address to store the location of the array
// containing the faefs periodogram over the range
// of harmonics considered by faefs. Pass NULL if
// this result is not required from faefs, or to
// not have such memory freed by faefs_free while
// it frees others.
// At any particular harmonic, the periodogram is
// -log10(p), where p is the p value of a hypothesis
// test where the null hypothesis is that the
// tested harmonic has a zero magnitude in the true
// fourier series of the underlying process, and
// the alternative hypothesis is that the true
// magnitude is not zero. The hypothesis test is
// valid under the assumption that (*h) contains
// all harmonics with (true) non-zero magnitude.
// For example, P[10] = 6.0 means that, under the
// assumption above, and under the null hypothesis
// that the frequency 10*f0 has zero magnitude in
// the true fourier series of the underlying
// periodic process, the probability of a particular
// test statistic (a function of the vectors y and
// t) being as large as it is (or larger) is one in
// a million.
// The more unlikely the size of the statistic, the
// more the evidence of the data is in favour of the
// position that the magnitude of the frequency of
// interest in the true fourier series of the
// underlying process is non-zero, and therefore 
// that the associated fourier series coefficients
// are worthy of estimation.
//
// P_size
// The address to store the size of the array *P.
// Pass NULL if this result is not required from
// faefs.
//
// As in *f and *h, P_size is unknown at the time
// of a faefs function call, and so memory needs
// to be allocated by the faefs function, and
// must be freed with faefs_free.
//
// P_crit
// The address to store the critical periodogram
// value, beyond which a frequency component is
// included in the resultant fourier series.
// Pass NULL if this result is not required
// from faefs. This value increasese depending
// on num_obs, so that both the probability
// of incorrectly including a truely zero
// magnitude frequency component, and incorrectly
// dismissing a truely non-zero magnitude
// frequency component, both go to zero as the
// sample size increases.*
//
// en_eps_abs
// The absolute numeric error of various estimates
// (such asof  the bound on the frequency content of
// the process), below which will be regarded as
// acceptable by the faefs algorithm.
//
// en_eps_rel
// The proportion of a standard error below which
// the numeric error of a statistical estimate
// of a quantity of interest is acceptable.
//
// Note that faefs considers is acceptable for
// one of en_eps_abs and en_eps_rel to be
// violated, as long as the other is satisfied.
//
// * Currently this result is only proven to
//   hold under the assumption that faefs
//   arrives to a set of selected frequencies
//   that contain all frequencies with non-zero
//   magnitude in the true fourier series of
//   the underlying process.
//
// TODO:
//
// Add P_hist and f_hist (and sizes) parameters to
// analyse behaviour of frequency components as a function
// of the iteration number.
//
// Noramlise data first (subtract mean and devide by
// standard deviation) before analysis, then un-normalise
// the resulting model accordingly.

#ifdef __cplusplus
}
#endif

#endif
