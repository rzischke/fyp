#include <algorithm>
#include <cmath>
#include <cstring>
#include <iterator>
#include <vector>
#include <boost/math/constants/constants.hpp>
#include <Eigen/Core>

#include "dot_product_store.hpp"
#include "hypothesis_testing.hpp"
#include "moments.hpp"
#include "nyquist.hpp"
#include "smoothing.hpp"
#include "state.hpp"

// These need to be last since they define a macro "I" = sqrt(-1)
// that is used as a template letter in other headers.
#include "complex.h"
#include "faefs.h"

using complexd = double _Complex;

extern "C" void faefs(
    double f0,
    const double *y,
    const double *t,
    size_t num_obs,
    complexd **f,
    size_t **h,
    size_t *num_sig_freq,
    double **P,
    size_t *P_size,
    double *p_crit,
    double en_eps_abs,
    double en_eps_rel
) {
    bool fill_periodogram = (P != NULL);

    std::size_t nyquist_harmonic = get_nyquist_harmonic(f0, t, num_obs, en_eps_abs);

    double y_sample_mean = get_sample_mean(y, num_obs);
    double y_sample_var = get_sample_variance(y, num_obs, y_sample_mean);
    double y_sample_stddev = sqrt(y_sample_var);

    double t0 = t[0];
    double t_range = t[num_obs-1] - t0;

    std::vector<double> y_normalised;
    y_normalised.reserve(num_obs);
    std::vector<double> t_normalised;
    t_normalised.reserve(num_obs);
    
    for (std::size_t i = 0; i != num_obs; ++i) {
        y_normalised.push_back((y[i] - y_sample_mean)/y_sample_stddev);
        t_normalised.push_back((t[i] - t0)/t_range);
    }

    double f0_normalised = f0*t_range;
    constexpr double y_sample_mean_normalised = 0.0;
    constexpr double y_sample_var_normalised = 1.0;
    constexpr double y_sample_stddev_normalised = 1.0;
    double en_eps_abs_normalised = en_eps_abs/y_sample_stddev;

    faefs_state state(
        f0_normalised,
        time_series(
            std::move(y_normalised),
            std::move(t_normalised)
        ),
        y_sample_mean_normalised,
        y_sample_var_normalised,
        y_sample_stddev_normalised,
        en_eps_abs_normalised,
        en_eps_rel
    );

    std::size_t iteration = 0;

    do { // while(state.update_smoothed_energy_estimate_for_current_candidate_model());
        bool model_seen_before;
        do { // while (!model_seen_before);
            state.allocate_wald_test_least_squares_memory();
            state.reset_energy_sum();
            for (std::size_t harmonic = 1; !state.energy_budget_exceeded() && harmonic < nyquist_harmonic; ++harmonic) {
                auto significant_harmonic_location = state.find_significant_harmonic(harmonic);
                state.fold_harmonic_into_current_model(harmonic, significant_harmonic_location);
                if (state.harmonic_has_significantly_nonzero_magnitude(harmonic, significant_harmonic_location, fill_periodogram)) {
                    state.add_harmonic_to_next_model(harmonic);
                    state.add_harmonic_to_energy_sum(significant_harmonic_location);
                }
            }
            state.reset_energy_sum();
            model_seen_before = state.consolidate_if_repeated_model();
            if (!model_seen_before) {
                state.update_candidate_model_for_next_iteration();
            }
            ++iteration;
        } while (!model_seen_before);
    } while (state.update_smoothed_energy_estimate_for_current_candidate_model());

    size_t num_sig_freq_out = state.current_significant_harmonics.size();
    *num_sig_freq = num_sig_freq_out;

    size_t *h_out = new size_t[num_sig_freq_out];
    for (std::size_t i = 0; i != num_sig_freq_out; ++i) {
        memcpy(h_out, state.current_significant_harmonics.data(), num_sig_freq_out*sizeof(size_t));
    }
    *h = h_out;

    double w0 = 2*boost::math::constants::pi<double>()*f0;
    complexd *f_out = new complexd[num_sig_freq_out];
    f_out[0] = (y_sample_stddev*state.b_sig(0) + y_sample_mean) + I*0.0;
    for (std::size_t i = 1; i != num_sig_freq_out; ++i) {
        std::size_t hi = state.current_significant_harmonics[i];
        double cos_proj_normalised = state.b_sig(2*i-1);
        double sin_proj_normalised = state.b_sig(2*i);
        double phase_shift_cos = cos(hi*w0*t0);
        double phase_shift_sin = sin(hi*w0*t0);
        double cos_proj = y_sample_stddev*(phase_shift_cos*cos_proj_normalised - phase_shift_sin*sin_proj_normalised);
        double sin_proj = y_sample_stddev*(phase_shift_sin*cos_proj_normalised + phase_shift_cos*sin_proj_normalised);
        f_out[i] = cos_proj + I*sin_proj;
    }
    *f = f_out;

    if (fill_periodogram) {
        size_t P_size_out = std::min(state.current_significant_harmonics.back()+1, state.periodogram.size()); // throw warning here.
        if (P_size != NULL) *P_size = P_size_out;

        double *P_out = new double[P_size_out];
        memcpy(P_out, state.periodogram.data(), P_size_out*sizeof(double));
        *P = P_out;
    }

    if (p_crit != NULL) *p_crit = -log10(state.f_crit_generator.get_test_size());

}

extern "C" void faefs_free(
    complexd *f,
    size_t *h,
    double *P
) {
    if (f != NULL) delete[] f;
    if (h != NULL) delete[] h;
    if (P != NULL) delete[] P;
}

