#define EIGEN_DONT_VECTORIZE
#define EIGEN_DISABLE_UNALIGNED_ARRAY_ASSERT

#include "cli_utilities.hpp"
#include "time_series.hpp"
#include "model.hpp"
#include "rng.hpp"
#include "energy_regularised_lssa.hpp"
#include "kahan_sum.hpp"
#include <algorithm>
#include <cmath>
#include <iterator>
#include <memory>
#include <boost/math/constants/constants.hpp>
#include <boost/math/distributions/chi_squared.hpp>
#include <Eigen/Core>
#include <Eigen/QR>
#include <Eigen/SVD>

namespace gof {

    /*
    double trapezoidal_rule_energy(const time_series& ts) {
        // Use the other sum: this will mean that all additions are put through
        // the kahan summer.
        auto summer = make_kahan_summer(static_cast<double>(0.0));
        for (std::size_t i = 1; i < ts.y.size(); ++i) {
            auto yi = ts.y[i];
            auto yim1 = ts.y[i-1];
            summer.add((ts.t[i]-ts.t[i-1])*(yi*yi+yim1*yim1));
        }
        return 0.5*summer.get_sum();
    }
    */

    double trapezoidal_rule_energy(const time_series& ts, double sample_mean) {
        auto summer = make_kahan_summer(static_cast<double>(0.0));

        auto sample_mean2 = sample_mean*sample_mean;
        auto n = ts.y.size();

        auto y0 = ts.y[0];
        auto t0 = ts.t[0];
        summer.add(-t0*y0*y0);
        summer.add(2*t0*y0*sample_mean);
        summer.add(-t0*sample_mean2);

        for (std::size_t i = 1; i < n; ++i) {
            auto yi = ts.y[i];
            auto ti = ts.t[i];
            auto yim1 = ts.y[i-1];
            auto tim1 = ts.t[i-1];

            summer.add(ti*yim1*yim1);
            summer.add(-2*ti*yim1*sample_mean);
            summer.add(ti*sample_mean2);

            summer.add(-tim1*yi*yi);
            summer.add(2*tim1*yi*sample_mean);
            summer.add(-tim1*sample_mean2);
        }

        auto nm1 = n-1;
        auto ynm1 = ts.y[nm1];
        auto tnm1 = ts.t[nm1];
        summer.add(tnm1*ynm1*ynm1);
        summer.add(-2*tnm1*ynm1*sample_mean);
        summer.add(tnm1*sample_mean2);

        return 0.5*summer.get_sum()/(ts.t.back() - ts.t.front());
    }

    class energy_regularised_lssa : public model {
        public:
            energy_regularised_lssa(double f0_in): f0(f0_in) { }

            std::string name(void) override { return "Model 1: Lomb-Scargle Periodogram Method."; }
            std::string description(void) override { return "TODO: model 1 Lomb-Scargle periodogram description."; }

            energy_regularised_lssa_result_1d fit(const time_series& ts, gof::rng *rng) {
                constexpr double two_pi = 2*boost::math::constants::pi<double>();

                auto n = ts.t.size();
                auto nm1 = n-1;
                auto sqrt_n = sqrt(static_cast<double>(n));

                auto sample_mean = kahan_sum(ts.y.cbegin(), ts.y.cend(), static_cast<double>(0.0))/ts.y.size();

                auto energy_estimate = trapezoidal_rule_energy(ts,sample_mean);

                std::vector<double> delta_t(n - 1);
                for (std::size_t i = 0; i != n-1; ++i) {
                    delta_t[i] = ts.t[i+1] - ts.t[i];
                }
                auto min_delta_t = *std::min_element(delta_t.cbegin(), delta_t.cend()); // Segfault if delta_t is empty.
                auto nyquist_frequency = 0.5/min_delta_t;

                std::cout << "nyquist_frequency = " << nyquist_frequency << std::endl;
                std::cout << "energy_estimate = " << energy_estimate << std::endl;

                std::vector<double> f_hat;
                auto energy_budget_remaining_summer = make_kahan_summer(energy_estimate);
                for (std::size_t harmonic = 1; energy_budget_remaining_summer.get_sum() > 0.0 && harmonic*f0 < nyquist_frequency; ++harmonic) {
                    auto f = harmonic*f0;
                    auto w = two_pi*f;
                    auto two_w = 2*w;

                    auto sin_sum_summer = make_kahan_summer(0.0);
                    auto cos_sum_summer = make_kahan_summer(0.0);
                    for (std::size_t j = 0; j != n; ++j) {
                        auto tj = ts.t[j];
                        sin_sum_summer.add(sin(two_w*tj));
                        cos_sum_summer.add(cos(two_w*tj));
                    }
                    auto sin_sum = sin_sum_summer.get_sum();
                    auto cos_sum = cos_sum_summer.get_sum();
                    auto tau = atan2(sin_sum, cos_sum)/two_w;
                    
                    std::cout << "tau = " << tau << std::endl;

                    auto y_cos_sum_summer = make_kahan_summer(0.0);
                    auto y_sin_sum_summer = make_kahan_summer(0.0);
                    for (std::size_t j = 0; j != n; ++j) {
                        auto tj = ts.t[j];
                        auto yj = ts.y[j];
                        auto wtjmtau = w*(tj-tau);
                        y_cos_sum_summer.add((yj-sample_mean)*cos(wtjmtau));
                        y_sin_sum_summer.add((yj-sample_mean)*sin(wtjmtau));
                    }
                    auto y_cos_sum = y_cos_sum_summer.get_sum();
                    auto y_sin_sum = y_sin_sum_summer.get_sum();
                    
                    auto y_cos_mean = y_cos_sum/n;
                    auto y_sin_mean = y_sin_sum/n;

                    // Make more efficient by using variance - won't have to calcualte sqrt.
                    auto y_cos_stddev_summer = make_kahan_summer(0.0);     // rename to smd (squared mean deviation) summer
                    auto y_sin_stddev_summer = make_kahan_summer(0.0);
                    for (std::size_t j = 0; j != n; ++j) {
                        auto tj = ts.t[j];
                        auto yj = ts.y[j];
                        auto wtjmtau = w*(tj-tau);
                        auto yj_cos_wtjmtau = (yj-sample_mean)*cos(wtjmtau);
                        auto yj_sin_wtjmtau = (yj-sample_mean)*sin(wtjmtau);
                        auto yj_cos_wtjmtau_m_y_cos_mean = yj_cos_wtjmtau - y_cos_mean;
                        auto yj_sin_wtjmtau_m_y_sin_mean = yj_sin_wtjmtau - y_sin_mean;
                        y_cos_stddev_summer.add(yj_cos_wtjmtau_m_y_cos_mean * yj_cos_wtjmtau_m_y_cos_mean);
                        y_sin_stddev_summer.add(yj_sin_wtjmtau_m_y_sin_mean * yj_sin_wtjmtau_m_y_sin_mean);
                    }
                    auto y_cos_stddev = sqrt(y_cos_stddev_summer.get_sum()/nm1);
                    auto y_sin_stddev = sqrt(y_sin_stddev_summer.get_sum()/nm1);

                    // These are N(0,1) under null that process has no power at frequency f.
                    auto y_cos_sum_normalised = y_cos_sum/(sqrt_n*y_cos_stddev);
                    auto y_sin_sum_normalised = y_sin_sum/(sqrt_n*y_sin_stddev);

                    // Test statistic has a chi squared distribution with two degrees of fredom under null.
                    // Note that because of tau, the (null) covariance between the two components are zero.
                    auto test_statistic = y_cos_sum_normalised*y_cos_sum_normalised + y_sin_sum_normalised*y_sin_sum_normalised;
                    std::cout << "test_statistic = " << test_statistic << std::endl;

                    // In here decide whether to accept or reject frequency. You'll probably need the energy_regularised_lssa_result from the start.
                    // If accept frequency, decrement (1/2)*(a*a + b*b) from energy budget.

                    if (boost::math::cdf(boost::math::chi_squared(2.0), test_statistic) > 0.95) {   // Make test size configurable.
                        f_hat.emplace_back(f);

                        auto cos_2_sum_summer = make_kahan_summer(0.0);
                        auto sin_2_sum_summer = make_kahan_summer(0.0);
                        for (std::size_t j = 0; j != n; ++j){
                            auto tj = ts.t[j];
                            auto cos_w_tj_m_tau = cos(w*(tj-tau));
                            auto sin_w_tj_m_tau = sin(w*(tj-tau));
                            cos_2_sum_summer.add(cos_w_tj_m_tau*cos_w_tj_m_tau);
                            sin_2_sum_summer.add(sin_w_tj_m_tau*sin_w_tj_m_tau);
                        }
                        auto cos_2_sum = cos_2_sum_summer.get_sum();
                        auto sin_2_sum = sin_2_sum_summer.get_sum();
                        auto cos_2_sum_2 = cos_2_sum*cos_2_sum;
                        auto sin_2_sum_2 = sin_2_sum*sin_2_sum;
                        auto y_cos_sum_2 = y_cos_sum*y_cos_sum;
                        auto y_sin_sum_2 = y_sin_sum*y_sin_sum;
                        auto lomb_scargle_cos_projection = y_cos_sum_2/cos_2_sum_2;
                        auto lomb_scargle_sin_projection = y_sin_sum_2/sin_2_sum_2;

                        // THIS IS NOT THE LOMB-SCARGLE PERIODOGRAM (DIVIDING BY AN EXTRA COS/SIN SUM)
                        // NEED TO EXPLORE AND DOCUMENT THE DIFFERENCE.
                        energy_budget_remaining_summer.add(-(0.5)*(lomb_scargle_sin_projection));   // Not 100% sure of these, will need to diagnose.
                        energy_budget_remaining_summer.add(-(0.5)*(lomb_scargle_cos_projection));

                        std::cout << "y_cos_sum_2 = " << y_cos_sum_2 << std::endl;
                        std::cout << "cos_2_sum = " << cos_2_sum << std::endl;
                        std::cout << "cos_2_sum_2 = " << cos_2_sum_2 << std::endl;
                        std::cout << "y_sin_sum_2 = " << y_sin_sum_2 << std::endl;
                        std::cout << "sin_2_sum = " << sin_2_sum << std::endl;
                        std::cout << "sin_2_sum_2 = " << sin_2_sum_2 << std::endl;
                        std::cout << "energy_budget_remaining = " << energy_budget_remaining_summer.get_sum() << std::endl;
                        std::cout << "magnitude = " << std::hypot(lomb_scargle_cos_projection, lomb_scargle_sin_projection) << std::endl;
                    }
                } // for(std::size_t harmonic = ...

                // In here do least squares to find a better estimate of the frequency coefficients and intercept.

                Eigen::Map<const Eigen::VectorXd> y_eigen(ts.y.data(),ts.y.size());
                //Eigen::Matrix<double, Eigen::Dynamic, 1, Eigen::DontAlign> y_eigen(n);
                //for (std::size_t i = 0; i != n; ++i) y_eigen(i) = ts.y[i];

                Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::DontAlign> X(n,1 + f_hat.size()*2);
                for (std::size_t i = 0; i != n; ++i) {
                    auto ti = ts.t[i];
                    X(i,0) = 1.0;
                    for (std::size_t j = 0; j != f_hat.size(); ++j) {
                        // Do storage and computation in terms of angular frequency rather than
                        // Hz frequency, for the sake of this inner loop having to convert every time.
                        auto w_hat_j_ti = two_pi*f_hat[j]*ti;
                        X(i,2*j+1) = cos(w_hat_j_ti);
                        X(i,2*j+2) = sin(w_hat_j_ti);
                    }
                }

                //Eigen::VectorXd b(1 + f_hat.size()*2);
                //Eigen::VectorXd b = X.fullPivHouseholderQr().solve(y_eigen);
                std::cout << "f_hat.size() == " << f_hat.size() << std::endl;
                std::cout << "f_hat.back() == " << f_hat.back() << std::endl;
                std::cout << "min_delta_t == " << min_delta_t << std::endl;
                std::cout << "nyquist_frequency == " << nyquist_frequency << std::endl;
                std::cout << "Before solve." << std::endl;
                Eigen::VectorXd b = X.bdcSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(y_eigen);
                std::cout << "After solve." << std::endl;

                double gamma_hat = b(0);
                std::vector<double> a_hat(f_hat.size());
                std::vector<double> b_hat(f_hat.size());
                for (std::size_t j = 0; j != f_hat.size(); ++j) {   // This copy is bothersom, should just map b onto a likewise structured std::vector, which becomes the storage.
                                                                    // Then, have functions to access the ith cos coefficient, or the ith sin coefficient, or the intercept.
                    a_hat[j] = b(2*j+1);
                    b_hat[j] = b(2*j+2);
                }

                // Eventually, the whole thing should be using w, but for now:
                std::vector<double> angular_frequencies_hat;
                angular_frequencies_hat.reserve(f_hat.size());
                std::transform(
                    f_hat.cbegin(),
                    f_hat.cend(),
                    std::back_inserter(angular_frequencies_hat),
                    [two_pi](double f) { return two_pi*f; }
                );
                return energy_regularised_lssa_result_1d(
                    gamma_hat,
                    angular_frequencies_hat,
                    a_hat,
                    b_hat
                );
            }

            std::unique_ptr<model_result> fit(const std::vector<time_series>& dataset, gof::rng *rng) override {
                std::vector<energy_regularised_lssa_result_1d> results;
                results.reserve(dataset.size());
                for (auto iter = dataset.cbegin(); iter != dataset.cend(); ++iter) {
                    results.emplace_back(fit(*iter, rng));
                }
                return std::make_unique<energy_regularised_lssa_result>(std::move(results));
            }

        private:
            double f0;  // At some point change to support multiple fundamentals.

    };

}

extern "C" std::unique_ptr<gof::model> gof_plugin_factory(int argc, const char * const argv[]) {
    const char *lib_file_name = argv[0];

    auto print_help = [lib_file_name]() {
        std::cout << " uses the energy regularised lssa periodogram method to model the data.\n"
                     "Usage: " << lib_file_name << "-f/--fundamental-frequency <frequency>" << std::endl;
    };

    gof::assert_argc_eq(argc, 3, lib_file_name, print_help);

    boost::optional<double> f0;
    for (int i = 1; i != argc; ++i) {
        if (gof::check_and_get_option(f0, i, argc, argv, "-f", "--fundamental-frequency", print_help)) { }
    }
    gof::assert_arg_set(f0, "-f/--fundamental-frequency", lib_file_name, print_help);

    return std::make_unique<gof::energy_regularised_lssa>(
        *f0
    );
}

