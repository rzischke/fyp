#ifndef GOF_ENERGY_REGULARISED_LSSA_GUARD
#define GOF_ENERGY_REGULARISED_LSSA_GUARD

#include <cmath>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <vector>
#include <boost/math/constants/constants.hpp>
#include "sine_series.hpp"

namespace gof {

    class energy_regularised_lssa_result_1d {
        public:
            energy_regularised_lssa_result_1d(
                double gamma_in,
                std::vector<double> angular_frequencies_in,
                std::vector<double> cos_projections_in,
                std::vector<double> sin_projections_in
            ):
                gamma(gamma_in),
                sine_series(
                    sine_series_to_polar_cos(
                        construct_sine_series<sine_series_rectangular<double>>(
                            angular_frequencies_in,
                            cos_projections_in,
                            sin_projections_in
                        )
                    )
                )
            { }

            double estimate_implementation(double time) {
                return gamma + sine_series_at_time(sine_series,time);
            }

            double gamma;
            sine_series_polar_cos<double> sine_series;
    };

    class energy_regularised_lssa_result: public model_result {
        public:
            energy_regularised_lssa_result(
                std::vector<energy_regularised_lssa_result_1d> results_in
            ):
                model_result(results_in.size()),    // register process dimension
                results(std::move(results_in))
            { }

            std::string name(void) override { return "Sum of Sinusoids Model Result"; }

            std::string description(void) override {
                std::ostringstream ss;
                ss << "\\begin{align*}\n"
                      "    \\hat{y}(t)_j &= "
                              "\\hat{\\gamma}_j "
                              "+ \\sum_{l} \\Big( "
                                  "\\hat{a}_{j,l} \\cos (2 \\pi \\hat{f}_{j,l} t) "
                                  "+ \\hat{b}_{j,l} \\sin (2 \\pi \\hat{f}_{j,l} t)"
                              "\\Big) "
                          "\\\\\n"
                      "    &= "
                              "\\hat{\\gamma}_j "
                              "+ \\sum_{l} \\Big( "
                                  "\\|(\\hat{a}_{j,l},\\hat{b}_{j,l})\\| \\cos \\big( "
                                      "2 \\pi \\hat{f}_{j,l} t - \\angle (\\hat{a}_{j,l},\\hat{b}_{j,l}) "
                                  "\\big) "
                              "\\Big) "
                          "\\\\\n"
                      "\\end{align*}\n"
                      "\\begin{center}\n"
                      "\\begin{tabular}{|c|l l l l l l|}\n";

                for (std::size_t j = 0; j != results.size(); ++j) {
                    const auto& sine_series_j = results[j].sine_series;

                    auto f_hdr = hat_idx('f',j,'l');
                    auto a_hdr = hat_idx('a',j,'l');
                    auto b_hdr = hat_idx('b',j,'l');
                    auto ab = "(" + a_hdr + "," + b_hdr + ")";

                    ss << "    \\hline\n"
                          "    \\multicolumn{7}{|c|}{$ j = " << j << " $} \\\\\n"
                          "    \\hline\n"
                          "    $l$ & $"
                               << f_hdr << "$ & $"
                               << a_hdr << "$ & $"
                               << b_hdr << "$ & $"
                               "\\|" << ab << "\\|$ & $"
                               "-\\angle " << ab << "\\ \\mathrm{rad}$ & $"
                               "-\\angle " << ab << "^{\\circ}$ "
                               "\\\\\n"
                          "    \\hline\n";
                    for (std::size_t l = 0; l != sine_series_j.size(); ++l) {
                        const auto& sine_wave_j_l = sine_series_j[l];
                        ss << "    "
                           << l << " & "
                           << sine_wave_j_l.get_frequency() << " & "
                           << sine_wave_j_l.get_cos_projection() << " & "
                           << sine_wave_j_l.get_sin_projection() << " & "
                           << sine_wave_j_l.get_magnitude() << " & "
                           << sine_wave_j_l.get_cos_phase_rad() << " & "
                           << sine_wave_j_l.get_cos_phase_deg() << " "
                              "\\\\\n";
                    }

                    ss << "    \\hline\n";
                }

                ss << "\\end{tabular}\n"
                      "\\end{center}\n"
                      "\n";

                return ss.str();
            }

            std::vector<std::string> latex_dependencies(void) override {
                return {"amsmath","mathtools","gensymb","longtable"};
            }

        protected:
            double estimate_implementation(std::size_t i, double t) {
                return results[i].estimate_implementation(t);
            }

        private:
            std::vector<energy_regularised_lssa_result_1d> results;

            template <class Arg>
            void idx_impl(std::ostringstream& ss, const Arg& idx) {
                ss << idx;
            }

            template <class Arg, class... Args>
            void idx_impl(std::ostringstream& ss, const Arg& idx, const Args&... the_rest) {
                ss << idx << ',';
                idx_impl(ss, the_rest...);
            }

            template <class Param, class... Args>
            void idx(const Param& param, const Args&... idxs) {
                std::ostringstream ss;
                ss << param << "_{";
                idx_impl(ss, idxs...);
                ss << '}';
                return ss.str();
            }

            template <class Param, class... Args>
            std::string hat_idx(const Param& param, const Args&... idxs) {
                std::ostringstream ss;
                ss << "\\hat{" << param << "}_{";
                idx_impl(ss, idxs...);
                ss << '}';
                return ss.str();
            }

            template <class Param, class... Args>
            std::string hat_t_idx(const Param& param, const Args&... idxs) {
                std::ostringstream ss;
                ss << "\\hat{" << param << "}(t)_{";
                idx_impl(ss, idxs...);
                ss << '}';
                return ss.str();
            }
    };

}


#endif
