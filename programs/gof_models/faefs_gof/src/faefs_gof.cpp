#include <iostream>
#include <memory>
#include <utility>
#include <boost/math/constants/constants.hpp>
#include <boost/optional.hpp>
#include "complex.h"
#include "cli_utilities.hpp"
#include "faefs.h"
#include "faefs_gof.hpp"
#include "model.hpp"
#include "rng.hpp"
#include "time_series.hpp"

namespace gof {

    class faefs_gof : public model {
        public:
            faefs_gof(double f0_in): f0(f0_in) { }

            std::string name(void) override { return "Fast and Automatic Estimation of a Fourier Series"; }
            std::string description(void) override { return "TODO: FAEFS description."; }

            faefs_result_1d fit(const time_series& data, gof::rng*) {
                double _Complex *f;
                size_t *h;
                size_t num_sig_freq;
                double en_eps_abs = 1e-6;
                double en_eps_rel = 1e-2;

                faefs(
                    f0,
                    data.y.data(),
                    data.t.data(),
                    data.y.size(),
                    &f,
                    &h,
                    &num_sig_freq,
                    nullptr,
                    nullptr,
                    nullptr,
                    en_eps_abs,
                    en_eps_rel
                );

                std::vector<double> angular_frequencies;
                std::vector<double> cos_projections;
                std::vector<double> sin_projections;
                angular_frequencies.reserve(num_sig_freq);
                cos_projections.reserve(num_sig_freq);
                sin_projections.reserve(num_sig_freq);
                constexpr double two_pi = 2*boost::math::constants::pi<double>();
                double gamma = 0.0;
                if (num_sig_freq != 0) gamma = creal(f[0]);
                for (std::size_t i = 1; i < num_sig_freq; ++i) {
                    angular_frequencies.push_back(two_pi*h[i]*f0);
                    cos_projections.push_back(creal(f[i]));
                    sin_projections.push_back(cimag(f[i]));
                }

                faefs_free(f, h, nullptr);

                return faefs_result_1d(
                    gamma,
                    std::move(angular_frequencies),
                    std::move(cos_projections),
                    std::move(sin_projections)
                );
            }

            std::unique_ptr<model_result> fit(const std::vector<time_series>& dataset, gof::rng *rng) override {
                std::vector<faefs_result_1d> results;
                results.reserve(dataset.size());
                for (auto iter = dataset.cbegin(); iter != dataset.cend(); ++iter) {
                    results.emplace_back(fit(*iter, rng));
                }
                return std::make_unique<faefs_result>(std::move(results));
            }

        private:
            double f0;  // At some point change to support multiple fundamentals.
    };

}

extern "C" std::unique_ptr<gof::model> gof_plugin_factory(int argc, const char * const argv[]) {
    const char *lib_file_name = argv[0];

    auto print_help = [lib_file_name]() {
        std::cout << "Uses the FAEFS (Fast and Automatic Estimation of a Fourier Series) algorithm to model the data.\n"
                     "Usage: " << lib_file_name << "-f/--fundamental_frequency <frequency>" << std::endl;
    };

    gof::assert_argc_eq(argc, 3, lib_file_name, print_help);

    boost::optional<double> f0;
    for (int i = 1; i != argc; ++i) {
        if (gof::check_and_get_option(f0, i, argc, argv, "-f", "--fundamental-frequency", print_help)) { }
    }
    gof::assert_arg_set(f0, "-f/--fundamental_frequency", lib_file_name, print_help);

    return std::make_unique<gof::faefs_gof>(
        *f0
    );
}

