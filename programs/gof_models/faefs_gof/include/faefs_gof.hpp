#ifndef GOF_AUTO_LSSA_GUARD
#define GOF_AUTO_LSSA_GUARD

#include "model.hpp"
#include "sine_series.hpp"

namespace gof {

    class faefs_result_1d {
        public:
            faefs_result_1d(
                double gamma_in,
                const std::vector<double>& angular_frequencies_in,
                const std::vector<double>& cos_projections_in,
                const std::vector<double>& sin_projections_in
            ):
                gamma(gamma_in),
                sine_series(
                    construct_sine_series<sine_series_rectangular<double>>(
                        angular_frequencies_in,
                        cos_projections_in,
                        sin_projections_in
                    )
                )
            { }

            double estimate_implementation(double time) {
                if (!sine_series.empty()) {
                    return gamma + sine_series_at_time(sine_series,time);
                } else {
                    return gamma;
                }
            }

            double gamma;
            sine_series_rectangular<double> sine_series;
    };

    class faefs_result: public model_result {
        public:
            faefs_result(
                std::vector<faefs_result_1d> results_in
            ):
                model_result(results_in.size()),    // register process dimension
                results(std::move(results_in))
            { }

        protected:
            double estimate_implementation(std::size_t i, double t) {
                return results[i].estimate_implementation(t);
            }

            std::string name(void) override { return "Sum of Sinusoids Model Result"; }

            std::string description(void) override {
                return std::string("TODO: Sum of Sinusoids Description.");
            }

        private:
            std::vector<faefs_result_1d> results;
    };
}

#endif
