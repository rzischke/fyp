#include "cli_utilities.hpp"
#include "observation_times_simulator.hpp"
#include "plugin.hpp"
#include "rng.hpp"
#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <random>
#include <stdexcept>
#include <sstream>
#include <vector>
#include <utility>
#include <boost/optional.hpp>

#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"    // To suppress unitialized error/warning on boost::optionals.

namespace gof {
    
    class exp : public observation_times_simulator {
        public:

            exp(
                std::size_t dimension_of_process_in,
                double mean_in,
                double max_time_in
            ):
                dimension_of_process(dimension_of_process_in),
                dist(1.0/mean_in),
                max_time(max_time_in)
            { }

            std::string name(void) override { return "Exponential Observation Times Simulator"; }
            std::string description(void) override {
                std::ostringstream ss;
                ss << "Simulates observation times with an exponential distribution with a mean time between samples of " << 1.0/dist.lambda() << ".";
                return ss.str();
            }

            std::vector<std::vector<double>> simulate(gof::rng *rng) override {
                std::vector<std::vector<double>> observation_times;
                for (decltype(dimension_of_process) i = 0; i != dimension_of_process; ++i) {
                    std::vector<double> ti = {dist(*rng)};
                    while (ti.back() < max_time) ti.push_back(ti.back() + dist(*rng));
                    observation_times.push_back(ti);
                }
                return observation_times;
            }

        private:
            std::vector<std::vector<double>>::size_type dimension_of_process;
            std::exponential_distribution<double> dist;
            double max_time;
    };

}

extern "C" std::unique_ptr<gof::observation_times_simulator> gof_plugin_factory(int argc, const char * const argv[]) {
    const char *lib_file_name = argv[0];

    auto print_help = [lib_file_name]() {
        std::cout <<
            lib_file_name << " simulates observation times with an exponential distribution "
            "over the time between consecutive samples.\n"
            "Usage: " << lib_file_name << " -u/--mean <mean time between consecutive samples> -T/--max-time <most future time> [-D/--dimension <dimension of process>]" << std::endl;
    };

    gof::assert_argc_geq(argc, 2, lib_file_name, print_help);

    boost::optional<size_t> dimension_of_process;
    boost::optional<double> max_time;
    boost::optional<double> mean_time_between_samples;
    boost::optional<uint64_t> seed;
    for (int i = 1; i != argc; ++i) {
        if      (gof::check_and_get_option(dimension_of_process, i, argc, argv, "-ny", "--dimension", print_help)) { }
        else if (gof::check_and_get_option(max_time, i, argc, argv, "-T", "--max-time", print_help, gof::CLI_ARG_POS)) { }
        else if (gof::check_and_get_option(mean_time_between_samples, i, argc, argv, "-u", "--mean", print_help, gof::CLI_ARG_POS)) { }
        else gof::unrecognized_argument(argv[i], lib_file_name, print_help);
    }
    gof::assert_arg_set(max_time, "-T/--max-time", lib_file_name, print_help);
    gof::assert_arg_set(mean_time_between_samples, "-u/--mean", lib_file_name, print_help);
    if (!dimension_of_process) dimension_of_process = 1;

    return std::make_unique<gof::exp>(*dimension_of_process, *mean_time_between_samples, *max_time);
}
    
/*
std::vector<double> simulate_observation_times(int argc, const char * const argv[]) {    // argv[0] should be file name of lib, argv[1] should be exp mean.
    const char *lib_file_name = argv[0];

    auto print_help = [lib_file_name]() {
        std::cout <<
            lib_file_name << " simulates observation times with an exponential distribution "
            "over the time between consecutive samples.\n"
            "Usage: " << lib_file_name << " <mean time between consecutive samples> [-s/--seed <64big-seed>]" << std::endl;
    };

    gof::assert_argc_geq(argc, 2, lib_file_name, print_help);

    boost::optional<std::vector<double>::size_type> sample_size;
    boost::optional<double> mean;
    boost::optional<gof::seed_type> seed;
    for (int i = 1; i != argc; ++i) {
        if      (gof::check_and_get_option(sample_size, i, argc, argv, "-n", "--samplesize", print_help)) { }
        else if (gof::check_and_get_option(mean, i, argc, argv, "-u", "--mean", print_help, gof::CLI_ARG_NONNEG)) { }
        else if (gof::check_and_get_option(seed, i, argc, argv, "-s", "--seed", print_help)) { }
        else break;
    }
    gof::assert_arg_set(sample_size, "--samplesize", lib_file_name, print_help); 
    gof::assert_arg_set(mean, "--mean", lib_file_name, print_help);
    auto rng = seed ? gof::get_rng(lib_file_name, *seed) : gof::get_rng(lib_file_name);

    std::exponential_distribution<double> dist(1/(*mean));
    std::vector<double> times(*sample_size);
    std::generate(begin(times), end(times), [&dist, &rng]() { return dist(rng); });

    return times;
}
*/

