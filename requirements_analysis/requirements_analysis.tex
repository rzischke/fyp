\documentclass[journal]{IEEEtran}

\usepackage{amsmath,amssymb,mathtools,relsize,enumerate,multirow}

\DeclareMathOperator{\vect}{vec}
\DeclareMathOperator{\cov}{cov}
\DeclareMathOperator{\vech}{vech}
\DeclareMathOperator{\derivd}{\mathrm{d}\!}
\DeclareMathOperator{\derivdpow}{\mathrm{d}}
\DeclareMathOperator{\derivD}{\mathrm{D}\!}
\DeclareMathOperator{\derivDpow}{\mathrm{D}}
\newcommand\iidsim{\stackrel{i.i.d.}{\sim}}

\begin{document}

\title{Requirements Analysis for Modelling of Periodic Time-Series Processes with Unevenly-spaced Data applied to Cellular Telecommunications Networks} 
\author{Ryan~Zischke}

\maketitle

\begin{figure*}
	\centering
	\includegraphics[width=18.2cm]{Figure1}
	\caption{SMS and phone call activity during November of 2013 on the Telecom Italia network in the Trento province of Italy. The activity of a given action is proportional to it's quantity in the previous ten minutes.}
	\label{fig:telecom}
\end{figure*}

\section{Introduction}

\IEEEPARstart{M}{any} time series analysis techniques make assumptions about datasets that impede the creation of analysis systems that can be operated efficiently and by non-experts. Three such assumptions are considered:
\begin{enumerate}
	\item samples are evenly spaced, and different features of a multivariate process are sampled simultaneously;
	\item fine-grained modelling judgements are available; and
	\item the data generating process and its parameters are unchanging over time.
\end{enumerate}

Datasets can violate the first assumption due to sampling system failures, sampling frequency changes and the presence of periods of time when sampling is impossible. The requirement of prior fine-grained modelling judgements forces operators to provide expert information on a case-by-case basis. Likewise, the third assumption mandates that operators manually remove outliers from the data. These three assumptions force operators to have an intimate understanding of underlying data generating processes, how data is sampled and any analysis techniques applied. Additionally, these assumptions require operators to engage deeply with many datasets, limiting opportunities for automation and abstraction.

Computer scientists write database management systems, which are then used by database administrators to create databases, and then these databases are queried by software developers who write user-facing applications. While software developers, database administrators and computer scientists may possess unique skills, this specialisation has not yet been achieved for time-series data analysis systems.

This project aims to create new time-series analysis techniques that have greater robustness, generality and ease of use than existing methods. The goal of these techniques is to be usable in time-series analysis systems that would facilitate the kind of role specialisation offered by database management systems. This will be achieved by relaxing the above assumptions, in that order of priority, and with models that capture the properties one would generally expect to encounter in organisational data.

One context where time-series analysis techniques would be valuable is the modelling of data generating processes in telecommunications networks. For example, network elements deviating from their respective models of healthy behaviour may indicate a network fault. However, the assumptions enumerated above are not compatible with this context. Many time-series processes in telecommunications are multivariate (consider upstream throughput and number of customers connected), and different features may be sampled out of phase or may have different sampling frequencies altogether. Data on telecommunications networks may be collected over a long period of time, increasing the risk of sampling system failures over the lifetime of the dataset. If different features of a multivariate process are sampled with different sampling systems, these failures are likely to occur over different time periods. A telecommunications company might want to change sampling frequencies, which would also violate the assumption that samples are evenly spaced. Telecommunications networks can have a large number of datasets corresponding to a large number of network elements, so it might not be possible to manually examine each dataset to make modelling judgements. Network changes mean that data generating processes are not likely to remain statistically stationary in the long term. These incompatibilities with the above assumptions limit the practicality of many classical time-series analysis techniques in the context of modelling in large telecommunications networks. As such, different techniques are necessary to conduct time series analysis in this environment.

Figure \ref{fig:telecom} on page \pageref{fig:telecom} is a plot of phone call activity and sms activity over a one month period on the Telecom Italia network in the Trento province of Italy. By visual inspection, this multivariate process is likely to be periodic, autocorrelated and heteroskedastic. The time-series analysis techniques explored in this project will aim to capture those properties in a way that is compatible with the broader organisational context in which the data was sampled.

\section{Document Structure and Interpretation}

This document outlines the aims of this research project along two axes: parametric models for broad classes of data generating processes, and techniques that use data to apply these models to particular processes. The models are specified in the form of a set of equations, and typically contain an arbitrary number of separate components. The modelling techniques then select a finite number of particular components, and fit the data to the model by estimating the remaining unknown parameters.

In ``Per Model Aims,'' the aims regarding component selection and parameter estimation are specified, and are identical for each model. The ``Common Model Properties'' are then defined. Following this, a section is dedicated to the specification of each model, along with which phenomena ought to be captured. Finally, this document finishes with the ``Order of Priorities,'' which presents the cartesian product of the models and the per model aims, along with the chronological order these resultant aims will be attempted.

This document represents the best estimate of this project's goals and their respective priorities at the time of submission. This best estimate may change over time with new information, and there is no guarantee that the final output of this project will reflect all aims as written below. No estimates are made regarding which aims ought to be satisfied at the completion of the project, nor which levels of completion correspond to which assessment marks.

\section{Per Model Aims}

\begin{enumerate}[A)]
	\item Find or create a technique to select significant components from the model, and discuss how this technique relaxes assumptions that impede non-expert operation.
	\item Find or create a technique to consistently estimate remaining model parameters, and discuss how this technique relaxes assumptions that impede non-expert operation. Consistency refers to the property of parameter estimates converging in probability to the true parameters of the particular data generating process being modelled.
	\item If possible, analytically derive relevant properties of the sampling distribution. This includes proving the consistency of the estimator, and may also include the variance of the sampling distribution (and therefore the rate of convergence), and any other properties that are deemed to be relevant to the model in question.
	\item Demonstrate the sampling distribution and its properties with simulated or real-world data. If real-world data is used, use data that appears to contain the phenomena that ought to be modelled, and compare the predicted properties of the residuals against the actual residuals in the data. Additionally, evaluate the quality of the match between the model and the data. This might include comparing the model's AIC or BIC with the AIC or BIC when applying a more conservative model, or conducting cross-validation.
	\item Improve these techniques to be robust to occasional outliers, whereby the data infrequently deviates from the underlying process being modelled.
	\item Improve these techniques to detect and adapt to systematic breaks in the underlying process.
\end{enumerate}

\section{Common Model Properties}

Every model defined here shares a common set of properties denoted by a common notation. 

All models share a one dimensional domain. This will be referred to as time, but any other real one dimensional domain is equally valid.
\begin{equation}
t \in \mathbb{R}
\end{equation}
The process has a multivariate output of multiple features, which will be represented by a column vector.
\begin{equation}
y(t) \in \mathbb{R}^{n_y \times 1}
\end{equation}
Known samples of feature $h$ of this output will be denoted by a subscript.
\begin{equation}
h \in \mathbb{Z}_{>0}
\end{equation}
\begin{equation}
i \in \mathbb{Z}_{\geq 0}
\end{equation}
\begin{equation}
y_{h,i} = y(t_{h,i})_h
\end{equation}
The time of the soonest sample is $t_{h,0}$, followed by $t_{h,1}$, and so on.
\begin{equation}
t_{h,i+1} > t_{h,i}
\end{equation}
The process has a non-random component
\begin{equation}
\mu(t;\theta_\mu) \in \mathbb{R}^{n_y \times 1}
\end{equation}
which takes a column vector of parameters. These will be estimated from samples of our process output.
\begin{equation}
\theta_\mu \in \mathbb{R}^{n_\mu \times 1}
\end{equation}
The process randomly fluctuates around $\mu$, and these fluctuations are parameterised in the same way.
\begin{equation}
\varepsilon(t;\theta_\varepsilon) \in \mathbb{R}^{n_y \times 1}
\end{equation}
\begin{equation}
\theta_\varepsilon \in \mathbb{R}^{n_\varepsilon \times 1}
\end{equation}
The process is modelled as the sum of the non-random component and random fluctuations.
\begin{equation}
y(t) = \mu(t;\theta_\mu) + \varepsilon(t;\theta_\varepsilon)
\end{equation}
In the long run, the random fluctuations are centred on zero
\begin{equation}
\mathbb{E}[\varepsilon(t;\theta_\varepsilon)] = \lim_{s\to\infty} \mathbb{E}[\varepsilon(t;\theta_\varepsilon) \mid \varepsilon(t - s - u;\theta_\varepsilon)\ \forall\ u \geq 0] = 0
\end{equation}
and the process output is therefore centred on $\mu$.
\begin{equation}
\mathbb{E}[y(t)] = \lim_{s\to\infty} \mathbb{E}[y(t) \mid y(t - s - u) \forall\ u \geq 0] = \mu(t,\theta_\mu)
\end{equation}
For completeness, the dimensionality of $y(t)$, $\theta_\mu$ and $\theta_\varepsilon$ are all integers.
\begin{equation}
n_y \in \mathbb{Z}_{>0} ; n_\mu,n_\varepsilon \in \mathbb{Z}_{\geq 0}
\end{equation}

\section{Model One}
Model One focuses on periodicity. There is a fundamental frequency, $f_0$, and period, $T$,
\begin{equation}
f_0,T \in \mathbb{R}_{>0}
\end{equation}
\begin{equation}
f_0 = \frac{1}{T}
\end{equation}
underlying the expectation of the process.
\begin{equation}
\mu(t + T;\theta_\mu) = \mu(t;\theta_\mu)
\end{equation}
Each element $j$ of this expectation can therefore be decomposed into a Fourier series
\begin{equation}
\mu(t;\theta_\mu)_j = \gamma_j + \sum_{k=1}^{\infty} a_{j,k} \cos (k 2 \pi f_0 t) + b_{j,k} \sin (k 2 \pi f_0 t)
\end{equation}
with parameters $\theta_\mu$ spanning the frequency domain for each element of the expectation.
\begin{equation}
\theta_{\mu_j} = \left[\begin{IEEEeqnarraybox*}[][c]{,c/c/c/c/c/c/c/c,}
\gamma_j&a_{j,1}&b_{j,1}&a_{j,2}&b_{j,2}&\ldots&a_{j,\infty}&b_{j,\infty}%
\end{IEEEeqnarraybox*}\right]^T
\end{equation}
\begin{equation}
\theta_\mu =
\begin{bmatrix}
\theta_{\mu_1}\\
\vdots\\
\theta_{\mu_{n_y}}
\end{bmatrix}
\end{equation}
Assuming some finite set of frequencies make a non-negligible contribution to each element of the process, and assuming there exists some method to identify these sets, this model may be approximated by a reduced form of the Fourier series with a finite number of parameters.
\begin{equation}
l,m_j \in \mathbb{N}
\end{equation}
\begin{equation}
k_{j,l} \in \mathbb{N} \setminus \{j,k \in \mathbb{N} \mid a_{j,k}^2 + b_{j,k}^2 \approx 0\}, k_{j,l+1} > k_{j,l}
\end{equation}
\begin{equation}
\hat{\mu}(t,\theta_{\hat{\mu}})_j = \gamma_j + \sum_{l=1}^{m_j} a_{k_{j,l}} \cos (k_{j,l} 2 \pi f_0 t) + b_{k_{j,l}} \sin (k_{j,l} 2 \pi f_0 t)
\end{equation}
\begin{equation}
\theta_{\hat{\mu}_j} = \left[\begin{IEEEeqnarraybox*}[][c]{,c/c/c/c/c/c/c/c,}
\gamma_j&a_{k_{j,1}}&b_{k_{j,1}}&a_{k_{j,2}}&b_{k_{j,2}}&\ldots&a_{k_{j,m_j}}&b_{k_{j,m_j}}%
\end{IEEEeqnarraybox*}\right]^T
\end{equation}
\begin{equation}
\theta_{\hat{\mu}} =
\begin{bmatrix}
\theta_{\hat{\mu}_1}\\
\vdots\\
\theta_{\hat{\mu}_{n_y}}
\end{bmatrix}
\end{equation}
Assume that $\varepsilon$ is gaussian white noise with respect to the sampled dataset, and is uncorrelated between features.
\begin{equation}
\varepsilon(t_{h,i}; \theta_\varepsilon) \sim N\ \forall\ h,i
\end{equation}
\begin{equation}
\cov (\varepsilon(t_{h_0,i_0}; \theta_\varepsilon),\varepsilon(t_{h_1,i_1}; \theta_\varepsilon)) = \begin{cases}
	\sigma^{2}_{h} & h_0 = h_1 = h, i_0 = i_1\\
	0 & \text{otherwise}
\end{cases}
\end{equation}
The variances require estimation.
\begin{equation}
\theta_\varepsilon = \begin{bmatrix}
	\sigma^{2}_{1} & \sigma^{2}_{2} & \ldots & \sigma^{2}_{n_y}
\end{bmatrix}^T
\end{equation}

\section{Model Two}
Model Two defines a set of continuous random processes that fluctuates around zero
and are influenced by recent outputs. $\varepsilon$ will be defined by a stochastic differential equation, so let $W$ denote a $n_y$ dimensional column vector of independent Wiener processes.
\begin{equation}
\mu(t) = 0
\end{equation}
\begin{equation}
W(t) \in \mathbb{R}^{n_y \times 1}
\end{equation}
\begin{equation}
A_q , \Sigma \in \mathbb{R}^{n_y \times n_y}
\end{equation}
\begin{equation}
\derivd \derivDpow^{r-1}\varepsilon(t;\theta_\varepsilon) = \sum_{q=1}^{r} A_q \derivDpow^{q-1} \varepsilon(t;\theta_\varepsilon) \derivd t + \Sigma \derivd W(t)
\end{equation}
\begin{equation}
\derivDpow^\nu = \frac{\derivdpow^\nu}{\derivd t^\nu}
\end{equation}
Tentatively assume that for the implied autocovariance matrix to be a valid covariance matrix, $\Sigma$ must be symmetric and positive semidefinite.
\begin{equation}
\Sigma^T = \Sigma
\end{equation}
\begin{equation}
x^T \Sigma x \geq 0\ \forall\ x \in \mathbb{R}^{n_y \times 1}
\end{equation}
This assumption will be validated in the course of the project. Finally, the model parameters:
\begin{equation}
\theta_\varepsilon = \left[\begin{IEEEeqnarraybox*}[][c]{,c/c/c/c/c,}
\vect (A_1)^T&\vect (A_2)^T&\ldots&\vect (A_r)^T&\vech (\Sigma)^T%
\end{IEEEeqnarraybox*}\right]^T
\end{equation}

\section{Model Three}
Model Three combines the periodic nature of the non-random process component from Model One, with the random fluctuations from Model Two. Define $\mu$, $\hat{\mu}$, $\theta_\mu$ and $\theta_{\hat{\mu}}$ as in Model One, and define $\varepsilon$ and $\theta_\varepsilon$ as in Model Two.

\section{Model Four}
Model Four relaxes the assumption of a lag-constant covariance matrix present in Model Two. Instead it models $\Sigma$ as a continuous random process that fluctuates around some long-run expectation $\Phi$  and is influenced by past outputs of both itself and $y$. Let $W_1$ and $W_2$ denote two independent column vectors of independent Wiener processes and assume that $\Sigma$ is symmetric.
\begin{equation}
\mu(t) = 0
\end{equation}
\begin{equation}
n_\sigma = \frac{1}{2} n_y (n_y + 1)
\end{equation}
\begin{equation}
A_q, \Sigma(t), \Phi \in \mathbb{R}^{n_y \times n_y}
\end{equation}
\begin{equation}
B_q \in \mathbb{R}^{n_\sigma \times n_y}
\end{equation}
\begin{equation}
C_q,\Omega \in \mathbb{R}^{n_\sigma \times n_\sigma}
\end{equation}
\begin{equation}
\derivd \derivDpow^{r-1}\varepsilon(t;\theta_\varepsilon) = \sum_{q=1}^{r} A_q \derivDpow^{q-1} \varepsilon(t;\theta_\varepsilon) \derivd t + \Sigma(t) \derivd W_1(t)
\end{equation}
\begin{align}
\begin{split}
\derivd \derivDpow^{v_1 \vee v_2 -1}\vech (\Sigma(t)) &= \vech (\Phi) \\
&+ \sum_{u=1}^{v_1} B_q \derivD^{u-1} y(t) \derivd t\\
&+ \sum_{u=1}^{v_2} C_q \derivD^{u-1} \vech (\Sigma(t)) \derivd t\\
&+ \Omega \derivd W_2(t)
\end{split}
\end{align}
Similar to Model Two, assume that $\Omega$ is symmetric and positive semidefinite.
\begin{equation}
\Omega^T = \Omega
\end{equation}
\begin{equation}
x^T \Omega x \geq 0\ \forall\ x \in \mathbb{R}^{n_\sigma \times 1}
\end{equation}
Finally, the model parameters:
\begin{equation}
\theta_{\hat{\varepsilon}} = \left[\begin{IEEEeqnarraybox*}[][c]{,c/c/c/c,}
\vect (A_1)^T&\vect (A_2)^T&\ldots&\vect (A_r)^T%
\end{IEEEeqnarraybox*}\right]^T
\end{equation}
\begin{equation}
\theta_{\hat{\sigma}_y} = \left[\begin{IEEEeqnarraybox*}[][c]{,c/c/c/c,}
\vect (B_1)^T&\vect (B_2)^T&\ldots&\vect (B_{v_1})^T%
\end{IEEEeqnarraybox*}\right]^T
\end{equation}
\begin{equation}
\theta_{\hat{\sigma}_\sigma} = \left[\begin{IEEEeqnarraybox*}[][c]{,c/c/c/c,}
\vect (C_1)^T&\vect (C_2)^T&\ldots&\vect (C_{v_2})^T%
\end{IEEEeqnarraybox*}\right]^T
\end{equation}
\begin{equation}
\theta_\varepsilon =
\begin{bmatrix}
\theta_{\hat{\varepsilon}}\\
\theta_{\hat{\sigma}_y}\\
\theta_{\hat{\sigma}_\sigma}\\
\vech (\Omega)
\end{bmatrix}
\end{equation}

\section{Model Five}

Model Five combines the periodic nature of the non-random process from Model One, with the random fluctuations from Model Four. Define $f$, $\hat{f}$, $\theta_f$ and $\theta_{\hat{f}}$ as in Model One, and define $\varepsilon$ and $\theta_\varepsilon$ as in Model Four.

\newpage

\section{Order of Priorities}
\begin{table}[h]
	\centering
	\caption{Chronological priority of each Aim-Model combination.}
	\label{table:priorities}
	\begin{tabular}{|c|c|c|c|c|c|c|c|}
		\cline{3-8}
		 \multicolumn{2}{c|}{} & \multicolumn{6}{|c|}{Per Model Aim}\\
		 \cline{3-8}
		 \multicolumn{2}{c|}{} & A & B & C & D & E & F\\
		\hline
		\multirow{5}{*}{Model Number}
		& One & \textbf{1} & \textbf{2} & \textbf{3} & \textbf{4} & 9 & 10\\
		\cline{2-8}
		& Two & 5 & 6 & 7 & 8 & 15 & 16\\
		\cline{2-8}
		& Three & 11 & 12 & 13 & 14 & 17 & 18\\
		\cline{2-8}
		& Four & 19 & 20 & 21 & 22 & 27 & 28\\
		\cline{2-8}
		& Five & 23 & 24 & 25 & 26 & 29 & 30\\
		\hline
	\end{tabular}
\end{table}

Each element of table \ref{table:priorities} contains the order that the corresponding Aim-Model combination will be satisfied. For example, Aim E for Model Two will be explored 15th after Aim D for Model Three. Where existing research has covered an entry, the research will be cited as satisfaction of the Aim-Model combination. Otherwise, satisfaction of this entry will constitute original research.

Together, the bold entries correspond to strong project output that may combine into a compelling narrative for a non-technical audience. Progress up to entry four lends itself to convincing spark night and summary video presentations.

\end{document}