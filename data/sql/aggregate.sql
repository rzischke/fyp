CREATE TABLE IF NOT EXISTS telecom_time_interval_marginal (
    time_interval               INTEGER,
    sms_in_activity             REAL,
    sms_out_activity            REAL,
    call_in_activity            REAL,
    call_out_activity           REAL,
    internet_traffic_activity   REAL,
    PRIMARY KEY (time_interval)
);

INSERT INTO telecom_time_interval_marginal SELECT
    time_interval,
    total(sms_in_activity),
    total(sms_out_activity),
    total(call_in_activity),
    total(call_out_activity),
    total(internet_traffic_activity)
FROM
    telecom
GROUP BY
    time_interval
;

