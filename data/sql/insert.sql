/*
    See
    https://dandelion.eu/datagems/SpazioDati/telecom-sms-call-internet-tn/resource/
    for documentation.
*/


INSERT OR IGNORE INTO telecom (
    square_id,
    time_interval,
    country_code,
    sms_in_activity,
    sms_out_activity,
    call_in_activity,
    call_out_activity,
    internet_traffic_activity
) VALUES (
    ?,
    ?,
    ?,
    ?,
    ?,
    ?,
    ?,
    ?
);

