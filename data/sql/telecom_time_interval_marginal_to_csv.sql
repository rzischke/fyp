CREATE TEMPORARY TABLE first_sample AS SELECT
    MIN(telecom_time_interval_marginal.time_interval) AS time_interval
FROM
    telecom_time_interval_marginal
;

.headers on
.mode csv
.output telecom_time_interval_marginal.csv

SELECT 
    telecom_time_interval_marginal.time_interval AS time_interval,
    (telecom_time_interval_marginal.time_interval - first_sample.time_interval)/1000 AS seconds_since_first_sample,
    (telecom_time_interval_marginal.time_interval - first_sample.time_interval)/86400000.0 AS days_since_first_sample,
    datetime(telecom_time_interval_marginal.time_interval/1000, 'unixepoch') AS timestamp,
    telecom_time_interval_marginal.sms_in_activity AS sms_in_activity,
    telecom_time_interval_marginal.sms_out_activity AS sms_out_activity,
    telecom_time_interval_marginal.call_in_activity AS call_in_activity,
    telecom_time_interval_marginal.call_out_activity AS call_out_activity,
    telecom_time_interval_marginal.internet_traffic_activity AS internet_traffic_activity
FROM
    telecom_time_interval_marginal,
    first_sample
;

.mode column
.output stdout
