/*
    See
    https://dandelion.eu/datagems/SpazioDati/telecom-sms-call-internet-tn/resource/
    for documentation.
*/

CREATE TABLE IF NOT EXISTS telecom (
    time_interval               INTEGER,
    square_id                   INTEGER,
    country_code                INTEGER,
    sms_in_activity             REAL,
    sms_out_activity            REAL,
    call_in_activity            REAL,
    call_out_activity           REAL,
    internet_traffic_activity   REAL,
    PRIMARY KEY (time_interval, square_id, country_code)
);

