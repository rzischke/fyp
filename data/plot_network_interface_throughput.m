clear variables;
close all;
clc;

format_spec = '%s %s %{yyyy-MM-dd}D %{yyyy-MM-dd HH:mm:ss}D %s %s %s %s %s';
dsl_snmp_bng_unclean = readtable('network_interface_throughput.csv','Filetype','text','Delimiter',',','Format',format_spec,'ReadVariableNames',true);
dsl_snmp_bng = dsl_snmp_bng_unclean(...
    dsl_snmp_bng_unclean.totalbytes~="NULL" &...
    dsl_snmp_bng_unclean.bytesin~="NULL" &...
    dsl_snmp_bng_unclean.bytesout~="NULL"...
,...
    :...
);

interface_1_name = "397ae4e2e83eb5eb96ddd769994c87435e765102";
interface_2_name = "c49d9624222752a816aab2a87fcd391765c6168a";
interface_3_name = "df4594706c74649407779202d11b89845cf5ec12";
interface_1 = sortrows(dsl_snmp_bng(dsl_snmp_bng.sha1instancename==interface_1_name,:),'sampletime');
interface_2 = sortrows(dsl_snmp_bng(dsl_snmp_bng.sha1instancename==interface_2_name,:),'sampletime');
interface_3 = sortrows(dsl_snmp_bng(dsl_snmp_bng.sha1instancename==interface_3_name,:),'sampletime');

figure;
plot(interface_1.sampletime, log10(str2double(interface_1.bytesin)+1));
hold on;
plot(interface_1.sampletime, log10(str2double(interface_1.bytesout)+1));
title('Interface 1');
ylabel('log(bytes+1)');
legend('bytesin','bytesout','Location','northwest');

figure;
plot(interface_2.sampletime, log10(str2double(interface_2.bytesin)+1));
hold on;
plot(interface_2.sampletime, log10(str2double(interface_2.bytesout)+1));
title('Interface 2');
ylabel('log(bytes+1)');
legend('bytesin','bytesout','Location','northwest');

figure;
plot(interface_3.sampletime, log10(str2double(interface_3.bytesin)+1));
hold on;
plot(interface_3.sampletime, log10(str2double(interface_3.bytesout)+1));
title('Interface 3');
ylabel('log(bytes+1)');
legend('bytesin','bytesout','Location','northwest');
